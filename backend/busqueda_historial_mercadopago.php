<?php
include("../comunes/variables.php");
include("../comunes/verificar_admin.php");
include("../comunes/conexion.php");

$corre_user=$_POST['corre_user'];
$fstat_cupon=$_POST['fstat_cupon'];
$fstat_cupon1=$_POST['fstat_cupon1'];
$status=$_POST['status'];

$buscar_mp = "SELECT * FROM metodo_pago WHERE nomb_mpago = 'MERCADOPAGO'";
$codg_mp = mysql_fetch_array(mysql_query($buscar_mp));
$id_mpago = $codg_mp[id_mpago];

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <title><?php echo $nom_pagina; ?></title>
  </head>
<body>
<?php
	$consulta="SELECT * FROM pago p, venta v, usuarios u WHERE p.id_mpago = ".$id_mpago." AND p.id_venta=v.id_venta AND v.id_user=u.id_user AND u.corre_user LIKE '%".$_POST[corre_user]."%'";
  if ($_POST[fini_mp]){
    $_POST[fini_mp] = date('Y-m-d',strtotime($_POST[fini_mp]));
    $consulta.= " AND fech_pago >= '".$_POST[fini_mp]."'";
  }
  if ($_POST[ffin_mp]){
    $_POST[ffin_mp] = date('Y-m-d',strtotime($_POST[ffin_mp]));
    $consulta.= " AND fech_pago <= '".$_POST[ffin_mp]."'";
  }
  $consulta.= " ORDER BY fech_pago DESC";
  //echo $consulta;
  $con=mysql_query($consulta);
	if (mysql_num_rows($con)>0)
	{
    echo '<div class="container">';
    	echo '<div class="titulo_form">Pagos efectuados con MercadoPago</div>';
    	echo '<div class="table-responsive">';
      	echo '<table class="table table-striped table-bordered table-hover table-condensed">';
        	echo '<tr class="info" style="font-size: 0.8em">';
          	echo '<th>#</th> <th>Nombre y Apellidos</th> <th>Correo</th><th style="text-align: center"># de Orden</th><th>Referencia MP</th> <th>Monto</th>  <th style="text-align: center">Fecha de Pago</th>';
        	echo '</tr>';
          $i = 1;
          while($res_mp=mysql_fetch_array($con)){
            echo '<tr style="font-size: 0.8em">
              <td style="text-align: right; padding-right: 5px;">'.$i.'</td>
              <td>'.$res_mp[nom_ape_user].'</td>
              <td>'.$res_mp[corre_user].'</td>
              <td style="text-align: center">'.$res_mp[codg_trans].'</td>
              <td>'.$res_mp[num_refe_banc].'</td>
              <td style="text-align:right;padding-right:5px;">'.number_format($res_mp[mont_pago],2,',','.').'</td>
              <td style="text-align: center">'.date('d-m-Y', strtotime($res_mp[fech_pago])).'</td>
            </tr>';
          }
      	echo '</table>';
    	echo '</div>';
    echo '</div>';
	}
	else
	{
		echo '<div class="titulo_form">Registros No Encontrados</div>';
	}
?>
  <br>
 <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>