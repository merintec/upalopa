<?php 
session_start();
include("../comunes/conexion.php");

$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
$id_user=$_SESSION['id_user'];
$categoria=$_GET['categoria'];
$categoria = '7';
$logo='../imagenes/sistema/logo.png';
$con[nomb_cate] = 'Cesta de Compras';
$con[desc_cate] = '<b>Tu Tienda UPALOPA<b>';
$color_fondo='#D2C8B0';
include("../comunes/variables.php");
//include("../comunes/verificar_admin.php");
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <script>
    $(document).ready(function() {
        // bootstrap-datepicker
        $('.datepicker').datepicker({
          format: "dd-mm-yyyy",
          language: "es",
          autoclose: true,
          todayBtn: true
        }).on(
          'show', function() {      
          // Obtener valores actuales z-index de cada elemento
          var zIndexModal = $('#modal_pago').css('z-index');
          var zIndexFecha = $('.datepicker').css('z-index');
         
                // alert(zIndexModal + zIndexFEcha);
         
                // Re asignamos el valor z-index para mostrar sobre la ventana modal
                $('.datepicker').css('z-index',zIndexModal+1);
        });
    });
    function guardar_envio(formulario){
      if ($("#" + formulario).validationEngine('validate')){
          var url="procesar_envio.php"; 
          $.ajax
          ({
              type: "POST",
              url: url,
              data: $("#" + formulario).serialize(),
              success: function(data)
              {
                var codigo, datatemp, mensaje, texto;
                datatemp=data;
                datatemp=datatemp.split(":::");
                codigo=datatemp[0];
                mensaje=datatemp[1];
                if (codigo==001)
                {
                  destino = datatemp[2];
                  orden = datatemp[3];
                  empresa = datatemp[4];
                  guia = datatemp[5];
                  boton = datatemp[6];
                  texto = "Hemos efectuado el envío de tu Orden de compra Nº " + orden + ".<br>Empresa de envío: <b>" + empresa + "</b>.<br>El número de Guía: <b>" + guia + "</b>";
                  enviar_correo(destino,'Datos de Envío (UPALOPA)',texto);
                  $('#btn_' + boton).removeClass('oculto');
                }
                $("#resultado").html(mensaje);
                setTimeout(function() {
                  $("#msg_act").fadeOut(1500);
                },3000);
              }
          });
      }
    }
    function facturado (venta,estado)
    {
      var tabla, campo, valor, campo_id, valor_id;
      tabla = 'venta';
      campo = 'factura';
      valor = estado;
      campo_id = 'id_venta';
      valor_id = venta;
      var parametros = {
        "var_tabla": tabla,
        "var_campo" : campo,
        "var_valor" : valor,
        "var_id" : campo_id,
        "var_id_val" : valor_id
      };
      var url="../comunes/funcion_actualizarcampo.php"; 
      $.ajax
      ({
        type: "POST",
          url: url,
          data: parametros,
          success: function(data)
          {
            $("#resultado").html(data);
            setTimeout(function() {
                  $("#msg_act").fadeOut(1500);
            },3000);
            if (estado == 'SI'){
              $("#venta" + venta).addClass('oculto');
            }
          }
      });
      ////// actualizar fecha de finalizacion de la venta
      campo = 'ffin_venta';
      valor = '<?php echo date('Y-m-d'); ?>';
      var parametros = {
        "var_tabla": tabla,
        "var_campo" : campo,
        "var_valor" : valor,
        "var_id" : campo_id,
        "var_id_val" : valor_id
      };
      $.ajax
      ({
        type: "POST",
          url: url,
          data: parametros,
          success: function(data)
          {
            
          }
      });
      return false; 
    }

      function enviar_correo(destino,titulo,mensaje)
      {
        //alert(destino + titulo + mensaje);
        var url="../comunes/enviar_correo.php"; 
        var parametros = {
            "mensaje" : mensaje,
            "destino" : destino, 
            "titulo" : titulo
          };
        $.ajax
        ({
            type: "POST",
            url: url,
            data: parametros,
            success: function(data)
            {

               
            }
        });
      }

      function muestra_oculta(id){
          if (document.getElementById){ //se obtiene el id
              var el = document.getElementById(id); //se define la variable "el" igual a nuestro div
              el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
          }
      }
      function pasar_modal(pago,venta,accion,tot_compra,motivo){
        var var_onclick = 'procesar('+pago+','+venta+',"'+accion+'","'+tot_compra+'",$("#moti_rech").val())';
        $('#aceptar_motivo').attr("onclick", var_onclick);
      }
    </script> 
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <body class="">    
    <div class="container-fluid">
      <div class="row">
        <?php include("menu_backend.php"); ?>
      </div>
      <div data-offset-top="100" class="container" data-spy="affix">
        <div id="resultado"></div>
      </div>
      <div class="text-center"> <span class="text-info"><h3> <b> Gestión de Facturación </b>  </h3></span> </div>
    <div class="container" >

        <?php    
        $consulta="SELECT * FROM venta as v, usuarios u  where v.id_user=u.id_user AND (v.status_venta='confirmado' OR v.status_venta='enviado') AND v.factura!='SI' ";
        $consulta=mysql_query($consulta);

        $i=0;
        while($fila=mysql_fetch_array($consulta))
        {
          $nombre_form = 'form'.$fila[id_venta];
          $totalp=0;
          $total_pagar=0;
          $total_cant=0;
          $i++;
          // consultar si ya tiene un envio registrado
          $id_empr_envi = '';
          $fecha = '';
          $guia = '';
          $sql_env = "SELECT * FROM envios WHERE id_venta = ".$fila[id_venta];
          if ($fila_env = mysql_fetch_array(mysql_query($sql_env))){
              $id_empr_envi = $fila_env[id_empr_envi];
              $fecha = $fila_env[fech_envio];
              $nueva = explode('-', $fecha);
              if (strlen($fecha) == 10 && strlen($nueva[0]) == 4 && strlen($nueva[1]) == 2 && strlen($nueva[2]) == 2){
                $fecha = $nueva[2].'-'.$nueva[1].'-'.$nueva[0];
              }
              $guia = $fila_env[guia_envio];
          }

                   $consulta2="SELECT * FROM venta_productos as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
                   vp.id_venta='$fila[id_venta]' and vp.status_vent='procesado' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
                  $consulta2=mysql_query($consulta2);
                  while ($fila2=mysql_fetch_array($consulta2)) 
                  {
                     $totalp=$fila2[vuni_venta_prod]*$fila2[cant_venta_prod];
                     $total_pagar += $totalp;
                     $total_cant += $fila2[cant_venta_prod];
                     $peso_cu=$fila2[peso_prod] * $fila2[cant_venta_prod];
                     $total_peso += $peso_cu;

                      $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
                      $consulta_envio=mysql_query($consulta_envio);
                      $con_env=mysql_fetch_assoc($consulta_envio);;

                      if (!$con_env[id_tenvios])
                      {

                            $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                            $consulta_envio=mysql_query($consulta_envio);
                            $con_env=mysql_fetch_assoc($consulta_envio);

                            $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);

                      }


                    }

                   $enviar_regalo=$fila[regalo];
                   if ($enviar_regalo=='')
                        {
                         $enviar_regalo='NO';
                        }
                    echo '<div id="venta'.$fila[id_venta].'" class="table-responsive procesar_linea_punteada">';
                    echo '<table class="table table-striped table-hover">
                    <tr><th>N° de Orden </th> <th>Cantidad de Productos </th> <th> Monto a Cancelar </th> <th> &nbsp; </th> </tr>';
                  if (!$guia) { $add_btn = 'oculto'; } else { $add_btn = ''; }       
                  echo '<tr><td>'.$fila[codg_trans].' </td> <td>'.$total_cant.'</td> <td>'.number_format(($total_pagar+$con_env[prec_tenvio]+($total_pagar * ($con_env[porc_tenvio]/100))),2,",",".").' </td><td class="text-right">'; echo '<button id="btn_'.$fila[id_venta].'" class="btn btn-success" title="Marcar como Facturado." onclick="facturado(\''.$fila[id_venta].'\',\'SI\')"> <span class="glyphicon glyphicon-ok"  aria-hidden="true"> </button> '; echo'<button class="btn btn-primary" title="Mostrar detalle de la compra" onclick="muestra_oculta(\'desplegar'.$i.'\')"> <span class="glyphicon glyphicon-eye-open"  aria-hidden="true"> </button></td> </tr>';
                  echo '</table>';
                        echo '<div class="table-responsive" >';
                          echo '<div class="row" id="desplegar'.$i.'" style="display: none;">';
                          echo '<div class="col-md-12 col-xs-12">';                        
                            echo '<span class="text-info" ><b>Datos del comprador(a):</b></span>';
                            echo '<table  class="table table-striped table-hover" style="font-size: 0.7em">';
                            echo '<tr><th>Nombres y Apellidos</th><th>Correo</th><th>Teléfono</th></tr>';
                            echo '<tr><td>'.$fila[nom_ape_user].'</td><td>'.$fila[corre_user].'</td><td>'.$fila[tele_user].'</td></tr>';
                            echo '</table>'; 
                          echo '</div>';
                            echo '<div class="col-md-6 col-xs-6">';
                                  echo '<span class="text-info" ><b>Productos de la Compra</b></span>';
                                  echo '<table  class="table table-striped table-hover" style="font-size: 0.7em">'; 
                                  echo '<tr> <th># </th> <th>Producto</th> <th>Cantidad </th> <th>Talla </th> <th>Color </th> <th> Precio Unitario </th> <th> Subtotal</th> <th> Impuesto </th> <th> Total </th> </tr>';

                                  //consulta para mostrar detalle de articulos
                                  $consulta3="SELECT * FROM venta_productos as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
                                   vp.id_venta='$fila[id_venta]' and vp.status_vent='procesado' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
                                  $consulta3=mysql_query($consulta3);
                                  $j=0;
                                    

                                  while ($fila3=mysql_fetch_array($consulta3)) 
                                  {
                                      $totalp=$fila[vuni_venta_prod]*$fila[cant_venta_prod];
                                      $imp_cu = ($totalp / (($val_imp/100)+1));
                                      $imp_cu = $imp_cu * $val_imp / 100;
                                      $imp_cu = number_format($imp_cu,2,'.','');
                                      //// para los totales generales
                                      $peso_cu=$fila[peso_prod] * $fila[cant_venta_prod];
                                      $total_pagar += $totalp;
                                      $total_peso += $peso_cu;
                                      $total_imp += $imp_cu;
                                      $total_cant += $fila[cant_venta_prod];
                                      $subtotal=$fila3[vuni_venta_prod]*$fila3[cant_venta_prod];

                                      $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
                                      $consulta_envio=mysql_query($consulta_envio);
                                      $con_env=mysql_fetch_assoc($consulta_envio);;

                                      $impuesto= (($subtotal*12)/100);

                                      $sub_imp=$subtotal-$impuesto;

                                      $sub_prod= $subtotal - $imp_cu;

                                      if (!$con_env[id_tenvios])
                                      {

                                            $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                                            $consulta_envio=mysql_query($consulta_envio);
                                            $con_env=mysql_fetch_assoc($consulta_envio);

                                            $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);

                                      }
                                      $j++;
                                              echo '<tr> <td>'.$j.'</td><td>'.$fila3[nomb_prod].' </td> <td align="center">'.$fila3[cant_venta_prod].'</td> <td>'.$fila3[desc_talla].' </td> <td>'.$fila3[nom_color].' </td>  <td align="right">'.number_format($fila3[vuni_venta_prod],2,",",".").' </td> <td align="right">'.number_format($sub_imp,2,",",".").' </td> <td align="right">'.number_format($impuesto,2,",",".").' </td><td align="right">'.number_format($subtotal,2,",",".").' </td> </tr>';

                                     
                                      
                                  }   $final_pagar = number_format(($total_pagar+$con_env[prec_tenvio]+($total_pagar * ($con_env[porc_tenvio]/100))),2,".","");
                                      echo '<tr><td align="right" colspan="3"><b>Subtotal: Bs.</b> </td><td align="right" colspan="2"><b>'.number_format($total_pagar,2,",",".").'</b></td></tr>';
                                      echo '<tr><td align="right" colspan="3"><b>Costo de Envio: Bs. </b></td><td align="right" colspan="2"><b>'.number_format($con_env[prec_tenvio],2,",",".").'</b></td></tr>';
                                      echo '<tr><td align="right" colspan="3"><b>Costo de Seguro: Bs.</b> </td><td align="right" colspan="2"><b>'.number_format(($total_pagar * ($con_env[porc_tenvio]/100)),2,",",".").'</b></td></tr>';
                                      echo '<tr><td align="right" colspan="3"><b>Total a Pagar Bs. </b></td><td align="right" colspan="2"  style=" font-size: 1.3em; color:#c174ad;?>;" ><b>'.$final_pagar.'</b></td></tr>';
                                  echo "</table>";
                            echo '</div>';
                            echo '<div class="col-md-6 col-xs-6">';
                              echo '<span class="text-info" ><b>Datos de Facturación</b></span>';
                              ///// datos de facturacion
                              $sql_fact = "SELECT * FROM datos_fact WHERE id_datos = ".$fila[id_datos];
                              if ($res_fact = mysql_fetch_array(mysql_query($sql_fact))){
                                      $datos_fact  = '<table class="table table-striped"  style="margin-bottom: 0px; font-size: 0.7em">';
                                      $datos_fact .= '<tr><td>RIF:</td><td><b>&nbsp;'.$res_fact[tiden_datos].'-'.$res_fact[iden_datos].'</b></td></tr>';
                                      $datos_fact .= '<tr><td>Razón Social: </td><td><b>&nbsp;'.$res_fact[razon_datos].'</b></td></tr>';
                                      $datos_fact .= '<tr><td>Dirección:</td><td><b>&nbsp;'.ucwords($res_fact[dire_datos]).'</b></td></tr>';
                                      $datos_fact .= '<tr><td>Teléfonos:</td><td><b>&nbsp;'.$res_fact[telfh_datos].' / '.$res_fact[telfm_datos].'</b></td></tr>';
                                      $datos_fact .= '</table>';
                              }
                              echo $datos_fact;
                            echo '</div>';
                          echo '</div>'; 
                        echo "</div>";
                  echo "</div>";

        }
	if ($i==0){
		echo '<div class="alert-info text-center" style="border: 1px dashed #000; max-width: 50%; height: 5em; padding-top: 1em; margin: auto auto;" ><h4>No tienes facturas por procesar!!!</h4></div>';
	}  
       ?>
       </div>
 
      <br>
      <br>
      <br>
      <br>

    </div>
    <!-- Modal para login -->
    <div class="modal fade" id="login" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 350px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal">
              <?php 
                include('login.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div> 
    </div>
    <!-- Modal Motivo -->
    <div class="modal fade" id="modal_motivo" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4>Esta acción requiere información adicional</h4>
          </div>
          <div class="modal-body">
            <span class="glyphicon glyphicon-question-sign glyphicon_azul pull-left msg_modal_icon"></span>
              <p>¿Cuál es el motivo del Rechazo?</p>
              <input id="moti_rech" name="moti_rech" type="text" style="max-width: 30em;" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[60]] text-input, form-control">
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button id="aceptar_motivo" class="btn btn-primary" >Aceptar</button>
          </div>
        </div>  
      </div>
    </div>    
    <script src="../bootstrap/js/bootstrap.min.js"> </script>  
  </body>
</html>
