<?php
include("../comunes/variables.php");
include("../comunes/verificar_admin.php");
include("../comunes/conexion.php");


$mostrar=$_POST[mostrar];

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
    <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    <title><?php echo $nom_pagina; ?></title>
  </head>
<script type="text/javascript">


  function actualizar(valor_id,fecha,respuesta)
  {
    var tabla='devoluciones';
    var campo='fecha_resp'; 
    var campo_id='id_devo';
    var valor=fecha;
    actualizar_dato_mp(tabla,campo,valor,campo_id,valor_id);
    campo='respuesta';
    valor=respuesta;
    actualizar_dato_mp(tabla,campo,valor,campo_id,valor_id);
    window.location='devolucion_admin.php';

  }

 function actualizar_dato_mp(tabla,campo,valor,campo_id,valor_id)
    {
        //para verificar si es una fecha voltearla al momento de guardarla
          var resultado;
          var parametros = {
            "var_tabla": tabla,
            "var_campo" : campo,
            "var_valor" : valor,
            "var_id" : campo_id,
            "var_id_val" : valor_id
          };
          var url="../comunes/funcion_actualizarcampo.php"; 
          $.ajax
          ({
            type: "POST",
              url: url,
              data: parametros,
              success: function(data)
              {

              }
          });
    }

  </script>

<body>
    <?php
        include("menu_backend.php");
    ?>
<div data-offset-top="100" class="container" data-spy="affix">
  <div id="resultado"> </div>
</div>

  <div class="container">

   <form id="form1" name="form1" action="" method="POST">

     <table> <tr> <td> <h5><b> Mostrar Todos  
         <?php
         if ($mostrar==1)
         {

            echo '<input type="checkbox" name="mostrar" id="mostrar"  value="1" onclick="submit()" checked  >';

         }
         else
         {
            echo '<input type="checkbox" name="mostrar" id="mostrar"  value="1" onclick="submit()"  >';

         }
          ?>
        </form>  
       </b></h5> </td>   </tr> </table> 
        
  </div>  




  <div id="consulta">  



    <div class="container">

          <div class="titulo_form">  Devoluciones de Productos </div>

       
             <div class="row">


              <div class="table-responsive procesar_linea_punteada">
                 <table class="table table-striped table-hover" width="100%">
                    <tr><th class="fondo_predefinido">N° </th> <th class="fondo_predefinido">Producto </th> <th class="fondo_predefinido"> Cantidad </th> <th class="fondo_predefinido"> Orden de Compra </th> <th class="fondo_predefinido"> Usuario </th> <th class="fondo_predefinido"> Motivo </th> <th class="fondo_predefinido"> Fecha de Entregado </th> <th class="fondo_predefinido"> Fecha de Reporte </th>  <th class="fondo_predefinido"> Respuesta </th> <th class="fondo_predefinido"> Acción </th>  </tr>
   
                <?php  

                if ($mostrar==1)
                {
                      $consulta="SELECT * FROM devoluciones as d, usuarios as u, venta_productos as vp, venta as v, producto_detalles as pd, productos as p where  
                      vp.id_venta_prod=d.id_venta_prod and v.id_venta=vp.id_venta and u.id_user=v.id_user and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod ";
                
                }  
                else
                {

                      $consulta="SELECT * FROM devoluciones as d, usuarios as u, venta_productos as vp, venta as v, producto_detalles as pd, productos as p where  
                      d.respuesta is NULL and vp.id_venta_prod=d.id_venta_prod and v.id_venta=vp.id_venta and u.id_user=v.id_user and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod ";
                
                }

                $consulta=mysql_query($consulta);

                $i=0;
                while($fila=mysql_fetch_array($consulta))
                {
                 
                  $i++;
                  $fecha=date("Y-m-d");


          
                          echo '<tr><td>'.$i.' </td> <td class="text-left">'.$fila[nomb_prod].' </td> <td class="text-left">'.$fila[cantidad].' </td><td class="text-left">'.$fila[codg_trans].' </td> <td class="text-left">'.$fila[corre_user].' </td><td class="text-left">'.$fila[motivo].' </td> <td class="text-left">'.$fila[ffin_venta].' </td> <td class="text-left">'.$fila[fecha_reporte].' </td> <td class="text-left"> '; if ($fila[respuesta]) { echo $fila[respuesta].'</td><td>&nbsp;</td></tr>'; } else { echo '<input type="text" name="respuesta'.$i.'" id="respuesta'.$i.'"></td><td><button class="btn btn-primary btn-xs" title="Guardar" onclick="actualizar('.$fila[id_devo].',\''.$fecha.'\',$(\'#respuesta'.$i.'\').val())" id="guardar"> <span class="glyphicon glyphicon-floppy-disk"  aria-hidden="true"> </button> </td></tr>';}
                          
                
                  

                 }


?>

                     </table>;
                       
                    </div>    

</div>
</div>
 
 

  <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>

