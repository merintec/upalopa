<?php 
session_start();
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
include("../comunes/verificar_admin.php");
include_once("../comunes/verificacion.php");

?>
<div class="row" id="top_pagina">
  <div class="col-md-2" align="center"><a href="principal.php"><img src="../imagenes/sistema/logo.png" title="Logo Upapola" width="100" height="100" ></a></div>
  <div class="col-md-10">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

          <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Configuraci&oacute;n <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="tipos_pago.php">Tipos de Pago</a></li>
                <li><a href="cuentas_bancarias.php">Cuentas Bancarias</a></li>
                <li><a href="empresa_envio.php">Empresas de Envios</a></li>
                <li><a href="tabla_envios.php">Tabla de Envios</a> </li>
              </ul>
            </li>
          </ul>

          </ul>

          <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuarios<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="usuarios.php">Registro de Usuarios</a></li>
                <li><a href="usuarios_news.php">Usuarios News</a></li>
            
                
              </ul>
            </li>
          </ul>
           <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Inventario <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="categorias.php">Categorias</a></li>
                <li><a href="sub_categoria.php">Sub-Categorias</a></li>
                <li><a href="tallas.php">Tallas</a></li>
                <li><a href="colores.php">Colores</a></li>
                <li><a href="marca.php">Marcas</a></li>
                <li><a href="productos.php">Productos</a></li>
                <li><a href="inventario.php">Inventario</a></li>
                <li><a href="disponibilidad.php">Disponibilidad</a></li>

              </ul>
            </li>
            <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Herramientas<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="cupones_descuentos.php">Cupones de Descuento</a></li>
                <li><a href="historial_cupones.php">Historial de Cupones</a></li>
                <li><a href="historial_giftcard.php">Historial de GiftCard</a></li>
                <li><a href="historial_mercadopago.php">Historial de MercadoPago</a></li>
                <li><a href="historial.php">Historial de Ventas</a></li>
                <li><a href="devolucion_admin.php">Devoluciones</a></li>




              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="pagos.php" role="button" aria-expanded="false">Pagos</a>
              </li>
          </ul>
           <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="facturacion.php" role="button">Factura</a>
             </li>
           </ul>
           <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="envios.php" role="button">Envíos </a>
             </li>
           </ul>

            <ul class="nav navbar-right">
             <li class="dropdown">
              <a href="../comunes/cerrar_sesion.php"  >Cerrar Sesi&oacute;n (<?php echo $_SESSION['usuario_logueado'];  ?>) <span class="glyphicon glyphicon-user"> </span> </a>
               
            </li>
          </ul>

          
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
  </div>
</div>

