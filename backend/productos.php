<?php
include("../comunes/variables.php");
include("../comunes/verificar_admin.php");
include("../comunes/conexion.php");
$tabla='productos';
$titulos_busqueda = 'Categoría,Nombre,Descripci&oacute;n,Precio,Status,Marca,Sub-Categoria';
$datos_busqueda = 'id_scate->sub_categoria::id_scate::id_cate->categoria::id_cate::nomb_cate,nomb_prod,desc_prod,prec_prod,status_prod,id_marca->marca::id_marca::nomb_marca,id_scate->sub_categoria::id_scate::nomb_scate';
$nregistros = 5;      
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <script>
    $(document).ready(function() {
        $('.datepicker')
            .datepicker({
              format: 'dd-mm-yyyy',
              autoclose: true,
              language: 'es'
            });
    });
    </script>
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <!-- validacion en vivo -->
<script >
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
   });
</script>
<!-- funcion de guardar formulario -->  
<script type="text/javascript">

//para el combo de subcategoria

 
    $(document).ready(function() {
    // Parametros para el combo
     $("#id_cate").change(function () {
        $("#id_cate option:selected").each(function () {
          
          id_cate=$(this).val();
          $.post("combo_subcategoria.php", { id_cate: id_cate }, function(data){
          //$("#prueba").html(data);
          $("#id_scate").html(data);
        });     
       });
     });    
  });

$(function()
{
    $("#guardar").click(function()
    {
      if ($("#form1").validationEngine('validate')){
        var url="../comunes/funcion_guardar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: $("#form1").serialize(),
            success: function(data)
            {
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                $('#msg_imag').hide();
                $('#msg_imag1').hide();
                $('#msg_imag2').hide();
                $('#msg_imag3').hide();
                $("#form1")[0].reset();
                $('#imagen_cargada').attr('src','');
                $('#imagen_cargada1').attr('src','');
                $('#imagen_cargada2').attr('src','');
                $('#imagen_cargada3').attr('src','');
              }
              $("#resultado").html(mensaje);
              setTimeout(function() {
                $("#msg_act").fadeOut(1500);
              },3000);
              buscar();
            }
        });
        return false;
      }
    });
});
function buscar()
{
  var url="../comunes/busqueda.php"; 
  $.ajax
  ({
    type: "POST",
    url: url,
    data: $("#form1").serialize(),
    success: function(data)
    {
      $("#resultado_busqueda").html(data);
    }
  });
  return false; 
}
</script>
<script type="text/javascript">
//imagen 1
$(document).ready(function(){
    var parametros = {
            "direc" : "msg_imag"
    };
    var button = $('#upload_button');
    new AjaxUpload('#upload_button', {
        type: "POST", 
        data: parametros,
        action: 'subir_imagen_productos.php',
        onSubmit : function(file , ext){
        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
            // extensiones permitidas
            var mensaje;
            mensaje='<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error... Solo se permiten imagenes</strong></div>';
             $("#resultado").html(mensaje);
            // cancela upload
            return false;
        } else {
            $('#status_imagen').html('Cargando...');
            this.disable();
        }
        },
        onComplete: function(file, response){
            // habilito upload button
            $('#status_imagen').html(response);            
            this.enable();          
            // Agrega archivo a la lista
            var nom_arch=file;
            $('#imag_prod').val(nom_arch);
            $('#imagen_cargada').attr('src','../imagenes/uploads/productos/'+nom_arch);

            //html('.files').text(file);
        }   
    });
});

//imagen 1
$(document).ready(function(){
    var parametros = {
            "direc" : "msg_imag1"
    };
    var button = $('#upload_button1');
    new AjaxUpload('#upload_button1', {
        type: "POST", 
        data: parametros,
        action: 'subir_imagen_productos.php',
        onSubmit : function(file , ext){
        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
            // extensiones permitidas
            var mensaje;
            mensaje='<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error... Solo se permiten imagenes</strong></div>';
             $("#resultado").html(mensaje);
            // cancela upload
            return false;
        } else {
            $('#status_imagen1').html('Cargando...');
            this.disable();
        }
        },
        onComplete: function(file, response){
            // habilito upload button
            $('#status_imagen1').html(response);            
            this.enable();          
            // Agrega archivo a la lista
            var nom_arch=file;
            $('#imag_prod1').val(nom_arch);
            $('#imagen_cargada1').attr('src','../imagenes/uploads/productos/'+nom_arch);

            //html('.files').text(file);
        }   
    });
});


//imagen 2
$(document).ready(function(){
    var parametros = {
            "direc" : "msg_imag2"
    };
    var button = $('#upload_button2');
    new AjaxUpload('#upload_button2', {
        type: "POST", 
        data: parametros,
        action: 'subir_imagen_productos.php',
        onSubmit : function(file , ext){
        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
            // extensiones permitidas
            var mensaje;
            mensaje='<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error... Solo se permiten imagenes</strong></div>';
             $("#resultado").html(mensaje);
            // cancela upload
            return false;
        } else {
            $('#status_imagen2').html('Cargando...');
            this.disable();
        }
        },
        onComplete: function(file, response){
            // habilito upload button
            $('#status_imagen2').html(response);            
            this.enable();          
            // Agrega archivo a la lista
            var nom_arch=file;
            $('#imag_prod2').val(nom_arch);
            $('#imagen_cargada2').attr('src','../imagenes/uploads/productos/'+nom_arch);

            //html('.files').text(file);
        }   
    });
});


//imagen 3
$(document).ready(function(){
    var parametros = {
            "direc" : "msg_imag3"
    };
    var button = $('#upload_button3');
    new AjaxUpload('#upload_button3', {
        type: "POST", 
        data: parametros,
        action: 'subir_imagen_productos.php',
        onSubmit : function(file , ext){
        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
            // extensiones permitidas
            var mensaje;
            mensaje='<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error... Solo se permiten imagenes</strong></div>';
             $("#resultado").html(mensaje);
            // cancela upload
            return false;
        } else {
            $('#status_imagen3').html('Cargando...');
            this.disable();
        }
        },
        onComplete: function(file, response){
            // habilito upload button
            $('#status_imagen3').html(response);            
            this.enable();          
            // Agrega archivo a la lista
            var nom_arch=file;
            $('#imag_prod3').val(nom_arch);
            $('#imagen_cargada3').attr('src','../imagenes/uploads/productos/'+nom_arch);

            //html('.files').text(file);
        }   
    });
});


$(document).ready(function(){
    var parametros = {
            "direc" : "msg_imag"
    };
    var button = $('#subir_imagen');
    new AjaxUpload('#subir_imagen', {
        action: 'subir_imagen_productos.php',
        onSubmit : function(file , ext){
        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
            // extensiones permitidas
            var mensaje;
            mensaje='<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error... Solo se permiten imagenes</strong></div>';
             $("#resultado").html(mensaje);
            // cancela upload
            return false;
        } else {
            $('#status_imagen3').html('Cargando...');
            this.disable();
        }
        },
        onComplete: function(file, response){
            // habilito upload button
            $('#status_imagen3').html(response);            
            this.enable();          
            // Agrega archivo a la lista
            var nom_arch=file;
            $('#imag_prod3').val(nom_arch);
            $('#imagen_cargada3').attr('src','../imagenes/uploads/productos/'+nom_arch);

            //html('.files').text(file);
        }   
    });
});




 
</script>




<body>
    <?php
        include("menu_backend.php");
    ?>
<div data-offset-top="100" class="container" data-spy="affix">
  <div id="resultado"> </div>
</div>

<div class="jumbotron cajaancha">
      <div class="titulo_form">   Productos </div>


<div class="row">
                    <div class="col-md-6">   

                        <div class="form-group">
                           <label for="id_cate" class="etq_form">Categoria:</label>
                             <div id="grupo_id_cate" class="">  
                                <select name="id_cate" id="id_cate" class="validate[required], form-control">
                                <option value="" selected disabled style="display:none;">Seleccione la Categoria</option>
                                  <?php 
                                  //consulta  
                                  $consulta_categoria = mysql_query("SELECT * FROM categoria order by nomb_cate ");
                                  while($fila=mysql_fetch_array($consulta_categoria))
                                  {
                                     echo "<option  value=".$fila[id_cate].">".$fila[nomb_cate]."</option>";
                                  }
                                  ?>


                              </select>      

                                

                            </div>
                    </div>
                </div>
            <input type="hidden" value="sub_categoria::id_scate::id_cate::nomb_scate" name="llenado_id_scate" id="llenado_id_scate">
            <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">
            <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
              
            <input type="hidden" name="var_titulos_busqueda" id="var_titulos_busqueda" value='<?php echo $titulos_busqueda; ?>'>
             <input type="hidden" name="var_datos_busqueda" id="var_datos_busqueda" value='<?php echo $datos_busqueda; ?>'>
            <input type="hidden" name="var_nregistros" id="var_nregistros" value='<?php echo $nregistros; ?>'>
            <input type="hidden" name="var_pag_actual" id="var_pag_actual" value=''>
                 
                <div class="col-md-6">   

                        <div class="form-group">
                           <label for="id_scate" class="etq_form" > Sub-Categoria:</label>
                             <div id="grupo_id_scate" class="">  
                                <select name="id_scate" id="id_scate" class="validate[required], form-control">
                                <option value="" selected disabled style="display:none;">Debe Seleccionar una categoria</option>
                                   


                              </select>      

                               <span id="boton_id_scate" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                      <span id="actualiza_id_scate" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                               </span> 

                            </div>
                    </div>
                </div>    
</div>

               
  

                <div class="row">
                    <div class="col-md-6">   
                         <div class="form-group">
                            <label for="nomb_prod" class="etq_form" > Nombre:</label>
                            <div id="grupo_nomb_prod" class="input-group">
                                <input type="text" name="nomb_prod" id="nomb_prod" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[60]] text-input, form-control" placeholder="Nombre del Producto">
                                <span id="boton_nomb_prod" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="buscar()";>
                                    <span id="buscar_nomb_prod" title="Buscar" class="glyphicon glyphicon-search"> </span>
                                    <span id="actualiza_nomb_prod" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                 </span>                                  
                           </div>
                          </div>
                    </div>

                    <div class="col-md-6">   

                        <div class="form-group">
                          <label for="desc_prod" class="etq_form" >Descripci&oacute;n:</label>
                           <div id="grupo_desc_prod" class="">
                              <input type="text" name="desc_prod" id="desc_prod" class="validate[required, minSize[3], maxSize[120]] text-input, form-control" placeholder="Descripci&oacute;n del Producto">
                              <span id="boton_desc_prod" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                <span id="actualiza_desc_prod" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                              </span>
                           </div>
                        </div>  
                    </div>
                </div>   
                <div class="row">
                    <div class="col-md-6">   
                         <div class="form-group">
                            <label for="prec_prod" class="etq_form" > Peso:</label>
                              <div id="grupo_peso_prod" class="">
                                <input type="text" name="peso_prod" id="peso_prod" class="validate[required, custom[number], minSize[1], maxSize[9]] text-input, form-control" placeholder="Peso del Producto(Establecer en Gramos)">
                                <span id="boton_peso_prod" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                <span id="actualiza_peso_prod" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                              </span>
                              </div>
                          </div>
                    </div>
                    <div class="col-md-6">   
                         <div class="form-group">
                            <label for="id_marca" class="etq_form" > Marca:</label>
                            <div id="grupo_id_marca" class="">  

                                  <select name="id_marca" id="id_marca"  class="validate[required], form-control">
                                  <option value="" selected disabled style="display:none;">Seleccione la Marca</option>
                                    <?php 
                                    //consulta  
                                    $consulta_marca = mysql_query("SELECT * FROM marca order by nomb_marca ");
                                    while($fila=mysql_fetch_array($consulta_marca))
                                    {
                                       echo "<option  value=".$fila[id_marca].">".$fila[nomb_marca]."</option>";
                                    }
                                    ?>


                                </select>  
                                  <span id="boton_id_marca" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                      <span id="actualiza_id_marca" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                  </span>

                            </div>
                          </div>
                    </div>
 
                   
                </div>  
                <div class="row">
                    <div class="col-md-6">   
                         <div class="form-group">
                            <label for="prec_prod" class="etq_form" > Precio:</label>
                                <div id="grupo_prec_prod" class="">
                                   <input type="text" name="prec_prod" id="prec_prod" class="validate[required, custom[number], minSize[2], maxSize[10]], text-input, form-control" placeholder="Precio del Producto">
                                    <span id="boton_prec_prod" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                    <span id="actualiza_prec_prod" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                    </span>
                                </div>
                          </div>
                    </div>

                    <div class="col-md-6">   

                        <div class="form-group">
                          <label for="porc_desc" class="etq_form" >Descuento:</label>
                            <div id="grupo_porc_desc" class="">
                              <input type="text" name="porc_desc" id="porc_desc" class="validate[custom[number], minSize[1],  maxSize[3]], text-input, form-control" placeholder="Porcentaje del Descuento">
                               <span id="boton_porc_desc" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;">
                                  <span id="actualiza_porc_desc" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                </span>
                            </div>
                        </div>  
                    </div>
                </div> 
                 <div class="row">
                    <div class="col-md-6">   
                         <div class="form-group">
                            <label for="fechi_desc" class="etq_form" > Fecha Fin de Descuento:</label>
                            <div class="input-group" id="grupo_fechi_desc">
                                  <input type="text" name="fechi_desc" id="fechi_desc" class="validate[custom[date]] text-input es form-control input-append date datepicker" placeholder="Fecha fin de Descuento">
                                  <span id="boton_fechi_desc" class="input-group-addon"  style="visibility:visible; cursor:pointer; cursor: hand;">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                      <span id="actualiza_fechi_desc" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                  </span>
                            </div>
                          </div>
                    </div>

                    <div class="col-md-6">   
                         <div class="form-group">
                            <label for="fechf_desc" class="etq_form" > Fecha Inicio de Descuento:</label>
                            <div class="input-group" id="grupo_fechf_desc">
                                  <input type="text" name="fechf_desc" id="fechf_desc" class="validate[custom[date]] text-input es form-control input-append date datepicker" placeholder="Fecha Inicio de Descuento">
                                  <span id="boton_fechf_desc" class="input-group-addon"  style="visibility:visible; cursor:pointer; cursor: hand;">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                      <span id="actualiza_fechf_desc" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                  </span>
                            </div>
                          </div>
                    </div>
                </div>

 
               <div class="row">
                    <div class="col-md-6">   
                         <div class="form-group">
                            <label for="status_prod" class="etq_form" > Status:</label>
                             <div id="grupo_status_prod" class=""> 
                                  <select name="status_prod" id="status_prod"  class="validate[required], form-control">
                                  <option value="" selected disabled style="display:none;">Seleccione el Status del Producto</option>
                                     <option  value="Inactivo">Inactivo</option> 
                                    <option  value="Activo">Activo</option> 

                                </select>  

                                 <span id="boton_status_prod" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                        <span id="actualiza_status_prod" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                 </span> 

                              </div>
                          </div>
                    </div>

                    <div class="col-md-6">   

                        <div class="form-group">
                           <label for="tags_prod" class="etq_form" > Palabras Claves:</label>
                           <div id="grupo_tags_prod" class=""> 
                              <input type="text" name="tags_prod" id="tags_prod" class="validate[minSize[3], maxSize[100]] text-input, form-control" placeholder="Palabras de busqueda del Producto">
                               <span id="boton_tags_prod" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                 <span id="actualiza_tags_prod" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                               </span>
                           </div>
                    </div>
                </div>    
              </div>
                <div class="row">
                 <div class="col-md-6">  
                        <div class="form-group">
                          <label for="imag_prod" class="etq_form" > Imagen:</label>
                          <div id="grupo_imag_prod" class="input-group">
                                <span id="upload_button" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="";>
                                    <span  class="glyphicon glyphicon-picture"> </span> 
                                </span>
                              <input type="text" name="imag_prod" id="imag_prod" class="form-control"  placeholder="Subir Imagen">
                                 <span id="boton_imag_prod" class="input-group-addon"  style=" visibility:hidden; cursor:pointer; cursor: hand;" onclick="";>
                                     <span id="actualiza_imag_prod" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                </span>
                          </div>
                          <div id="status_imagen"></div>
                            <div class="text-center">
                                <img id="imagen_cargada" style="max-width:30%; border:1px solid #000;">                    
                             </div>
                        </div>
                  </div>
                   <div class="col-md-6"> 
                        <div class="form-group">
                          <label for="imag_prod1" class="etq_form" > Imagen1:</label>
                           <div id="grupo_imag_prod1" class="input-group">
                            <span id="upload_button1" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="";>
                                    <span  class="glyphicon glyphicon-picture"> </span> 
                                 </span>
                              <input type="text" name="imag_prod1" id="imag_prod1" class="form-control"  placeholder="Subir Imagen1">
                                <span id="boton_imag_prod1" class="input-group-addon"  style=" visibility:hidden; cursor:pointer;  cursor: hand;" onclick="";>
                                    
                                    <span id="actualiza_imag_prod1" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                </span>

                          </div>
                          <div id="status_imagen1"></div>
                          <div class="text-center">
                            <img id="imagen_cargada1" style="max-width:30%; border:1px solid #000; " src="" >                    
                          </div>
                        </div>
                   </div>
                  </div>
                <div class="row">
                 <div class="col-md-6">  
                        <div class="form-group">
                          <label for="imag_prod2" class="etq_form" > Imagen2:</label>
                          <div id="grupo_imag_prod2" class="input-group"> 
                          <span id="upload_button2" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="";>
                                    <span id="upload_button2" class="glyphicon glyphicon-picture"> </span> 
                            </span>
                             <input type="text" name="imag_prod2" id="imag_prod2" class="form-control"  placeholder="Subir Imagen2">
                            <span id="boton_imag_prod2" class="input-group-addon"  style=" visibility:hidden; cursor:pointer; cursor: hand;" onclick="";>
                                    <span id="actualiza_imag_prod2" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                            </span>
                          </div>
                          <div id="status_imagen2"></div>
                          <div class="text-center">
                             <img id="imagen_cargada2" style="max-width:30%; border:1px solid #000; " src="" >                    
                          </div>
                        </div>
                  </div>
                   <div class="col-md-6"> 
                        <div class="form-group">
                          <label for="imag_prod3" class="etq_form" > Imagen3:</label>
                          <div id="grupo_imag_prod3" class="input-group"> 
                              <span id="upload_button3" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="">
                                    <span  class="glyphicon glyphicon-picture"> </span> 
                              </span>
                              <input type="text" name="imag_prod3" id="imag_prod3" class="form-control"  placeholder="Subir Imagen3">
                              <span id="boton_imag_prod3" class="input-group-addon"  style=" visibility:hidden; cursor:pointer; cursor: hand;" onclick="">
                                     <span id="actualiza_imag_prod3" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                              </span>


                          </div>
                          <div id="status_imagen3"></div>
                          <div class="text-center">
                              <img id="imagen_cargada3" style="max-width:30%; border:1px solid #000; " src="" >                    
                          </div>
                        </div>
                    </div>
                  </div>

                <div class="row">
                    <div class="col-md-6">   
                         <div class="form-group">
                            <label for="condi_prod" class="etq_form" > Condición:</label>
                             <div id="grupo_condi_prod" class=""> 
                                  <select name="condi_prod" id="condi_prod"  class="validate[required], form-control">
                                  <option value="" selected disabled style="display:none;">Seleccione el Status del Producto</option>
                                     <option  value="0">Regular</option> 
                                    <option  value="1">Destacado</option>
                                    <option  value="2">Nuevo</option> 
                                    <option  value="3">Outlet</option> 

                                </select>  

                                 <span id="boton_condi_prod" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                        <span id="actualiza_condi_prod" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                 </span> 

                              </div>
                          </div>
                    </div>
              </div>
                  <?php $fecha_actual=date("Y-m-d") ?>
                 
                 <input type="hidden" name="fech_regi" id="fech_regi" value='<?php echo $fecha_actual; ?>'>


                <div align="center"><a href="<?php echo $_PHP_SELF; ?>"><button id="cancelar" type="button" class="btn btn_form oculto" >Cancelar</button></a>  <input type="submit" name="guardar"  id="guardar" value="Guardar" class="btn btn_form" > </div>
           
            </form>
          
           

</div>
 
 
<div class="row">
  <div id='resultado_busqueda' class="container">
    <?php 
      include("../comunes/busqueda.php");
    ?>
  </div>
</div>
  <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>