<?php
include("../comunes/variables.php");
include("../comunes/verificar_admin.php");
include("../comunes/conexion.php");
$tabla='sub_categoria';
$titulos_busqueda = 'Nombre Sub-categoria, Descripci&oacute;n, Categoria';
$datos_busqueda = 'nomb_scate,desc_scate,id_cate->categoria::id_cate::nomb_cate';
$nregistros = 5;    




?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    <title><?php echo $nom_pagina; ?></title>
  </head>
   <!-- validacion en vivo -->
<script>
  <!-- validacion en vivo -->
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
   });
</script>
<!-- CALENDARIO-->
<script>
    jQuery(document).ready(function(){
      // binds form submission and fields to the validation engine
      $( ".datepicker" ).datepicker();
      jQuery("form1").validationEngine();
    });
</script>
<!-- funcion de guardar formulario -->  
<script type="text/javascript">
$(function()
{
    $("#guardar").click(function()
    {
      if ($("#form1").validationEngine('validate')){
        var url="../comunes/funcion_guardar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: $("#form1").serialize(),
            success: function(data)
            {
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                $("#form1")[0].reset();
              }
              $("#resultado").html(mensaje);
              setTimeout(function() {
                $("#msg_act").fadeOut(1500);
              },3000);
              buscar();
            }
        });
        return false;
      }
    });
});
function buscar()
{
  var url="../comunes/busqueda.php"; 
  $.ajax
  ({
    type: "POST",
    url: url,
    data: $("#form1").serialize(),
    success: function(data)
    {
      $("#resultado_busqueda").html(data);
    }
  });
  return false; 
}

</script>


<body>
    <?php
        include("menu_backend.php");
    ?>
<div data-offset-top="100" class="container" data-spy="affix">
  <div id="resultado"></div>
</div>
<div class="jumbotron cajalogin">
      <div class="titulo_form">   Sub-Categorias </div>
            <form method="POST" name="form1" id="form1" onsubmit="return jQuery(this).validationEngine('validate');">

                <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
                <input type="hidden" name="var_titulos_busqueda" id="var_titulos_busqueda" value='<?php echo $titulos_busqueda; ?>'>
                <input type="hidden" name="var_datos_busqueda" id="var_datos_busqueda" value='<?php echo $datos_busqueda; ?>'>
                <input type="hidden" name="var_nregistros" id="var_nregistros" value='<?php echo $nregistros; ?>'>
                <input type="hidden" name="var_pag_actual" id="var_pag_actual" value=''>  
                 

 

                 <div class="form-group">
                    <label for="nomb_scate" class="etq_form" > Nombre:</label>
                       <div   id="grupo_nomb_scate" class="input-group">
                           <input type="text" name="nomb_scate" id="nomb_scate" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[30]] text-input, form-control" placeholder="Nombre de Sub-Categoria">
                           <span id="boton_nomb_scate" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="buscar()";>
                                    <span id="buscar_nomb_scate" title="Buscar" class="glyphicon glyphicon-search"> </span>
                                    <span id="actualiza_nomb_scate" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                           </span>       
                        </div>
                    </div>
                
 
                  <div class="form-group">
                    <label for="desc_cate" class="etq_form" > Descripci&oacute;n:</label>
                    <div id="grupo_desc_scate" class="">
                        <input type="text" name="desc_scate" id="desc_scate" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[60]] text-input, form-control" placeholder="Descripci&oacute;n de Sub-Categoria">
                        <span id="boton_desc_scate" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                          <span id="actualiza_desc_scate" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                        </span>
                    </div>
                  </div>           
                <div class="form-group">
                    <label for="id_empr_envi" class="etq_form" >Categoria:</label>
                    <div id="grupo_id_cate" class="">

                        <select name="id_cate" id="id_cate"  class="validate[required], form-control">
                          <option value="" selected disabled style="display:none;">Seleccione la Categoria</option>
                            <?php 
                            //consulta  
                            $consulta_categoria = mysql_query("SELECT * FROM categoria order by nomb_cate ");
                            while($fila=mysql_fetch_array($consulta_categoria))
                            {
                               echo "<option  value=".$fila[id_cate].">".$fila[nomb_cate]."</option>";
                            }
                            ?>


                        </select>
                          <span id="boton_id_cate" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                          <span id="actualiza_id_cate" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                        </span>
                      </div>
                  </div>

                <div align="center"><a href="<?php echo $_PHP_SELF; ?>"><button id="cancelar" type="button" class="btn btn_form oculto" >Cancelar</button></a>  <input type="submit" name="guardar"  id="guardar" value="Guardar" class="btn btn_form" > </div>
           
            </form>
          
           

</div>

<div class="row">
  <div id='resultado_busqueda' class="container">
    <?php 
      include("../comunes/busqueda.php");
    ?>
  </div>
</div>
 

  <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>

