<?php

ini_set('post_max_size','10M');
ini_set('upload_max_filesize','10M');
ini_set('max_execution_time','99000');
ini_set('max_input_time','99000');

// defino la carpeta para subir
$uploaddir = '../imagenes/uploads/colores/';
// defino el nombre del archivo
$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
$res = 'dds';
// Lo mueve a la carpeta elegida
if ($res = move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
  //echo "<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Imagen Cargada Exitosamente</strong></div>";
} else {
  echo $res;
  //echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error al subir imagen</strong></div>";
}
?>