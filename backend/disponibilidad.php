<?php
include("../comunes/variables.php");
include("../comunes/verificar_admin.php");
include("../comunes/conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
    <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    <title><?php echo $nom_pagina; ?></title>
      <script type="text/javascript">
        function pasar_modal(id,tipo,producto,cant_prod)
             {
              
                $("#id_prod_deta").val(id);
                $("#tipo"). html(tipo);

                $("#var_tipo_carga").val(tipo);
                $("#tipo_carga").val(tipo);


                $("#producto").html(producto);
                 $("#var_cant_prod").val(cant_prod);
             } 
              function consulta_inventario(id,producto)
             {
                //para verificar si es una fecha voltearla al momento de guardarla
                  var resultado;
                  var parametros = {
                    "id": id
                  
                  };
                  var url="ver_movimientos_inventario.php"; 
                  $("#nomb_producto").html(producto);
                  $.ajax
                  ({
                    type: "POST",
                      url: url,
                      data: parametros,

                      success: function(data)
                      {
                         $("#contenido_modal2").html(data);

                      }
                  });
             }
      </script>
    </head>
<body>
  <?php include("menu_backend.php"); ?>
  <div data-offset-top="100" class="container" data-spy="affix">
    <div id="resultado"> </div>
  </div>
  <div class="container">
    <div class="jumbotron">
      <a href="disponible_excel.php"><button id="xls" type="button" class="btn btn-success pull-right" style="margin-top: -3em;">Descargar XLS<div class="glyphicon glyphicon-floppy-save" style="padding-left: 1em;"></div></button></a>
      <div class="titulo_form">   Disponibilidad en Inventario </div>
      <div class="row">
        <?php 
          $consulta_productos="SELECT * FROM  producto_detalles as pd , productos as p, tallas  as t, colores as c, categoria as ca, sub_categoria as sc where p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color and sc.id_scate=p.id_scate and ca.id_cate=sc.id_cate order by p.nomb_prod ASC";
          $consulta_productos=mysql_query($consulta_productos);
          echo '<table class="table table-striped">';
          echo '<tr><th>#</th> <th>Producto</th> <th> Categoria </th> <th>Subcategoria</th> <th>Color</th> <th> Talla</th> <th> Disponible </th> <th> Apartado </th>  <th> Vendido </th> <th colspan="3" class="text-center" > Acciones </th> </tr>';
          while ($fila=mysql_fetch_array($consulta_productos))
          {
            $i++;
            $consulta_disponibilidad="SELECT sum(cant_venta_prod) as suma_prod FROM producto_detalles as pd, venta_productos as vp, venta as v where pd.id_prod_deta='$fila[id_prod_deta]' and vp.id_prod_deta=pd.id_prod_deta and v.id_venta=vp.id_venta and  (v.status_venta='carrito' or v.status_venta='procesado' or v.status_venta='pagado' or v.status_venta='confirmado')";
            $con_dis=mysql_fetch_assoc(mysql_query($consulta_disponibilidad));
            $producto_apartado=$con_dis[suma_prod];
            if ($producto_apartado=='') { $producto_apartado='0'; }
            $consulta_vendidos="SELECT sum(cant_venta_prod) as suma_prod FROM producto_detalles as pd, venta_productos as vp, venta as v where pd.id_prod_deta='$fila[id_prod_deta]' and  vp.id_prod_deta=pd.id_prod_deta and v.id_venta=vp.id_venta and (v.status_venta='enviado' or v.status_venta='entregado'  )";
            $con_ven=mysql_fetch_assoc(mysql_query($consulta_vendidos));
            $producto_vendido=$con_ven[suma_prod];
            if ($producto_vendido=='') { $producto_vendido='0'; }
            $producto=$fila[nomb_prod].' para '.$fila[nomb_cate].' talla '.$fila[nomb_talla].' '.$fila[nom_color]; 
            echo '<tr><td>'.$i.'</td><td>'.$fila[nomb_prod].'</td><td>'.$fila[nomb_cate].'</td><td>'.$fila[nomb_scate].'</td><td>'.$fila[nom_color].'</td><td>'.$fila[nomb_talla].'</td><td align="right">'.$fila[cant_prod].'</td><td align="right">'.$producto_apartado.'</td><td align="right">'.$producto_vendido.'</td>';              
            echo '<td align="right"><button class="btn btn-success btn-xs" title="Cargar de Inventario" data-toggle="modal" data-target="#carga" onclick="pasar_modal('.$fila[id_prod_deta].',\'Carga\',\''.$producto.'\','.$fila[cant_prod].')" id="inventario"> <span class="glyphicon glyphicon-plus"  aria-hidden="true"> </button></td>';
            echo '<td align="right"><button class="btn btn-danger btn-xs" title="Descargar de Inventario" data-toggle="modal" data-target="#carga" onclick="pasar_modal('.$fila[id_prod_deta].',\'Descarga\',\''.$producto.'\','.$fila[cant_prod].')" id="inventario"> <span class="glyphicon glyphicon-minus"  aria-hidden="true"> </button></td>';
            echo '<td align="right"><button class="btn btn-info btn-xs" title="Ver movimientos de Inventario" data-toggle="modal" data-target="#ver" onclick="consulta_inventario('.$fila[id_prod_deta].',\''.$producto.'\')" id="ver_detalle"> <span class="glyphicon glyphicon-search"  aria-hidden="true"> </button></td>';
            echo '</tr>';
          }
        ?>
      </div>
    </div>
  </div>
  <!-- Ver detalle de Inventario -->
  <div class="modal fade" id="ver" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 900px">
      <div class="modal-content">
        <div class="modal-body">
          <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
           <button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -15px;  margin-right: -9px;">×</button>
              <div class="titulo_form"> Historial de Ajustes de Inventario </div>
              <br>
              <b>
              <div id="nomb_producto" class="text-center"> </div>
              </b>
              <br>
                <div id="contenido_modal2" style="height: 400px; overflow: auto;">
          </div>
        </div>
      </div>  
    </div> 
  </div>
  <!-- Modal para cargar y descargar de inventario -->
  <div class="modal fade" id="carga" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 400px">
      <div class="modal-content">
        <div class="modal-body">
          <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
          <div id="contenido_modal">
            <?php 
              include('cargar.php');
            ?>
          </div>
        </div>
      </div>  
    </div> 
  </div>
  <script src="../bootstrap/js/bootstrap.min.js"> </script>
</body>
</html>
