<?php
include("../comunes/variables.php");
include("../comunes/verificar_admin.php");
include("../comunes/conexion.php");



?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
    <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    <title><?php echo $nom_pagina; ?></title>
  </head>



<body>
    <?php
        include("menu_backend.php");
    ?>
<div data-offset-top="100" class="container" data-spy="affix">
  <div id="resultado"> </div>
</div>

<div class="container">

          <div class="titulo_form">   Historico de Compras </div>
               
                   
                    <div class="row">


        <?php    

        $consulta="SELECT * FROM venta as v  where  v.status_venta!='procesado' and v.status_venta!='carrito' order by v.id_venta ";
        $consulta=mysql_query($consulta);

        $i=0;
        while($fila=mysql_fetch_array($consulta))
        {
          $icono = '';
          $totalp=0;
          $total_pagar=0;
          $total_cant=0;
          $i++;

          if ($fila[status_venta]=='pagado')
          {

            $icono= '<button style="width:13em; background-color: #00b6ce; border: 0px;" type="button" class="btn btn-warning" title="En espera de confirmación de pago" > <span class="pull-left"> Por Confirmar Pago</span> <span class="vineta-pago" aria-hidden="true"></span></button> ';
            
          }
           if ($fila[status_venta]=='confirmado')
          {

            $icono= '<button style="width:13em; background-color: #ae4f9e; border: 0px;" type="button" class="btn btn-info" title="Pago Confirmador. En espera de envío" ><span class="pull-left"> Por Enviar</span> <span class="vineta-enviar" aria-hidden="true"></span></button> ';
          }
          if ($fila[status_venta]=='enviado')
          {
            $icono= '<button style="width:13em; background-color: #ef4a7c; border: 1px;" type="button" class="btn btn-primary" title="Enviado" > <span class="pull-left">Enviado</span> <span class="vineta-enviado" aria-hidden="true"></span></button> ';
            $consulta_envio="SELECT * FROM envios, empresa_envio where envios.id_venta=$fila[id_venta] and empresa_envio.id_empr_envi=envios.id_empr_envi";
            $con_envio= mysql_fetch_assoc(mysql_query($consulta_envio));
          }
           if ($fila[status_venta]=='entregado')
          {

            $icono= '<button style="width:13em; background-color: #3e658b; border: 1px;" type="button" class="btn btn-success" title="Entregado al cliente" > <span class="pull-left">Entregado</span> <span class="vineta-compras" aria-hidden="true"></span></button> ';
          }
                   $consulta2="SELECT * FROM venta_productos as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
                   vp.id_venta='$fila[id_venta]' and vp.status_vent='procesado' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
                  $consulta2=mysql_query($consulta2);
                  while ($fila2=mysql_fetch_array($consulta2)) 
                  {
                     $totalp=$fila2[vuni_venta_prod]*$fila2[cant_venta_prod];
                     $total_pagar += $totalp;
                     $total_cant += $fila2[cant_venta_prod];
                     $peso_cu=$fila2[peso_prod] * $fila2[cant_venta_prod];
                     $total_peso += $peso_cu;

                      $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
                      $consulta_envio=mysql_query($consulta_envio);
                      $con_env=mysql_fetch_assoc($consulta_envio);

                      if (!$con_env[id_tenvios])
                      {

                            $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                            $consulta_envio=mysql_query($consulta_envio);
                            $con_env=mysql_fetch_assoc($consulta_envio);

                            $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);

                      }


                    }

                  
                    echo '<div class="table-responsive procesar_linea_punteada">';
                    echo '<table class="table table-striped table-hover" width="100%">
                    <tr><th class="fondo_predefinido"># Orden </th> <th class="fondo_predefinido">Cantidad de Productos </th> <th class="fondo_predefinido"> Monto a Cancelar </th>';    if ($fila[status_venta]=='enviado') { echo '<th class="fondo_predefinido"> Empresa de Envio </th> <th class="fondo_predefinido"> N° de Guia </th> <th class="fondo_predefinido"> Fecha de Envio </th>'; }  echo '<th class="fondo_predefinido text-center" style="width: 240px;"> Status </th> </tr>';
       
                  echo '<tr><td>'.$fila[codg_trans].' </td> <td>'.$total_cant.'</td> <td>'.number_format(($total_pagar+$con_env[prec_tenvio]+($total_pagar * ($con_env[porc_tenvio]/100))),2,",",".").'</td>';  if ($fila[status_venta]=='enviado') {echo '<td>'.$con_envio[nomb_empr_envi].'</td><td>'.$con_envio[guia_envio].'</td> <td>'.$con_envio[fech_envio].'</td>';  } echo '<td class="text-right"> '.$icono.'</button> </td> </tr>';
                  echo '</table>';
                
                  

        }


?>

                       
                    </div>    

</div>

 
 

  <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>

