<?php
include("../comunes/variables.php");
include("../comunes/verificar_admin.php");
include("../comunes/conexion.php");

$corre_user=$_POST['corre_user'];

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>

    <script type="text/javascript">

    $(function()
{
    $("#guardar").click(function()
    {
      if ($("#form2").validationEngine('validate')){
        var url="guardar_cupones.php"; 
        //alert ($("#id_user1").val());
        $.ajax
        ({

            type: "POST",
            url: url,
            data: $("#form2").serialize(),
            beforeSend: function () 
                {
                    $("#procesando").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, <br>Espere por favor...</td><td><img valign='middle' src='../imagenes/acciones/cargando_gray.gif' width='30px'></td></tr></table>");
                },
            success: function(data)
            {

            	
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                $("#form2")[0].reset();
              }
              $("#resultado").html(mensaje);
              setTimeout(function() {
                $("#msg_act").fadeOut(1500);
              },3000); 
            
              $("#procesando").html("");
               $("#resultado_usuarios").html('');
            }
        });
        return false;
      }
    });
});

    $(document).ready(function() {
        $('.datepicker')
            .datepicker({
              format: 'dd-mm-yyyy',
              autoclose: true,
              language: 'es'
            });
    });
    </script>
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <!-- validacion en vivo -->
<script >
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form2").validationEngine('attach', {bindMethod:"live"});
   });
</script>

<body>


<?php

if ($corre_user!='')
{
	$consulta="SELECT * FROM usuarios where corre_user='$corre_user' and tipo_user='2'";
}
else
{
	$consulta="SELECT * FROM usuarios where tipo_user='2'";

}

	$con=mysql_query($consulta);

	if (mysql_num_rows($con)>0)
	{
    $fechaa=date('d-m-Y');
		echo '<div class="jumbotron cajalogin">
      		<div class="titulo_form"> Datos Cup&oacute;n de Descuento  </div>';
          

                 echo '<br><div class="form-group">
                    
                     <input type="text" name="valor_cupon" id="valor_cupon" class="validate[required, custom[number], minSize[2], maxSize[60]] text-input, form-control" placeholder="Valor del Cup&oacute;n en Bs.">
                     
                  </div>';
                  echo '<div class="form-group">
                    
                     <input type="text" name="moti_cupon" id="moti_cupon" class="validate[required, minSize[3], maxSize[100] text-input, form-control" placeholder="Motivo del Cup&oacute;n">
                     
                  </div>';

                    echo '<div class="form-group">
                    
                       <input type="text" name="ffin_cupon" id="ffin_cupon" class="validate[custom[date], future['.$fechaa.']] text-input es form-control input-append date datepicker" placeholder="Fecha fin del Cup&oacute;n">
                     
                  </div>';

                  $fecha=date(Y).'-'.date(m).'-'.date(d);
        		   echo '<input type="hidden" name="fstat_cupon" id="fstat_cupon" value="'.$fecha.'" >';
        		   echo '<input type="hidden" name="stat_cupon" id="stat_cupon" value="enviado" >';


        echo '</div>';


        echo '<div class="container">';
	echo '<div class="titulo_form">Usuarios a Asignar Cup&oacute;n</div>';
	echo '<div class="table-responsive">';
	echo '<table class="table table-striped table-bordered table-hover table-condensed">';
	echo '<tr class="info">';
	echo '<th>N°</th> <th>Nombre y Apellidos</th> <th>Correo</th> <th>Seleccionar</th> ';
	
	echo '</tr>';
	
 	$i=0;
	while($fila=mysql_fetch_array($con)) 
	{

			$i++;
			echo '<tr> <td>'.$i.' </td> <td>'.$fila[nom_ape_user].'</td> <td>'.$fila[corre_user].'</td> <td> <input type="checkbox" name="id_user'.$i.'" id="id_user'.$i.'"  value="'.$fila[id_user].'" > </td> </tr>';
      echo '<input type="hidden" name="correou'.$i.'" id="correou'.$i.'" value="'.$fila[corre_user].'" >';

		
	}
	    echo '<input type="hidden" name="i" id="i" value="'.$i.'" >';

	
	echo '</table>';
	echo '</div>';
	echo '</div>';
	           echo '<div align="center"> <button  id="guardar" class="btn btn_form" title="Asignar Cupones" style="color:#FFFFFF">Asignar<span id="procesando"></span></button> </div> <br><br><br>';

        echo '<div data-offset-top="100" class="container" data-spy="affix">
          <div id="resultado"> </div>
        </div>';

	}

	else
	{

		echo '<div class="titulo_form">Registros No Encontrados</div>';


	}
	

	



?>



 <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>