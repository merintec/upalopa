<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <title><?php echo $nom_pagina; ?></title>
  </head>
<body>
<div class="row text-center text-info">
	<div class="hidden-xs col-md-1"> &nbsp;</div>
	<div class="col-xs-3 col-md-1" style="padding: 0px; font-size: 0.8em;" onclick="ira_cesta();" title="Productos en tu Cesta de Compra" > <img src="../imagenes/sistema/logo_tn_off.png" width="60em" height="60em" class="procesar_ico_on" id="ico_cesta" ><br>Cesta</div>
	<div class="hidden-xs col-md-2">  <hr class="procesar_linea_punteada"></div>
	<div id="btn_fact" class="col-xs-3 col-md-1" style="padding: 0px; font-size: 0.8em;" onclick="ira_facturacion();" title="Datos de Facturación"> <img src="../imagenes/sistema/logo_tn_off.png" width="60em" height="60em"  class="procesar_ico_on" id="ico_factura"/><br>Factura</div>
	<div class="hidden-xs col-md-2">  <hr class="procesar_linea_punteada"></div>
	<div id="btn_envi" class="col-xs-3 col-md-1" style="padding: 0px; font-size: 0.8em;" onclick="ira_envio();" title="Dirección de Envío"> <img src="../imagenes/sistema/logo_tn_off.png" width="60em" height="60em"  class="procesar_ico_on" id="ico_envio"/><br>Envio</div>
	<div class="hidden-xs col-md-2">  <hr class="procesar_linea_punteada"></div>
  	<div id="btn_resu" class="col-xs-3 col-md-1" style="padding: 0px; font-size: 0.8em;" onclick="ira_resumen();" title="Resumen de Compra"> <img src="../imagenes/sistema/logo_tn_off.png" width="60em" height="60em" class="procesar_ico_on" id="ico_resumen"/> <br>Resumen</div>
	<div class="hidden-xs col-md-1"> &nbsp; </div>
</div>

 </body>
</html>
