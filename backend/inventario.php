<?php
include("../comunes/variables.php");
include("../comunes/verificar_admin.php");
include("../comunes/conexion.php");
$tabla='producto_detalles';
$titulos_busqueda = 'Producto, Categoria,Subcategoria, Color, Talla, Cantidad, Fecha';
$datos_busqueda = 'id_prod->productos::id_prod::nomb_prod,id_prod->productos::id_prod::id_scate->sub_categoria::id_scate::id_cate->categoria::id_cate::nomb_cate,id_prod->productos::id_prod::id_scate->sub_categoria::id_scate::nomb_scate,id_color->colores::id_color::nom_color,id_talla->tallas::id_talla::nomb_talla,cant_prod,fech_actu';
$nregistros = 100;      


?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
    <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <!-- validacion en vivo -->
<script >
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
   });
</script>
<!-- CALENDARIO-->
<script>
    jQuery(document).ready(function(){
      // binds form submission and fields to the validation engine
      $( ".datepicker" ).datepicker();
      jQuery("form1").validationEngine();
    });
</script>
<!-- funcion de guardar formulario -->  
<script type="text/javascript">
$(function()
{
    $("#guardar").click(function()
    {
      if ($("#form1").validationEngine('validate')){
        var url="../comunes/funcion_guardar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: $("#form1").serialize(),
            success: function(data)
            {
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                $("#form1")[0].reset();
              }
              $("#resultado").html(mensaje);
              setTimeout(function() {
                $("#msg_act").fadeOut(1500);
              },3000);
              buscar();
            }
        });
        return false;
      }
    });
});
function buscar()
{
  var url="../comunes/busqueda.php"; 
  $.ajax
  ({
    type: "POST",
    url: url,
    data: $("#form1").serialize(),
    success: function(data)
    {
      $("#resultado_busqueda").html(data);
    }
  });
  return false; 
}



//para el combo de subcategoria

 
    $(document).ready(function() {
    // Parametros para el combo
     $("#id_cate").change(function () {
        $("#id_cate option:selected").each(function () {
          id_cate=$(this).val();
          $.post("combo_subcategoria.php", { id_cate: id_cate }, function(data){
         // $("#prueba").html(data);
          $("#id_scate").html(data);
        });     
       });
     });    
  });



//para el combo de productos

 
    $(document).ready(function() {
    // Parametros para el combo
     $("#id_scate").change(function () {
        $("#id_scate option:selected").each(function () {
          id_scate=$(this).val();
          $.post("combo_productos.php", { id_scate: id_scate }, function(data){
          $("#id_prod").html(data);
        });     
       });
     });    
  });
 
</script>
 



<body>
    <?php
        include("menu_backend.php");
    ?>
<div data-offset-top="100" class="container" data-spy="affix">
  <div id="resultado"> </div>
</div>

<div class="jumbotron cajaancha">
      <div class="titulo_form">   Inventario </div>
           
               
                <div class="row">
                    <div class="col-md-6">   

                        <div class="form-group">
                           <label for="id_cate" class="etq_form">Categoria:</label>
                             <div id="grupo_id_cate" class="">  
                                <select name="id_cate" id="id_cate" class="validate[required], form-control">
                                <option value="" selected disabled style="display:none;">Seleccione la Categoria</option>
                                  <?php 
                                  //consulta  
                                  $consulta_categoria = mysql_query("SELECT * FROM categoria order by nomb_cate ");
                                  while($fila=mysql_fetch_array($consulta_categoria))
                                  {
                                     echo "<option  value=".$fila[id_cate].">".$fila[nomb_cate]."</option>";
                                  }
                                  ?>


                              </select>      

                               <span id="boton_id_cate" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                      <span id="actualiza_id_cate" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                               </span> 

                            </div>
                    </div>
                </div>    


 

 
                    <div class="col-md-6">   

                        <div class="form-group">
                           <label for="id_scate" class="etq_form" > Sub-Categoria:</label>
                             <div id="grupo_id_scate" class="">  
                                <select name="id_scate" id="id_scate" class="validate[required], form-control">
                                <option value="" selected disabled style="display:none;">Debe seleccionar Categoría</option>
                                 


                              </select>      

                               <span id="boton_id_scate" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                      <span id="actualiza_id_scate" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                               </span> 

                            </div>
                    </div>
                </div>    
              </div>
 
               <input type="hidden" value="productos::id_prod::id_scate::nomb_prod" name="llenado_id_prod" id="llenado_id_prod">
               <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">
                <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
                 <input type="hidden" name="var_titulos_busqueda" id="var_titulos_busqueda" value='<?php echo $titulos_busqueda; ?>'>
                 <input type="hidden" name="var_datos_busqueda" id="var_datos_busqueda" value='<?php echo $datos_busqueda; ?>'>
                <input type="hidden" name="var_nregistros" id="var_nregistros" value='<?php echo $nregistros; ?>'>
                <input type="hidden" name="var_pag_actual" id="var_pag_actual" value=''>

               <div class="row">
                    <div class="col-md-6">   

                        <div class="form-group">
                           <label for="id_prod" class="etq_form">Productos:</label>
                             <div id="grupo_id_prod" class="">  
                                <select name="id_prod" id="id_prod" class="validate[required], form-control">
                                <option value="" selected disabled style="display:none;">Debes seleccionar Sub-Categoría</option>
                                 

                              </select>      

                               <span id="boton_id_prod" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                      <span id="actualiza_id_prod" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                               </span> 

                            </div>
                    </div>
                </div>    
 
                <div class="col-md-6">   

                        <div class="form-group">
                           <label for="id_color" class="etq_form" > Color:</label>
                                <div id="grupo_id_color" class="">  
                                  <select name="id_color" id="id_color"  class="validate[required], form-control">
                                    <option value="" selected disabled style="display:none;">Seleccione el Color</option>
                                      <?php 
                                      //consulta  
                                      $consulta_color = mysql_query("SELECT * FROM colores order by nom_color ");
                                      while($fila=mysql_fetch_array($consulta_color))
                                      {
                                         echo "<option  value=".$fila[id_color].">".$fila[nom_color]."</option>";
                                      }
                                      ?>


                                  </select>  
                                  <span id="boton_id_color" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                    <span id="actualiza_id_color" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                  </span> 
                                </div>     
                    </div>
                </div>
                </div>    
                <div class="row">
                    <div class="col-md-6">   
                         <div class="form-group">
                            <label for="id_talla" class="etq_form" > Talla:</label>
                            <div id="grupo_id_talla" class="">  
                                  <select name="id_talla" id="id_talla"  class="validate[required], form-control">
                                  <option value="" selected disabled style="display:none;">Seleccione la Talla</option>
                                    <?php 
                                    //consulta  
                                    $consulta_tallas = mysql_query("SELECT * FROM tallas order by nomb_talla ");
                                    while($fila=mysql_fetch_array($consulta_tallas))
                                    {
                                       echo "<option  value=".$fila[id_talla].">".$fila[nomb_talla]."</option>";
                                    }
                                    ?>


                                </select>  
                            

                             <span id="boton_id_talla" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                    <span id="actualiza_id_talla" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                             </span> 
                            </div>

                          </div>
                    </div>
 
                      <div class="col-md-6">   

                        <div class="form-group">
                          <label for="cant_prod" class="etq_form" >Cantidad:</label>
                          <div id="grupo_cant_prod" class="">
                             <input type="text" name="cant_prod" id="cant_prod" class="validate[required, custom[integer], minSize[1], maxSize[11]] text-input, form-control" placeholder="Cantidad del Producto">
                               <span id="boton_cant_prod" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                 <span id="actualiza_cant_prod" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                               </span>
                          </div>
                        </div>  
                    </div>
                </div>  
               
                                  <?php $fecha_actual=date("Y-m-d") ?>    
                 <input type="hidden" name="fech_actu" id="fech_actu" value='<?php echo $fecha_actual; ?>'>


                <div align="center"><a href="<?php echo $_PHP_SELF; ?>"><button id="cancelar" type="button" class="btn btn_form oculto" >Cancelar</button></a>  <input type="submit" name="guardar"  id="guardar" value="Guardar" class="btn btn_form" > </div>
           
            </form>
          
           

</div>
 
 
<div class="row">
  <div id='resultado_busqueda' class="container">
    <?php 
      include("../comunes/busqueda.php");
    ?>
  </div>
</div>
 
 

  <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>

