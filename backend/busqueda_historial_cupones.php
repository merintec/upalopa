<?php
include("../comunes/variables.php");
include("../comunes/verificar_admin.php");
include("../comunes/conexion.php");

$corre_user=$_POST['corre_user'];
$fstat_cupon=$_POST['fstat_cupon'];
$fstat_cupon1=$_POST['fstat_cupon1'];
$status=$_POST['status'];



?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>

    <script type="text/javascript">

  function eliminar(valor_id,correo,nombre,sexo,num_cupon)
  {
    var tabla='cupones_descuentos';
    var campo='stat_cupon'; 
    var campo_id='id_cupon';
    var valor='eliminado';
    actualizar_dato_mp(tabla,campo,valor,campo_id,valor_id);
    campo='fstat_cupon';
    valor=<?php echo date("Y-m-d"); ?>;
    actualizar_dato_mp(tabla,campo,valor,campo_id,valor_id);
    enviar_correo_mp(nombre,correo,sexo,num_cupon);
    $('#resultado_usuarios').html('');
  }


   function enviar_correo_mp(nombre,correo,sexo,num_cupon)
    {
              var correo=correo;
              var nom_ape_user=nombre;
              var sexo = sexo;
              var num_cupon = num_cupon;
              var url="../comunes/enviar_correo.php";
              var sex;
              if (sexo=='F'){
                sex = 'a';
              }
              else {
                sex = 'o';  
              }  
              var mensaje = "Estimad" + sex + " " + nom_ape_user + ", <p style='padding-top: 20px;'>Por motivo de nuestras Politicas Hemos tenido que Revocar  el Cupón de Descuento N° <b>" + num_cupon + "</b> Disculpa las molestias ocacionadas </p><p style='padding-top: 20px;'>Gracias por preferirnos <a href='http://upalopa.com'>Upalopa.com</a></p>";
              var parametros = {
                  "mensaje" : mensaje,
                  "destino" : correo, 
                  "correo_origen" : "<?php echo $correo_compras; ?>",
                  "titulo" : "Cupón Revocado"
                };
              $.ajax
              ({
                  type: "POST",
                  url: url,
                  data: parametros,
                  success: function(data)
                  {
                     
                  }
              });
    }

  function actualizar_dato_mp(tabla,campo,valor,campo_id,valor_id)
    {
        //para verificar si es una fecha voltearla al momento de guardarla
          var resultado;
          var parametros = {
            "var_tabla": tabla,
            "var_campo" : campo,
            "var_valor" : valor,
            "var_id" : campo_id,
            "var_id_val" : valor_id
          };
          var url="../comunes/funcion_actualizarcampo.php"; 
          $.ajax
          ({
            type: "POST",
              url: url,
              data: parametros,
              success: function(data)
              {

              }
          });
    }

    $(document).ready(function() {
        $('.datepicker')
            .datepicker({
              format: 'dd-mm-yyyy',
              autoclose: true,
              language: 'es'
            });
    });
    </script>
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <!-- validacion en vivo -->
<script >
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form2").validationEngine('attach', {bindMethod:"live"});
   });
</script>

<body>


<?php
      //evaluando las fechas sino para asignarle alguna
      $fechaa=date('d-m-Y');


      if ($fstat_cupon=='')
      {
        $fstat_cupon='01-01-2000';
      }
      if ($fstat_cupon1=='')
      {
        $fstat_cupon1=$fechaa;
      }


      //convirtiendo las fechas
      $nueva = explode('-', $fstat_cupon);
      if (strlen($fstat_cupon) == 10 && strlen($nueva[0]) == 2 && strlen($nueva[1]) == 2 && strlen($nueva[2]) == 4){
          $fstat_cupon = $nueva[2].'-'.$nueva[1].'-'.$nueva[0];
        }

        $nueva2 = explode('-', $fstat_cupon1);
      if (strlen($fstat_cupon1) == 10 && strlen($nueva2[0]) == 2 && strlen($nueva2[1]) == 2 && strlen($nueva2[2]) == 4){
          $fstat_cupon1 = $nueva2[2].'-'.$nueva2[1].'-'.$nueva2[0];
        }

      

   $consulta="SELECT * FROM cupones_descuentos as cd, usuarios where usuarios.corre_user LIKE '%$corre_user%' and usuarios.tipo_user='2' and cd.id_user=usuarios.id_user and cd.stat_cupon LIKE '%$status%' having (cd.fech_asig_cupon>='$fstat_cupon' and cd.fech_asig_cupon<='$fstat_cupon1' ) ";


	$con=mysql_query($consulta);

  echo '<div data-offset-top="100" class="container" data-spy="affix">
          <div id="resultado"> </div>
        </div>';

	if (mysql_num_rows($con)>0)
	{

echo '<div class="container">';
	echo '<div class="titulo_form">Usuarios a Asignar Cup&oacute;n</div>';
	echo '<div class="table-responsive">';
	echo '<table class="table table-striped table-bordered table-hover table-condensed">';
	echo '<tr class="info">';
	echo '<th>N°</th> <th>Nombre y Apellidos</th> <th>Correo</th> <th>N° de Cupón</th> <th>Valor Cupón</th>  <th>Fecha de Asignación</th>  <th>Status</th> <th>Acción</th>  ';
	
	echo '</tr>';
	
 	$i=0;
  echo '<form method="POST" name="form2" id="form2">';

	while($fila=mysql_fetch_array($con)) 
	{

     if ($fila[stat_cupon]=='enviado')
          {

            $icono= '<button style="width:13em; background-color: #00b6ce; border: 0px;" type="button" class="btn btn-warning" title="Enviado" > <span class="pull-center"> Enviado</span> </button> ';
            
          }
           if ($fila[stat_cupon]=='utilizado')
          {

            $icono= '<button style="width:13em; background-color: #ae4f9e; border: 0px;" type="button" class="btn btn-info" title="Utilizado" ><span class="pull-center"> Utilizado</span> </button> ';
          }
          if ($fila[stat_cupon]=='eliminado')
          {
            $icono= '<button style="width:13em; background-color: #ef4a7c; border: 1px;" type="button" class="btn btn-primary" title="Enviado" > <span class="pull-center">Eliminado</span> </button> ';
            
          }
           if ($fila[stat_cupon]=='vencido')
          {

            $icono= '<button style="width:13em; background-color: #3e658b; border: 1px;" type="button" class="btn btn-success" title="Vencido" > <span class="pull-center">Vencido</span> </button> ';
          }

			$i++;
			echo '<tr> <td>'.$i.' </td> <td>'.$fila[nom_ape_user].'</td> <td>'.$fila[corre_user].'</td> <td>'.$fila[num_cupon].'</td> <td>'.$fila[valor_cupon].'</td> <td>'.$fila[fech_asig_cupon].'</td> <td class="text-center">'.$icono.'</button></td> <td>';
      echo '<input type="hidden" name="id_user'.$i.'" id="id_user'.$i.'" value="'.$fila[id_user].'" >';
      echo '<input type="hidden" name="id_cupon" id="id_cupon" value="'.$fila[id_cupon].'" >';
      echo '<input type="hidden" name="i" id="i" value="'.$i.'" >';

      if ($fila[stat_cupon]=='enviado') 
      { 

        echo '<button class="btn btn-danger btn-xs" title="Revocar Cupón" onclick="eliminar('.$fila[id_cupon].',\''.$fila[corre_user].'\',\''.$fila[nom_ape_user].'\',\''.$fila[sex_user].'\',\''.$fila[num_cupon].'\')" id="eliminar"> <span class="glyphicon glyphicon-remove eliminar"  aria-hidden="true"> </button>';

       } 
       else 
        { 
          echo '&nbsp;';
        }
        echo '</td>  </tr>';


       
		
	}
	   
	 echo '</form>';
	echo '</table>';
	echo '</div>';
	echo '</div>';

        

	}

	else
	{

		echo '<div class="titulo_form">Registros No Encontrados</div>';


	}
	

	



?>



 <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>