<?php 
session_start();
include("../comunes/conexion.php");

$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
$id_user=$_SESSION['id_user'];
$categoria=$_GET['categoria'];
$categoria = '7';
$logo='../imagenes/sistema/logo.png';
$con[nomb_cate] = 'Cesta de Compras';
$con[desc_cate] = '<b>Tu Tienda UPALOPA<b>';
$color_fondo='#D2C8B0';
include("../comunes/variables.php");
include("../comunes/verificar_admin.php");

  $fechaa=date('d-m-Y');
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>

    <script type="text/javascript">

    $(function()
    {
        $("#buscar").click(function()
        {
          if ($("#form1").validationEngine('validate'))
          {
            var url="busqueda_historial_cupones.php"; 
            $.ajax
            ({
              type: "POST",
              url: url,
              data: $("#form1").serialize(),
               beforeSend: function () 
                {
                    $("#procesando2").html("<table border='0' cellpaddig='0' cellspacing='0'><tr><td>Procesando, <br>Espere por favor...</td><td><img valign='middle' src='../imagenes/acciones/cargando_gray.gif' width='30px'></td></tr></table>");
                },
                success: function(data)              {
                $("#resultado_usuarios").html(data);
                $("#procesando2").html('');
              }
            });
            return false; 
          }

          });       

     });

      </script>

          <!-- validacion en vivo -->
      <script>
        <!-- validacion en vivo -->
        jQuery(document).ready(function(){
          // binds form submission and fields to the validation engine
          jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
         });
      </script>
      <!-- CALENDARIO-->
          <script>
    $(document).ready(function() {
        $('.datepicker')
            .datepicker({
              format: 'dd-mm-yyyy',
              autoclose: true,
              language: 'es'
            });
    });
    </script>
      <!-- funcion de guardar formulario --> 
    
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <body class="">    
    <div class="container-fluid">
      <div class="row">
        <?php include("menu_backend.php"); ?>
      </div>
      <div data-offset-top="100" class="container" data-spy="affix">
        <div id="resultado"></div>
      </div>
      <div class="text-center"> <span class="text-info"><h3> <b> Historial de Cupones </b>  </h3></span> </div>
    <div class="container" >

         <div class="jumbotron">
            <div class="titulo_form">   Parametros de Filtrado </div>
           
                   <form method="POST" name="form1" id="form1" onsubmit="return jQuery(this).validationEngine('validate');">
                     <br>
                     <div class="row"> 
                        <div class="col-md-3">
                            <div class="form-group">           
                             <input type="text" name="corre_user" id="corre_user" class="validate[custom[email], minSize[5], maxSize[100]] text-input, form-control" placeholder="Email">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                             <input type="text" name="fstat_cupon" id="fstat_cupon" class="validate[custom[date]] text-input es form-control input-append date datepicker" placeholder="Fecha de Asignacion Cup&oacute;n Inicio">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                            <?php   echo '<input type="text" name="fstat_cupon1" id="fstat_cupon1" value="" class="validate[custom[date]] text-input es form-control input-append date datepicker" placeholder="Fecha de Asignacion del Cup&oacute;n Fin">'; ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                             <div class="form-group">
                              <select name="status" id="status"  class="form-control">
                                        <option value="" selected disabled style="display:none;">Seleccione el Status</option>
                                         <option  value="enviado">enviado</option>
                                         <option  value="utilizado">utilizado</option>
                                         <option  value="eliminado">eliminado</option>
                                         <option  value="vencido">vencido</option>
                              </select> 
                            </div>
                        </div>
                        <br>
                      <div align="center"> <button  id="buscar" class="btn btn_form" title="Buscar Usuarios" style="color:#FFFFFF">Buscar<span id="procesando2"></span></button> </div> <br><br><br>';
                     

                  </form>
         </div>
    </div>
      

    </div>

  

    <div class="row">
        <div id='resultado_usuarios' class="container">


        </div>

    </div>

</div>
   
    <script src="../bootstrap/js/bootstrap.min.js"> </script>  
  </body>
</html>
