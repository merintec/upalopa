<?php
include_once("../comunes/variables.php");
include_once("../comunes/conexion.php");
$tabla='carga_descarga';
$fecha=date("Y-m-d");
?>
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
    <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    <title><?php echo $nom_pagina; ?></title>
    <script>
      function guardar()
      {
          if ($("#motivo").validationEngine('validate')==false && $("#cantidad").validationEngine('validate')==false)
          {
                  var url="../comunes/funcion_guardar.php"; 
                  $.ajax
                  ({
                      type: "POST",
                      url: url,
                      data: $("#formdev").serialize(),
                      success: function(data)
                      {
                        //$("#resultado2").html(data);
                        var codigo, datatemp, mensaje;
                        var tabla;
                        tabla='producto_detalles';
                        var campo='cant_prod'; 
                        var campo_id='id_prod_deta';
                        var valor=$("#cantidad").val();
                        var var_cant_prod=$("#var_cant_prod").val();
                        var valor_id=$("#id_prod_deta").val();
                        if ($("#var_tipo_carga").val()=='Carga')
                        {
                            valor=Number(var_cant_prod) + Number(valor);

                        }
                        else
                        {
                            valor=Number(var_cant_prod) - Number(valor);
                        }
                        datatemp=data;
                        datatemp=datatemp.split(":::");
                        codigo=datatemp[0];
                        mensaje=datatemp[1];
                        if (codigo==001)
                        {
                            actualizar_dato_mp(tabla,campo,valor,campo_id,valor_id);
                            setTimeout(function() {
                              window.location=('disponibilidad.php');
                            },1000);
                          //$("#formdev")[0].reset();
                        }
                        $("#resultado").html(mensaje);
                        $("#carga").modal('hide');                        
                        setTimeout(function() {
                          $("#msg_act").fadeOut(1500);
                        },3000);    
                      }
                  });       
          }
      }

      function actualizar_dato_mp(tabla,campo,valor,campo_id,valor_id)
        {
            //para verificar si es una fecha voltearla al momento de guardarla
              var resultado;
              var parametros = {
                "var_tabla": tabla,
                "var_campo" : campo,
                "var_valor" : valor,
                "var_id" : campo_id,
                "var_id_val" : valor_id
              };
              var url="../comunes/funcion_actualizarcampo.php"; 
              $.ajax
              ({
                type: "POST",
                  url: url,
                  data: parametros,
                  success: function(data)
                  {
                     
                  }
              });
        }




      
    </script>
  </head>
<div class="ventana-titulo">
  <span id="tipo"> </span>  de Inventario 
</div>
<div class="borde-ventana-punteada">
  <button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -4.7em; margin-right: -5px;">×</button>
  <!-- validacion en vivo -->
  <script >
    jQuery(document).ready(function(){
      // binds form submission and fields to the validation engine
      jQuery("#formdev").validationEngine('attach', {bindMethod:"live"});
     });
  </script>

  <div class="" style="padding-top: 0.3em;  margin:0px; margin-left:auto; margin-right: auto;">
     <br><br><br><br><br>

     <div class="">
         <b>  Producto <span id="producto">  </span> </b>
      </div>

      <br>
      <form method="POST" name="formdev" id="formdev" onsubmit="return jQuery(this).validationEngine('validate');">

          <input type="hidden" name="var_cant_prod" id="var_cant_prod" >
          <input type="hidden" name="var_tipo" id="var_tipo" >

          <input type="hidden" name="id_prod_deta" id="id_prod_deta" >
          <input type="hidden" name="fecha" id="fecha" value="<?php echo $fecha ?>" >
          <input type="hidden" name="tipo_carga" id="tipo_carga"  >
          <input type="hidden" name="var_tipo_carga" id="var_tipo_carga"  >



            
       
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-3 col-xs-3 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="motivo" > Motivo</label>
          </div>
          <div class="col-md-9 col-xs-9 text-right">
            <input type="text" name="motivo" id="motivo" class="validate[required] text-input form-control fondo_campo" placeholder="Motivo">
          </div> 
        </div>
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-3 col-xs-3 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="motivo" > Cantidad</label>
          </div>

          <div class="col-md-9 col-xs-9 text-right">
            <input type="text" name="cantidad" id="cantidad" class="validate[required, custom[integer]] text-input form-control fondo_campo" placeholder="Cantidad">
          </div> 
        </div>

        
       
        <?php 

        $consulta_usuario="SELECT * FROM usuarios where id_user='$id_user'";
        $con_usu=mysql_fetch_assoc(mysql_query($consulta_usuario));

        ?>
         
         
          <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
          <input type="hidden" name="var_correo" id="var_correo" value='<?php echo $con_usu[corre_user]; ?>'>
          <input type="hidden" name="var_nom_ape" id="var_nom_ape" value='<?php echo $con_usu[nom_ape_user]; ?>'>
          
          <br>
        <center><button id="ingresar" type="submit" class="btn fondo_boton" onclick="guardar();return false;"><div class="vineta-blanco">&nbsp;</div> Guardar </button></center>        
      </form>
  </div>

