<?php 
session_start();
include("../comunes/conexion.php");
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
$id_user=$_SESSION['id_user'];
$categoria=$_GET['categoria'];
$categoria = '7';
$logo='../imagenes/sistema/logo.png';
$con[nomb_cate] = 'Cesta de Compras';
$con[desc_cate] = '<b>Tu Tienda UPALOPA<b>';
$color_fondo='#D2C8B0';
include("../comunes/variables.php");
//include("../comunes/verificar_admin.php");
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes --> 
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <script>
      function procesar(pago,venta,accion,tot_compra,motivo){
        var valida = 'SI';
        if (accion=='R'){
          accion = 'Rechazado';
          if ($("#moti_rech").validationEngine('validate')){
            valida = 'NO';
          }

        }
        if (accion=='P'){
          accion = 'Aprobado';
        }
        if (valida=='SI'){
          /// reiniciar modal
          $('#modal_motivo').modal('hide');
          $('#moti_rech').val('');
          $('#aceptar_motivo').attr("onclick", '');
          /////////////////////
          var url="procesar_pago.php"; 
          var parametros = {
            "id_pago"  : pago,
            "id_venta" : venta, 
            "accion"   : accion,
            "tot_compra" : tot_compra,
            "motivo" : motivo
          };
          $.ajax
          ({
              type: "POST",
              url: url,
              data: parametros,
              success: function(data)
              {
                var codigo, datatemp, mensaje, accion_registrada, nuevo_status;
                datatemp=data;
                datatemp=datatemp.split(":::");
                codigo=datatemp[0];
                mensaje=datatemp[1];
                nuevo_status = datatemp[2];
                correo_enviar=datatemp[3].split("-->");
                accion_registrada=datatemp[4];
                enviar_correo(correo_enviar[0],correo_enviar[1],correo_enviar[2]);
                if (accion_registrada){
                  $("#status" + pago).html(accion_registrada);
                }
                if (nuevo_status == 'oculto'){
                  $("#venta" + venta).addClass('oculto');
                }
                $("#resultado").html(mensaje);
                setTimeout(function() {
                  $("#msg_act").fadeOut(1500);
                },3000);
                 
              }
          });
        }
      }

      function enviar_correo(destino,titulo,mensaje)
      {
        //alert(destino + titulo + mensaje);
        var url="../comunes/enviar_correo.php"; 
        var parametros = {
            "mensaje" : mensaje,
            "destino" : destino, 
            "correo_origen" : "<?php echo $correo_compras; ?>",
            "titulo" : titulo
          };
        $.ajax
        ({
            type: "POST",
            url: url,
            data: parametros,
            success: function(data)
            {

               
            }
        });
      }

      function muestra_oculta(id){
          if (document.getElementById){ //se obtiene el id
              var el = document.getElementById(id); //se define la variable "el" igual a nuestro div
              el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
          }
      }
      function pasar_modal(pago,venta,accion,tot_compra,motivo){
        var var_onclick = 'procesar('+pago+','+venta+',"'+accion+'","'+tot_compra+'",$("#moti_rech").val())';
        $('#aceptar_motivo').attr("onclick", var_onclick);
      }
      function confirma_eliminar(venta,trans){
        $('#venta_eliminar').val(venta);
        $('#orden_num').html(trans);
      }
      function eliminar_venta(destino,titulo,mensaje)
      {
        //alert(destino + titulo + mensaje);
        var url="../comunes/funcion_eliminar2.php"; 
        var parametros = {
            "var_tabla" : 'venta',
            "var_campo" : 'id_venta',
            "var_valor" : $('#venta_eliminar').val()
          };
        $.ajax
        ({
            type: "POST",
            url: url,
            data: parametros,
            success: function(data)
            {
              if (data=='1'){
                window.location=('pagos.php');
              }
              else{
                $('#pregunta').html('Ocurrió un error. <br>¿Desea Intentar nuevamente eliminar la orden ');
              }               
            }
        });
      }
    </script> 
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <body class="">    
    <div class="container-fluid">
      <div class="row">
        <?php include("menu_backend.php"); ?>
      </div>
      <div data-offset-top="100" class="container" data-spy="affix">
        <div id="resultado"></div>
      </div>
      <div class="text-center"> <span class="text-info"><h3> <b> Gestión de Pagos </b>  </h3></span> </div>
    <div class="container" >

        <?php    
        $consulta="SELECT * FROM venta as v, usuarios u  where v.id_user=u.id_user AND (v.status_venta='pagado' || v.status_venta='procesado') ";
        $consulta=mysql_query($consulta);

        $i=0;
        while($fila=mysql_fetch_array($consulta))
        {
          $totalp=0;
          $total_pagar=0;
          $total_cant=0;
          $i++;
                   $consulta2="SELECT * FROM venta_productos as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
                   vp.id_venta='$fila[id_venta]' and vp.status_vent='procesado' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
                  $consulta2=mysql_query($consulta2);
                  while ($fila2=mysql_fetch_array($consulta2)) 
                  {
                     $totalp=$fila2[vuni_venta_prod]*$fila2[cant_venta_prod];
                     $total_pagar += $totalp;
                     $total_cant += $fila2[cant_venta_prod];
                     $peso_cu=$fila2[peso_prod] * $fila2[cant_venta_prod];
                     $total_peso += $peso_cu;

                      $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
                      $consulta_envio=mysql_query($consulta_envio);
                      $con_env=mysql_fetch_assoc($consulta_envio);;
                      if (!$con_env[id_tenvios])
                      {
                            $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                            $consulta_envio=mysql_query($consulta_envio);
                            $con_env=mysql_fetch_assoc($consulta_envio);
                            $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);
                      }


                    }

                  
                    echo '<div id="venta'.$fila[id_venta].'" class="table-responsive procesar_linea_punteada">';
                    echo '<table class="table table-striped table-hover">
                    <tr><th>N° de Orden </th> <th>Cantidad de Productos </th> <th> Monto a Cancelar </th> <th> &nbsp; </th> </tr>';
       
                  echo '<tr><td>'.$fila[codg_trans].' </td> <td>'.$total_cant.'</td> <td>'.number_format(($total_pagar+$con_env[prec_tenvio]+($total_pagar * ($con_env[porc_tenvio]/100))),2,",",".").' </td><td>  <button class="btn btn-primary" title="Mostrar detalle de la compra" onclick="muestra_oculta(\'desplegar'.$i.'\')"> <span class="glyphicon glyphicon-eye-open"  aria-hidden="true"> </button>

                  <button class="btn btn-danger" title="Eliminar Venta" data-toggle="modal" data-target="#confirmar_eliminacion" onclick="confirma_eliminar('.$fila[id_venta].',\''.$fila[codg_trans].'\');"> <span class="glyphicon glyphicon-remove"  aria-hidden="true"> </button>



                  </td> </tr>';
                  echo '</table>';
                  
                  
                        echo '<div class="table-responsive" >';
                          echo '<div class="row" id="desplegar'.$i.'" style="display: none;">';
                          echo '<div class="col-md-12 col-xs-12">';                        
                            echo '<span class="text-info" ><b>Datos del comprador(a):</b></span>';
                            echo '<table  class="table table-striped table-hover" style="font-size: 0.7em">';
                            echo '<tr><th>Nombres y Apellidos</th><th>Correo</th><th>Teléfono</th></tr>';
                            echo '<tr><td>'.$fila[nom_ape_user].'</td><td>'.$fila[corre_user].'</td><td>'.$fila[tele_user].'</td></tr>';
                            echo '</table>'; 
                          echo '</div>';
                            echo '<div class="col-md-5 col-xs-5">';
                                  echo '<span class="text-info" ><b>Productos de la Compra</b></span>';
                                  echo '<table  class="table table-striped table-hover" style="font-size: 0.7em">'; 
                                  echo '<tr> <th># </th> <th>Producto</th> <th>Cantidad </th> <th>Talla </th> <th>Color </th>  <th> Precio Unitario </th> <th> Total </th> </tr>';

                                  //consulta para mostrar detalle de articulos
                                  $consulta3="SELECT * FROM venta_productos as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
                                   vp.id_venta='$fila[id_venta]' and vp.status_vent='procesado' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
                                  $consulta3=mysql_query($consulta3);
                                  $j=0;
                                    

                                  while ($fila3=mysql_fetch_array($consulta3)) 
                                  {
                                      $totalp=$fila[vuni_venta_prod]*$fila[cant_venta_prod];
                                      $imp_cu = ($totalp / (($val_imp/100)+1));
                                      $imp_cu = $imp_cu * $val_imp / 100;
                                      $imp_cu = number_format($imp_cu,2,'.','');
                                      //// para los totales generales
                                      $peso_cu=$fila[peso_prod] * $fila[cant_venta_prod];
                                      $total_pagar += $totalp;
                                      $total_peso += $peso_cu;
                                      $total_imp += $imp_cu;
                                      $total_cant += $fila[cant_venta_prod];
                                      $subtotal=$fila3[vuni_venta_prod]*$fila3[cant_venta_prod];

                                      $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
                                      $consulta_envio=mysql_query($consulta_envio);
                                      $con_env=mysql_fetch_assoc($consulta_envio);;

                                      if (!$con_env[id_tenvios])
                                      {

                                            $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                                            $consulta_envio=mysql_query($consulta_envio);
                                            $con_env=mysql_fetch_assoc($consulta_envio);

                                            $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);

                                      }
                                      $j++;
                                              echo '<tr> <td>'.$j.'</td><td>'.$fila3[nomb_prod].' </td> <td align="center">'.$fila3[cant_venta_prod].'</td> <td>'.$fila3[desc_talla].' </td> <td>'.$fila3[nom_color].' </td>  <td align="right">'.number_format($fila3[vuni_venta_prod],2,",",".").' </td><td align="right">'.number_format($subtotal,2,",",".").' </td> </tr>';

                                     
                                      
                                  }   $final_pagar = number_format(($total_pagar+$con_env[prec_tenvio]+($total_pagar * ($con_env[porc_tenvio]/100))),2,".","");
                                      echo '<tr><td align="right" colspan="3"><b>Subtotal: Bs.</b> </td><td align="right" colspan="2"><b>'.number_format($total_pagar,2,",",".").'</b></td></tr>';
                                      echo '<tr><td align="right" colspan="3"><b>Costo de Envio: Bs. </b></td><td align="right" colspan="2"><b>'.number_format($con_env[prec_tenvio],2,",",".").'</b></td></tr>';
                                      echo '<tr><td align="right" colspan="3"><b>Costo de Seguro: Bs.</b> </td><td align="right" colspan="2"><b>'.number_format(($total_pagar * ($con_env[porc_tenvio]/100)),2,",",".").'</b></td></tr>';
                                      echo '<tr><td align="right" colspan="3"><b>Total a Pagar Bs. </b></td><td align="right" colspan="2"  style=" font-size: 1.3em; color:#c174ad;?>;" ><b>'.$final_pagar.'</b></td></tr>';
                                  echo "</table>";
                            echo '</div>';
                            echo '<div class="col-md-7 col-xs-7">';
                              echo '<span class="text-info" ><b>Datos de los Pagos</b></span>';
                              echo '<table  class="table table-striped table-hover" style="font-size: 0.7em">'; 
                              echo '<tr> <th># </th> <th style="width:7.2em;">Fecha</th> <th>Tipo</th> <th>Destino</th> <th>Referencia</th> <th>Monto</th><th>Status</th><th>Acción</th></tr>';
                              $sql_pagos = "SELECT * FROM pago p, cuentas_bancarias cb, metodo_pago mp WHERE p.id_venta = ".$fila[id_venta]." AND p.id_cuen_banc=cb.id_cuen_banc AND p.id_mpago=mp.id_mpago";
                              $bus_pagos = mysql_query($sql_pagos);
                              $count_pago = 1;
                              while ($res_pagos=mysql_fetch_array($bus_pagos)){
                                $fecha = strtotime($res_pagos[fech_pago]);
                                $res_pagos[fech_pago] = date("d-m-Y", $fecha);
                                echo '<tr title="Realizó el pago: '.$res_pagos[nomb_per_pago].' '."\n".' Doc. Ident: '.$res_pagos[iden_per_pago].'. '."\n".' Desde: '.$res_pagos[banc_origen].'"> <td class="text_right">'.$count_pago.'</td> <td>'.$res_pagos[fech_pago].'</td><td>'.$res_pagos[nomb_mpago].'</td> <td>'.$res_pagos[desc_cuen_banc].'</td> <td class="text-center">'.$res_pagos[num_refe_banc].'</td> <td class="text_right">'.number_format($res_pagos[mont_pago],2,",",".").'</td><td><span id="status'.$res_pagos[id_pago].'">'.ucwords($res_pagos[status_pago]).'</span></td><td><button class="btn btn-success btn-xs" title="Aprobar Pago" onclick="procesar('.$res_pagos[id_pago].','.$res_pagos[id_venta].',\'P\',\''.$final_pagar.'\',\'\')"> <span class="glyphicon glyphicon-ok"  aria-hidden="true"> </button>&nbsp;<button class="btn btn-danger btn-xs" title="Rechazar Pago" data-toggle="modal" data-target="#modal_motivo" onclick="pasar_modal('.$res_pagos[id_pago].','.$res_pagos[id_venta].',\'R\',\''.$final_pagar.'\',\'\');"> <span class="glyphicon glyphicon-remove"  aria-hidden="true"> </button></td></tr>';
                                $count_pago++;
                              }
                              $sql_pagos = "SELECT * FROM pago p, metodo_pago mp WHERE p.id_venta = ".$fila[id_venta]." AND p.id_cuen_banc IS NULL AND p.id_mpago=mp.id_mpago";
                              $bus_pagos = mysql_query($sql_pagos);
                              $count_pago = 1;
                              while ($res_pagos=mysql_fetch_array($bus_pagos)){
                                $fecha = strtotime($res_pagos[fech_pago]);
                                $res_pagos[fech_pago] = date("d-m-Y", $fecha);
                                echo '<tr title="Realizó el pago: '.$res_pagos[nomb_per_pago].' '."\n".' Doc. Ident: '.$res_pagos[iden_per_pago].'. '."\n".' Desde: '.$res_pagos[banc_origen].'"> <td class="text_right">'.$count_pago.'</td> <td>'.$res_pagos[fech_pago].'</td><td>'.$res_pagos[nomb_mpago].'</td> <td>'.$res_pagos[desc_cuen_banc].'</td> <td class="text-center">'.$res_pagos[num_refe_banc].'</td> <td class="text_right">'.number_format($res_pagos[mont_pago],2,",",".").'</td><td><span id="status'.$res_pagos[id_pago].'">'.ucwords($res_pagos[status_pago]).'</span></td><td><button class="btn btn-success btn-xs" title="Aprobar Pago" onclick="procesar('.$res_pagos[id_pago].','.$res_pagos[id_venta].',\'P\',\''.$final_pagar.'\',\'\')"> <span class="glyphicon glyphicon-ok"  aria-hidden="true"> </button>&nbsp;<button class="btn btn-danger btn-xs" title="Rechazar Pago" data-toggle="modal" data-target="#modal_motivo" onclick="pasar_modal('.$res_pagos[id_pago].','.$res_pagos[id_venta].',\'R\',\''.$final_pagar.'\',\'\');"> <span class="glyphicon glyphicon-remove"  aria-hidden="true"> </button></td></tr>';
                                $count_pago++;
                              }
                              echo '</table>';
                            echo '</div>';

                          echo '</div>'; 
                        echo "</div>";
                  echo "</div>";

        }

	if ($i==0){
		echo '<div class="alert-info text-center" style="border: 1px dashed #000; max-width: 50%; height: 5em; padding-top: 1em; margin: auto auto;" ><h4>No tienes pagos por procesar!!!</h4></div>';
	}  

       ?>
       </div>
 
      <br>
      <br>
      <br>
      <br>

    </div>
    <!-- Modal para login -->
    <div class="modal fade" id="login" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 350px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal">
              <?php 
                include('login.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div> 
    </div>
    <!-- Modal para Eliminar -->
    <div class="modal fade" id="confirmar_eliminacion" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 350px">
        <div class="modal-content">
          <div class="modal-body">
            <div id="contenido_modal2">
              <center><h4><span id="pregunta">¿Realmente desea eliminar la Orden de compra </span><font color="RED">#<span id="orden_num"></span></font>?</h4></center>
              <input type="hidden" name="venta_eliminar" id="venta_eliminar"></input>
              <br><br>
              <div class="row">
                <div class="col-md-6 col-xs-6">            
                  <button class="btn btn-danger" style="float: right;" data-dismiss="modal">Cancelar</button>
                </div>
                <div class="col-md-6 col-xs-6">            
                  <button class="btn btn-success" onclick="eliminar_venta()">Aceptar</button>
                </div>
              </div>
            </div>
          </div>
        </div>  
      </div> 
    </div>
    <!-- Modal Motivo -->
    <div class="modal fade" id="modal_motivo" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4>Esta acción requiere información adicional</h4>
          </div>
          <div class="modal-body">
            <span class="glyphicon glyphicon-question-sign glyphicon_azul pull-left msg_modal_icon"></span>
              <p>¿Cuál es el motivo del Rechazo?</p>
              <input id="moti_rech" name="moti_rech" type="text" style="max-width: 30em;" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[60]] text-input, form-control">
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <button id="aceptar_motivo" class="btn btn-primary" >Aceptar</button>
          </div>
        </div>  
      </div>
    </div>    
    <script src="../bootstrap/js/bootstrap.min.js"> </script>  
  </body>
</html>
