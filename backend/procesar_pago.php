<?php
	include("../comunes/conexion.php");
	$id_pago = $_POST[id_pago];
	$id_venta = $_POST[id_venta]; 
    $accion = $_POST[accion];
    $tot_compra = $_POST[tot_compra];
    $motivo = $_POST[motivo];
    // actualizar pago
    $sql_update_pago = "UPDATE pago SET status_pago = '".$accion."', obsr_pago = '".$motivo."' WHERE id_pago = ".$id_pago; 
    if (mysql_query($sql_update_pago)){
    	$error = '';
    }
    else {
    	$error = mysql_error();
    }
    if ($error==''){
    	// actualizar venta	
	    $sql_pagado = "SELECT *, if(SUM(mont_pago),SUM(mont_pago),0) as pagado FROM pago WHERE id_venta = ".$id_venta." AND status_pago = 'Aprobado'";
	    $res_pagado = mysql_fetch_array(mysql_query($sql_pagado));
	    if ($tot_compra>0){
		    if ($res_pagado[pagado]>=$tot_compra && $accion=='Aprobado'){
		    	$sql_update_venta = "UPDATE venta SET status_venta = 'confirmado', fech_stat='".date('Y-m-d')."' WHERE id_venta = ".$id_venta;
		    	$nuevo_status = 'oculto';
                $modo = 'Total';
		    }
		    else {
                $deuda = $tot_compra - $res_pagado[pagado];
				$sql_update_venta = "UPDATE venta SET status_venta = 'procesado', fech_stat='".date('Y-m-d')."', obse_vent = '".$motivo."' WHERE id_venta = ".$id_venta;
		    	$nuevo_status = 'visible';
                $modo = '';
		    }
		    if (mysql_query($sql_update_venta)){
	    		$error = '';
		    }
		    else {
		    	$error = mysql_error();
		    }
	    }
    }
    $mensaje_resultado = '';
    $sql_info = "SELECT * FROM venta v, pago p, usuarios u, metodo_pago m, cuentas_bancarias b WHERE v.id_venta=".$id_venta." AND v.id_venta = p.id_venta AND p.id_pago = ".$id_pago." AND v.id_user=u.id_user AND p.id_mpago=m.id_mpago AND p.id_cuen_banc = b.id_cuen_banc";
    $info = mysql_fetch_array(mysql_query($sql_info));
    $sex = $info[sex_user];
    if ($sex=='F'){
        $sex = 'a';
    }
    else {
        $sex = 'o';  
    } 
    $tabla_p = "<table border='0' cellspacing='0px' cellpadding='0px' width='400px' style='font-family: arial;'> <tr> <td style='color: #fff; background-color: #b3908f; font-weight: bold; padding: 8px;' colspan='3'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Datos del Pago </td> </tr> <tr> <td width='10px'> &nbsp; </td> <td style='padding: 15px; border: 1px dashed #c0ab9e; border-top: 0px;'>&nbsp; <table border='0' cellspacing='5px' cellpadding='5px'> <tr> <td style='font-weight: bold;'> Estado: </td> <td> ".$accion." </td> </tr> <tr> <td style='font-weight: bold;'> Forma&nbsp;de&nbsp;Pago: </td> <td> ".$info[nomb_mpago]." </td> </tr> <tr> <td style='font-weight: bold;'> Referencia #: </td> <td> ".$info[num_refe_banc]." </td> </tr>  <tr> <td style='min-width: 100px; font-weight: bold;'> Fecha: </td> <td> ".date('d-m-Y', strtotime($info[fech_pago]))." </td> </tr> <tr> <td style='font-weight: bold;'> Monto: </td> <td> ".number_format($info[mont_pago],2,',','.')." </td> </tr> <tr> <td style='font-weight: bold;'> Nombre Depositante: </td> <td> ".$info[nomb_per_pago]." </td> </tr> <tr> <td style='font-weight: bold;'> Cuenta Destino: </td> <td> ".$info[nomb_banc]."<br><span style='font-size: 10px;'>".$info[nume_cuen_banc]."</span> </td> </tr> </table> </td> <td width='10px'> &nbsp; </td> </tr> </table>";
    if ($accion=='Rechazado'){
        $mensaje_resultado = "Estimad".$sex." ".$info[nom_ape_user].", <p style='padding-top: 20px;'>!Su pago para la orden de compra N° ".$info[codg_trans]." ha sido <b>Declinado</b>!</p><p style='padding-top: 20px;'>".$tabla_p."</p><p style='padding-top: 20px;'>El motivo es: <b>".ucfirst($motivo)."</b>.</p><p style='padding-top: 20px;'>Para mayor detalle consulte la sección <b>Tus Pagos</b> en nuestro sitio web <a href='http://upalopa.com'>www.upalopa.com</a>.</p><p style='padding-top: 20px;'>Por Favor verifique la información y registre nuevamente el pago.</p>";
        $correo_enviar = $info[corre_user]."-->Pago Declinado -->".$mensaje_resultado;
    }
    if ($accion=='Aprobado'){
        if ($modo == 'Total'){
            $mensaje_resultado = "Estimad".$sex." ".$info[nom_ape_user].", <p style='padding-top: 20px;'>!Su pago para la orden de compra N° ".$info[codg_trans]." ha sido <b>Aprobado</b> exitosamente!</p><p style='padding-top: 20px;'>".$tabla_p."</p><p style='padding-top: 20px;'>Para mayor detalle consulte la sección <b>Tus Compras</b> en nuestro sitio web <a href='http://upalopa.com'>www.upalopa.com</a>.</p>";
            $correo_enviar = $info[corre_user]."-->Pago Aprobado Exitosamente -->".$mensaje_resultado;
//            $correo_enviar = $info[corre_user]."-->Pago Aprobado Exitosamente-->Hola <b>".$info[nom_ape_user]."!!!</b>, <br>Hemos aprobado el pago registrado para la orden Nº ".$info[codg_trans]."<br>Proximamente te contactaremos para informate los datos del envío. <br>Por Favor ingrese con su usuario a http://www.upalopa.com a la sección de compras y verifique el estato de la misma. <br>Gracias por preferirnos, upalopa.com.";
        }
        else{
            $mensaje_resultado = "Estimad".$sex." ".$info[nom_ape_user].", <p style='padding-top: 20px;'>!Su pago parcial para la orden de compra N° ".$info[codg_trans]." ha sido <b>Aprobado</b> exitosamente!</p><p>Cuenta con 30 días para efectuar la cancelación total. </p><p style='padding-top: 20px;'>".$tabla_p."</p><p style='padding-top: 20px;'>Para mayor detalle consulte la sección <b>Tus Pagos</b> en nuestro sitio web <a href='http://upalopa.com'>www.upalopa.com</a>.</p><p style='padding-top: 20px;'>Recuerde que deberá registrar un pago por Bs. <b>".number_format($deuda,2,',','.')."</b> para proceder con el envío de tu compra.</p>";
            $correo_enviar = $info[corre_user]."-->Pago Parcial Aprogado Exitosamente -->".$mensaje_resultado;
//            $correo_enviar = $info[corre_user]."-->Pago Parcial Aprogado Exitosamente-->Hola <b>".$info[nom_ape_user]."!!!</b>, <br>Hemos aprobado un pago parcial para la orden Nº ".$info[codg_trans]."<br>Por Favor ingrese con su usuario a http://www.upalopa.com a la sección de pagos y verifique la información o registre un nuevo pago por la diferencia. <br>Gracias por preferirnos, upalopa.com.";
        }
    }
    if ($error==''){
		$resultado="001:::</br><div class='alert alert-info' id='msg_act'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <span class='glyphicon glyphicon-info-sign pull-left'></span>&nbsp;&nbsp;
        <strong>El pago ha sido ".$accion." exitosamente... </strong></div>:::".$nuevo_status.":::".$correo_enviar.":::".$accion;
    }
    else{
    	$resultado = "002:::</br><div class='alert alert-danger' id='msg_act'>
        <button type='button' class='close' data-dismiss='alert'>&times;</button>
        <span class='glyphicon glyphicon-alert pull-left'></span>&nbsp;&nbsp;
        <strong>Ocurrio un error al actualizar el pago... </strong></div>";
    	//$resultado .= mysql_error();
    }
	echo $resultado;
?>
