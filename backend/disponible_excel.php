<?php
	include("../comunes/variables.php");
	include("../comunes/verificar_admin.php");
	include("../comunes/conexion.php");
	$consulta = "SELECT * FROM  producto_detalles as pd , productos as p, tallas  as t, colores as c, categoria as ca, sub_categoria as sc where p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color and sc.id_scate=p.id_scate and ca.id_cate=sc.id_cate order by p.nomb_prod ASC";
	$resultado = mysql_query($consulta);
	$cuenta = mysql_num_rows($resultado);
	if($cuenta > 0){
		$fecha = date("d-m-Y");
		date_default_timezone_set('America/Mexico_City');

		if (PHP_SAPI == 'cli')
			die('Este archivo solo se puede ver desde un navegador web');

		/** Se agrega la libreria PHPExcel */
		require_once '../comunes/PHPExcel/PHPExcel.php';
		// Se crea el objeto PHPExcel
		$objPHPExcel = new PHPExcel();

		// Se asignan las propiedades del libro
		$objPHPExcel->getProperties()->setCreator("UPALOPA") //Autor
							 ->setLastModifiedBy("UPALOPA") //Ultimo usuario que lo modificó
							 ->setTitle("Disponibilidad de Productos al ".$fecha)
							 ->setSubject("Disponibilidad de Productos al ".$fecha)
							 ->setDescription("Disponibilidad de Productos al ".$fecha)
							 ->setKeywords("Disponibilidad de Productos en xls")
							 ->setCategory("Disponibilidad en Excel");

		$tituloReporte = "Disponibilidad de Productos al ".$fecha;
		$titulosColumnas = array('Producto', 'Categoría', 'Subcategoría', 'Color', 'Talla', 'Disponible', 'Apartado', 'Vendido');
		
		$objPHPExcel->setActiveSheetIndex(0)
        		    ->mergeCells('A1:H2');
						
		// Se agregan los titulos del reporte
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1',$tituloReporte)
        		    ->setCellValue('A3',  $titulosColumnas[0])
		            ->setCellValue('B3',  $titulosColumnas[1])
        		    ->setCellValue('C3',  $titulosColumnas[2])
            		->setCellValue('D3',  $titulosColumnas[3])
            		->setCellValue('E3',  $titulosColumnas[4])
            		->setCellValue('F3',  $titulosColumnas[5])
            		->setCellValue('G3',  $titulosColumnas[6])
            		->setCellValue('H3',  $titulosColumnas[7]);
		
		//Se agregan los datos de los alumnos
		$i = 4;

		while ($fila = mysql_fetch_array($resultado)) {

           $consulta_disponibilidad="SELECT sum(cant_venta_prod) as suma_prod FROM producto_detalles as pd, venta_productos as vp, venta as v where pd.id_prod_deta='$fila[id_prod_deta]' and vp.id_prod_deta=pd.id_prod_deta and v.id_venta=vp.id_venta and  (v.status_venta='carrito' or v.status_venta='procesado' or v.status_venta='pagado' or v.status_venta='confirmado')";
           $con_dis=mysql_fetch_assoc(mysql_query($consulta_disponibilidad));
           $producto_apartado=$con_dis[suma_prod];

           if ($producto_apartado=='') { $producto_apartado='0'; }

           $consulta_vendidos="SELECT sum(cant_venta_prod) as suma_prod FROM producto_detalles as pd, venta_productos as vp, venta as v where pd.id_prod_deta='$fila[id_prod_deta]' and  vp.id_prod_deta=pd.id_prod_deta and v.id_venta=vp.id_venta and (v.status_venta='enviado' or v.status_venta='entregado'  )";
           $con_ven=mysql_fetch_assoc(mysql_query($consulta_vendidos));
           $producto_vendido=$con_ven[suma_prod];

            if ($producto_vendido=='') { $producto_vendido='0'; }


			$objPHPExcel->setActiveSheetIndex(0)
        		    ->setCellValue('A'.$i,  $fila[nomb_prod])
		            ->setCellValue('B'.$i,  $fila[nomb_cate])
        		    ->setCellValue('C'.$i,  $fila[nomb_scate])
            		->setCellValue('D'.$i, utf8_encode($fila[nom_color]))
            		->setCellValue('E'.$i, utf8_encode($fila[nomb_talla]))
            		->setCellValue('F'.$i, utf8_encode($fila[cant_prod]))
            		->setCellValue('G'.$i, utf8_encode($producto_apartado))
            		->setCellValue('H'.$i, utf8_encode($producto_vendido));
					$i++;
		}
		
		$estiloTituloReporte = array(
        	'font' => array(
	        	'name'      => 'Verdana',
    	        'bold'      => true,
        	    'italic'    => false,
                'strike'    => false,
               	'size' =>16,
	            	'color'     => array(
    	            	'rgb' => '000000'
        	       	)
            ),
	        'fill' => array(
				'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
				'color'	=> array('rgb' => 'FFFFFF')
			),
            'borders' => array(
               	'allborders' => array(
                	'style' => PHPExcel_Style_Border::BORDER_NONE                    
               	)
            ), 
            'alignment' =>  array(
        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        			'rotation'   => 0,
        			'wrap'          => TRUE
    		)
        );

		$estiloTituloColumnas = array(
            'font' => array(
                'name'      => 'Arial',
                'bold'      => true,                          
                'color'     => array(
                    'rgb' => '000000'
                )
            ),
            'borders' => array(
            	'top'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '000000'
                    )
                ),
                'bottom'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '000000'
                    )
                )
            ),
			'alignment' =>  array(
        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        			'wrap'          => TRUE
    		));
			
		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray(
			array(
           		'font' => array(
               	'name'      => 'Arial',               
               	'color'     => array(
                   	'rgb' => '000000'
               	)
           	),
        ));
		 
		$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->applyFromArray($estiloTituloColumnas);		
		$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:H".($i-1));
				
		for($i = 'A'; $i <= 'D'; $i++){
			$objPHPExcel->setActiveSheetIndex(0)			
				->getColumnDimension($i)->setAutoSize(TRUE);
		}
		
		// Se asigna el nombre a la hoja
		$objPHPExcel->getActiveSheet()->setTitle('Productos');

		// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
		$objPHPExcel->setActiveSheetIndex(0);
		// Inmovilizar paneles 
		//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
		$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

		// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="ReporteDiponibilidad_'.$fecha.'.xls"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;	
	}
	else{
		print_r('No hay resultados para mostrar');
	}
?>
