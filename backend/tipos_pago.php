<?php
  include("../comunes/variables.php");
  include("../comunes/verificar_admin.php");
  include("../comunes/conexion.php");
  $tabla='metodo_pago';                       // tabla origen y Destino de los datos 
  $titulos_busqueda = 'Nombre,Descripción';   // Titlos de las columnas para mostrar en la busqueda
  //$datos_busqueda = 'nomb_mpago,desc_mpago->categoria::nomb_cate::desc_cate->sub_categoria::nomb_scate::desc_scate';  // nombres de los campos para mostrar en la busqueda
  $nregistros = 5;                           // cantidad de registros por pagina en los resultados de la Busqueda si no se especifica el valor por defecto es 10
  $datos_busqueda = 'nomb_mpago,desc_mpago';
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <!-- validacion en vivo -->
<script >
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
   });
</script>
<!-- CALENDARIO-->
<script>
    jQuery(document).ready(function(){
      // binds form submission and fields to the validation engine
      $( ".datepicker" ).datepicker();
      jQuery("form1").validationEngine();
    });
</script>
<!-- funcion de guardar formulario -->  
<script type="text/javascript">
$(function()
{
    $("#guardar").click(function()
    {
      $("#resultado").html("");
      if ($("#form1").validationEngine('validate')){
        var url="../comunes/funcion_guardar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: $("#form1").serialize(),
            success: function(data)
            {
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                $("#form1")[0].reset();
              }
              $("#resultado").html(mensaje);
              setTimeout(function() {
                $("#msg_act").fadeOut(1500);
              },3000);
              buscar();
            }
        });
        return false;
      }
    });
});
function buscar()
{
  var url="../comunes/busqueda.php"; 
  $.ajax
  ({
    type: "POST",
    url: url,
    data: $("#form1").serialize(),
    success: function(data)
    {
      $("#resultado_busqueda").html(data);
    }
  });
  return false; 
}
</script>
<body>
  <?php
    include("menu_backend.php");
  ?>
<div data-offset-top="100" class="container" data-spy="affix">
    <div id="resultado"></div>
</div>
  <div class="jumbotron cajalogin">
    <div class="titulo_form">   Tipos de Pagos </div>
    <form method="POST" name="form1" id="form1" onsubmit="return jQuery(this).validationEngine('validate');">
      <!--- Campos Necesarios para el Funcionamiento de los scripts auxiliares -->
      <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
      <input type="hidden" name="var_titulos_busqueda" id="var_titulos_busqueda" value='<?php echo $titulos_busqueda; ?>'>
      <input type="hidden" name="var_datos_busqueda" id="var_datos_busqueda" value='<?php echo $datos_busqueda; ?>'>
      <input type="hidden" name="var_nregistros" id="var_nregistros" value='<?php echo $nregistros; ?>'>
      <input type="hidden" name="var_pag_actual" id="var_pag_actual" value=''>
      <!-- Fin de los campos necesarios -->
      <div class="form-group">
        <label for="nomb_mpago" class="etq_form" > Nombre:</label>
        <div id="grupo_nomb_mpago" class="input-group">
          <input type="text" name="nomb_mpago" id="nomb_mpago" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[100]] text-input, form-control" placeholder="Nombre tipo de pago">
          <span id="boton_nomb_mpago" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="buscar()";>
            <span id="buscar_nomb_mpago" title="Buscar" class="glyphicon glyphicon-search"> </span>
            <span id="actualiza_nomb_mpago" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
          </span>
        </div>
      </div>
      <div class="form-group">
        <label for="desc_mpago" class="etq_form" > Descripci&oacute;n:</label>
        <div id="grupo_desc_mpago" class="">
          <input type="text" name="desc_mpago" id="desc_mpago" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[100]] text-input, form-control" placeholder="Describir tipo de pago">
          <span id="boton_desc_mpago" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
            <span id="actualiza_desc_mpago" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
          </span>
        </div>
      </div>
      <div align="center"><a href="<?php echo $_PHP_SELF; ?>"><button id="cancelar" type="button" class="btn btn_form oculto" >Cancelar</button></a> <input type="submit" name="guardar"  id="guardar" value="Guardar" class="btn btn_form" > </div>
    </form>
  </div>
  <div class="row">
    <div id='resultado_busqueda' class="container">
      <?php 
        include("../comunes/busqueda.php");
      ?>
    </div>
  </div>
  <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>