<?php
session_start();
include("../comunes/variables.php");
include("verificar_admin.php");
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <title><?php echo $nom_pagina; ?></title>
    <script type="text/javascript">
    function buscar_orden(){
      var orden = $('#orden_compra').val();
      var mensaje;
      if (!orden){
        mensaje = "<div id='msg_act' class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='glyphicon glyphicon-alert pull-left'></span>&nbsp;&nbsp;<strong>Debes especificar un número de orden</strong></div>";
        $('#resultado').html(mensaje);
        setTimeout(function() {
          $("#msg_act").fadeOut(1500);
        },3000);
      }
      else{
        var parametros = {
          "codg_trans": orden
        };
        var url="../comunes/buscar_orden.php"; 
        $.ajax
        ({
          type: "POST",
            url: url,
            data: parametros,
            success: function(data)
            {
              $('#resultado').html(data);
            }
        });
        setTimeout(function() {
          $("#msg_act").fadeOut(1500);
        },3000); 
      }
      return false;
    }
    </script>
  </head>
  <body>    
    <?php
        include("menu_backend.php");
    ?>
  <br>
  <div class="container">
    <div class="jumbotron cajalogin" style="margin: auto auto; padding:10px; margin-bottom: 1em;">
      <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">
        <div class="form-group">
          <label for="orden_compra" class="etq_form" > Verificar Orden de Compra</label>
          <div id="grupo_orden_compra" class="input-group">
            <input type="text" name="orden_compra" id="orden_compra" class="validate[required, minSize[5], maxSize[30]] text-input, form-control cajas" placeholder="# de Orden de Compra" value="<?php echo $guia; ?>">
            <span class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="buscar_orden()">
              <span title="Buscar Orden" class="glyphicon glyphicon-search"> </span>
            </span>                  
          </div>
        </div>
      </form>
    </div>
    <br>
    <div id="resultado">
      
    </div>
  </div>
  <script src="../bootstrap/js/jquery.js"> </script>  
  <script language="javascript" type="text/javascript">  
    $(document).ready(function(){
      $('.cajas').keypress(function(e){   
        if(e.which == 13){      
          buscar_orden();
          return false;        
        }   
      });    
    });  
  </script>
  <script src="../bootstrap/js/bootstrap.min.js"> </script>  
  </body>
</html>
