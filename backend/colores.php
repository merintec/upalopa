<?php
include("../comunes/variables.php");
include("../comunes/verificar_admin.php");
include("../comunes/conexion.php");
$tabla='colores';
$titulos_busqueda = 'Nombre,Codigo Color';
$datos_busqueda = 'nom_color,html_color';
$nregistros = 5;      


?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
    <!-- para el color picker -->
    <script src="../js/tinycolor.min.js"></script> 
    <link href="../js/bootstrap.colorpickersliders.css" rel="stylesheet" type="text/css" media="all">
    <script src="../js/bootstrap.colorpickersliders.js"></script>
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <!-- validacion en vivo -->
<script >
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
   });
</script>
<!-- funcion de guardar formulario -->  
<script type="text/javascript">
$(function()
{
    $("#guardar").click(function()
    {
       $("#resultado").html("");
      if ($("#form1").validationEngine('validate')){
        var url="../comunes/funcion_guardar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: $("#form1").serialize(),
            success: function(data)
            {
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                $('#msg_imag').hide();
                $("#form1")[0].reset();
                $('#imagen_cargada').attr('src','');

              }
              $("#resultado").html(mensaje);
              setTimeout(function() {
                $("#msg_act").fadeOut(1500);
              },3000);
              
              buscar();
            }
        });
        return false;
      }
    });
});
function buscar()
{
  var url="../comunes/busqueda.php"; 
  $.ajax
  ({
    type: "POST",
    url: url,
    data: $("#form1").serialize(),
    success: function(data)
    {
      $("#resultado_busqueda").html(data);
    }
  });
  return false; 
}
</script>
<script type="text/javascript">

$(document).ready(function(){

    var button = $('#upload_button');
    new AjaxUpload('#upload_button', {
        action: 'subir_imagen_color.php',
        onSubmit : function(file , ext){
        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
            // extensiones permitidas
            var mensaje;
            mensaje='<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error... Solo se permiten imagenes</strong></div>';
             $("#resultado").html(mensaje);
            // cancela upload
            return false;
        } else {
            $('#status_imagen').html('Cargando...');
            this.disable();
        }
        },
        onComplete: function(file, response){
            // habilito upload button
            $('#status_imagen').html(response);            
            this.enable();          
            // Agrega archivo a la lista
            var nom_arch=file;
            $('#imag_color').val(nom_arch);
            $('#imagen_cargada').attr('src','../imagenes/uploads/colores/'+nom_arch);
            //html('.files').text(file);
        }   
    });
});


 
</script>




<body>
    <?php
        include("menu_backend.php");
    ?>


<div data-offset-top="100" class="container" data-spy="affix">
  <div id="resultado"></div>
</div>

<div class="jumbotron cajalogin">
      <div class="titulo_form">   Colores </div>
            <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">
                <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
                 <input type="hidden" name="var_titulos_busqueda" id="var_titulos_busqueda" value='<?php echo $titulos_busqueda; ?>'>
                 <input type="hidden" name="var_datos_busqueda" id="var_datos_busqueda" value='<?php echo $datos_busqueda; ?>'>
                <input type="hidden" name="var_nregistros" id="var_nregistros" value='<?php echo $nregistros; ?>'>
                <input type="hidden" name="var_pag_actual" id="var_pag_actual" value=''>
                 
                 <div class="form-group">
                    <label for="nom_color" class="etq_form" > Nombre:</label>
                    <div id="grupo_nom_color" class="input-group">
                        <input type="text" name="nom_color" id="nom_color" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[30]] text-input, form-control" placeholder="Nombre de Color">
                        <span id="boton_nom_color" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="buscar()";>
                                    <span id="buscar_nom_color" title="Buscar" class="glyphicon glyphicon-search"> </span>
                                    <span id="actualiza_nom_color" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                        </span>                  

                      </div>
                   </div>
                   

                  <div class="form-group">
                    <label for="html_color" class="etq_form" > Codigo:</label>
                    <div id="grupo_html_color" class="">
                      <input type="text" name="html_color" id="html_color"  class="validate[required, minSize[3], maxSize[30]] text-input, form-control" placeholder="Codigo HTML del Color">
                      <span id="boton_html_color" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                        <span id="actualiza_html_color" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                      </span>
                    </div>
                  </div>   


 


                  <div class="form-group">
                    <label for="nomb_mpago" class="etq_form" > Imagen:</label>
                    <div  id="grupo_imag_color" class="input-group">
                     <span id="upload_button" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="buscar()">
                         <span   class="glyphicon glyphicon-picture"> </span> 
                      </span>
                        <input type="text" name="imag_color" id="imag_color" class="form-control" readonly="readonly" placeholder="Subir Imagen">
                       <span id="boton_imag_color" class="input-group-addon" style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()">
                                    <span id="actualiza_imag_color" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                        </span>

                    </div>
                    <div id="status_imagen"></div>
                    <div class="text-center">
                    <img id="imagen_cargada" style="max-width:30%; border:1px solid #000; " src="" >                    
                  </div>

                  </div>


               

                <div align="center"><a href="<?php echo $_PHP_SELF; ?>"><button id="cancelar" type="button" class="btn btn_form oculto" >Cancelar</button></a>  <input type="submit" name="guardar"  id="guardar" value="Guardar" class="btn btn_form" > </div>
           
            </form>
          
           

</div>
 
 
<div class="row">
  <div id='resultado_busqueda' class="container">
    <?php 
      include("../comunes/busqueda.php");
    ?>
  </div>
</div>
<!-- funcion para color picker -->
<script type="text/javascript">
$("#html_color").ColorPickerSliders({
  size: 'sm',
  placement: 'right',
  swatches: false,
  sliders: false,
  hsvpanel: true,
  previewformat: 'hex'

});

$("#html_color").val('');
</script> 
  <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>

