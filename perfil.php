<?php
include_once("comunes/variables.php");
include_once("comunes/conexion.php");
$tabla = 'usuarios';
$sql_perfil = "SELECT * FROM usuarios WHERE id_user = ".$_SESSION['id_user'];
$perfil = mysql_fetch_array(mysql_query($sql_perfil));
$noti_add = "";
/// tipo de documento de identidad
if ($perfil[tipoi_user] == 'V'){
  $tid_add_v = "selected";
}
elseif ($perfil[tipoi_user] == 'E'){
  $tid_add_e = "selected";
}
else {
  $tid_add_v = "";
  $tid_add_e = "";
}
/// tipo de sexo
if ($perfil[sex_user] == 'M'){
  $sex_add_m = "selected";
}
elseif ($perfil[sex_user] == 'F'){
  $sex_add_f = "selected";
}
else {
  $sex_add_m = "";
  $sex_add_f = "";
}
/// suscrito al boletin de noticias
if ($perfil[noti_user]){
  $noti_add = "checked";
}
////// Onclick para actualizar datos
$oc_tipoi_user = "actualizar_campo_perfil('".$tabla."','tipoi_user',$('#tipoi_user').val(),'id_user',".$_SESSION['id_user'].");";
$oc_numi_user = "actualizar_campo_perfil('".$tabla."','numi_user',$('#numi_user').val(),'id_user',".$_SESSION['id_user'].");";
$oc_nom_ape_user = "actualizar_campo_perfil('".$tabla."','nom_ape_user',$('#nom_ape_user').val(),'id_user',".$_SESSION['id_user'].");";
$oc_sex_user = "actualizar_campo_perfil('".$tabla."','sex_user',$('#sex_user').val(),'id_user',".$_SESSION['id_user'].");";
$oc_pass_user = "actualizar_campo_perfil('".$tabla."','pass_user',$('#pass_user').val(),'id_user',".$_SESSION['id_user'].");actualizar_campo_perfil('".$tabla."','pass_user2',$('#pass_user2').val(),'id_user',".$_SESSION['id_user'].");";
$oc_noti_user = "actualizar_campo_perfil('".$tabla."','noti_user',$('#noti_user').val(),'id_user',".$_SESSION['id_user'].");";
?>
<!DOCTYPE html>
<script type="text/javascript">
function actualizar_campo_perfil(tabla,campo,valor,campo_id,valor_id)
{
  if (campo == 'noti_user') {
    if ($( "#noti_user" ).prop( "checked")){
      valor = '1';
    }
    else {
      valor = 'NULL';
    }
  }
  if (!$("#" + campo).validationEngine('validate'))
  {
      //para verificar si es una fecha voltearla al momento de guardarla
      var nueva;
      nueva = valor.split("-");
      if (valor.length == 10 && nueva[0].length == 2 && nueva[1].length == 2 && nueva[2].length == 4){
        valor = nueva[2]+'-'+nueva[1]+'-'+nueva[0];
      }
      var parametros = {
        "var_tabla": tabla,
        "var_campo" : campo,
        "var_valor" : valor,
        "var_id" : campo_id,
        "var_id_val" : valor_id
      };
      var url="comunes/funcion_actualizarcampo.php"; 
      $.ajax
      ({
        type: "POST",
          url: url,
          data: parametros,
          success: function(data)
          {
            $("#resultado_perfil").html(data);
            setTimeout(function() {
                $("#msg_act").fadeOut(1500);
            },3000);
          }
      });
      return false; 
  }
}
</script>
<div class="ventana-titulo">
  Tu Perfil<br>Upalopa
</div>
<div class="borde-ventana-punteada">
<button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -4.7em; margin-right: -5px;">×</button>
<html lang="es">
  <body>
  <div class="" style="margin:0px; margin-top: 7.5em; margin-left:auto; margin-right: auto;">
      <div data-offset-top="400" class="" data-spy="affix">
        <div id="resultado_perfil"></div>
      </div>
      <form method="POST" name="form1" id="form1" onsubmit="return jQuery(this).validationEngine('validate');">
        <label for="tipoi_user" class="etq_form" > Identificaci&oacute;n:</label> 
        <div class="row">   
          <div class="col-md-5 col-xs-5">
            <div class="form-group" style="margin: 0px;">
              <div class="input-group">
                <select name="tipoi_user" id="tipoi_user"   class="validate[required] fondo_campo form-control" style="padding-left: 10px;">';
                  <option  <?php echo $tid_add_v; ?> value="V" >V</option>
                  <option  <?php echo $tid_add_e; ?> value="E" >E</option>
                </select>
                <span class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="<?php echo $oc_tipoi_user; ?>">
                  <span title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk"> </span>
                </span>                  
              </div>
            </div>   
          </div>
          <div class="col-md-7 col-xs-7">  
            <div class="form-group" style="margin: 0px;">
              <div class="input-group">
                <input type="text" name="numi_user" id="numi_user" class="validate[required, custom[integer], minSize[6], maxSize[20]] text-input fondo_campo form-control" placeholder="N&uacute;mero Identificación" value="<?php echo $perfil[numi_user]; ?>">
                <span class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="<?php echo $oc_numi_user; ?>">
                  <span title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk"> </span>
                </span>                  
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-xs-12">  
            <div class="form-group" style="margin: 0px;">
              <label for="nom_ape_user" class="etq_form" >Nombre y Apellido:</label>
              <div class="input-group">
                <input type="text" name="nom_ape_user" id="nom_ape_user" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[60]] text-input fondo_campo form-control" placeholder="Nombre y Apellido del Usuario" value="<?php echo $perfil[nom_ape_user]; ?>">
                <span class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="<?php echo $oc_nom_ape_user; ?>">
                  <span title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk"> </span>
                </span>                  
              </div>
            </div>
          </div>   
        </div>   
        <div class="row">
          <div class="col-md-5 col-xs-4"> 
            <div class="form-group" style="margin: 0px;">
              <label for="sex_user" class="etq_form" > Sexo:</label>  
              <div class="input-group">
                <select name="sex_user" id="sex_user"   class="validate[required] fondo_campo form-control" style="padding-left: 10px;">';
                  <option  <?php echo $sex_add_f; ?> value="F" >F</option>
                  <option  <?php echo $sex_add_m; ?> value="M" >M</option>
                </select>
                <span class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="<?php echo $oc_sex_user; ?>">
                  <span title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk"> </span>
                </span>                  
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-xs-12">  
             <label for="corre_user" class="etq_form" >Correo Electronico:</label>
             <input type="text" name="corre_user" id="corre_user" class="validate[required, custom[email], minSize[5], maxSize[100]], text-input  form-control fondo_campo" style="background-color: #f2e6f0;" placeholder="Email" value="<?php echo $perfil[corre_user]; ?>" readonly="readonly">
          </div>  
        </div>
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="form-group" style="margin: 0px;">
              <label for="pass_user" class="etq_form" >Clave:</label>
              <div class="input-group">
                <input type="password" name="pass_user" id="pass_user" class="validate[required, equals[pass_user2]], minSize[5], maxSize[100]] text-input fondo_campo form-control" placeholder="Clave de Acceso">
                <input type="password" name="pass_user2" id="pass_user2" class="validate[required, equals[pass_user]] text-input fondo_campo form-control" placeholder="Repetir Clave de Acceso">
                <span class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="<?php echo $oc_pass_user; ?>">
                  <span title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk"> </span>
                </span>                  
              </div>
            </div>
          </div>
        </div>
        <div class="checkbox">
          <label class="etiqueta_check" >
            <input type="checkbox" <?php echo $noti_add; ?> id="noti_user" name="noti_user" value="1" onclick="<?php echo $oc_noti_user; ?>"> Suscribirse a nuestro Newsletter, Recibirás ofertas y descuentos exclusivos, así como actualizaciones.  
          </label>  
        </div>
      </form>
    </div>
  </body>
</html>
</div>