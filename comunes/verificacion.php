<?php 
include_once('conexion.php');
include_once('variables.php');
echo '<meta charset="utf-8">';
/// verificar las ventas con N días de antiguedad sin procesar;
$hoy = date('Y-m-d');
$fechaComparacion = strtotime($hoy);
$tope = strtotime("-".$ndias_carrito." days", $fechaComparacion);
$tope = date('Y-m-d', $tope);
$sql_consulta = "SELECT * FROM venta v, usuarios u WHERE v.status_venta = 'carrito' AND v.aviso=1 AND v.fini_venta <= '".$tope."' AND v.id_user = u.id_user";
//echo $sql_consulta = "SELECT * FROM venta v, usuarios u WHERE v.id_user = u.id_user AND u.id_user=34";
$bus_consulta = mysql_query($sql_consulta);
while ($res_consulta = mysql_fetch_array($bus_consulta)){
	$sex = 'o(a)';
	if ($res_consulta[sex_user]=='F'){
		$sex = 'a';
	}elseif ($res_consulta[sex_user]=='M') {
		$sex = 'o';
	}
	$mensaje = "Estimad".$sex." ".$res_consulta[nom_ape_user].",<p style='padding-top: 20px;'>Antes que nada reciba un cordial saludo de la Tienda <b>Upalopa.com</b>.</p> <p style='padding-top: 20px;'>Te notificamos que hemos procedido a retirar los productos que tenías en tu Cesta de Compras en <a href='http://www.upalopa.com'>UPALOPA.COM</a>, ya que tal como te informamos en los <a href='http://www.upalopa.com/frontend/index.php?page=tac'>Términos y Condiciones</a>, han transcurrido ".($ndias_carrito-1)." días desde que iniciaste tu compra y no la has procesado.</p><p style='padding-top: 20px;'>Nos despedimos muy cordialmente,<br><a href='http://upalopa.com'>www.upalopa.com</a><br>Su tienda en línea.</p><p style='padding-top: 20px;'><a href='mailto:".$correo_compras."'>".$correo_compras."</a></p>";
	$destino = $res_consulta[corre_user];
	$correo_origen = $correo_compras;
	$titulo = 'Notificación de limpieza de cesta ';
	include ('enviar_correo.php');
	$sql_delete = "DELETE FROM venta WHERE id_venta = ".$res_consulta[id_venta];
	mysql_query($sql_delete);
}
/// verificar las ventas con N-1 días de antiguedad sin procesar para recordar;
$hoy = date('Y-m-d');
$fechaComparacion = strtotime($hoy);
$tope = strtotime("-".($ndias_carrito-1)." days", $fechaComparacion);
$tope = date('Y-m-d', $tope);
$sql_consulta = "SELECT * FROM venta v, usuarios u WHERE v.status_venta = 'carrito' AND v.aviso=0 AND v.fini_venta <= '".$tope."' AND v.id_user = u.id_user";
//echo $sql_consulta = "SELECT * FROM venta v, usuarios u WHERE v.status_venta = 'carrito' AND v.aviso=0 AND v.id_user = u.id_user AND u.id_user=34";
$bus_consulta = mysql_query($sql_consulta);
while ($res_consulta = mysql_fetch_array($bus_consulta)){
	$sex = 'o(a)';
	if ($res_consulta[sex_user]=='F'){
		$sex = 'a';
	}elseif ($res_consulta[sex_user]=='M') {
		$sex = 'o';
	}
	$mensaje = "Estimad".$sex." ".$res_consulta[nom_ape_user].",<p style='padding-top: 20px;'>Antes que nada reciba un cordial saludo de la Tienda <b>Upalopa.com</b> agradeciéndole su gentileza de visitar nuestra página web <a href='http://upalopa.com'>www.upalopa.com</a>.</p> <p style='padding-top: 20px;'>Hemos notado en nuestra plataforma que no has culminado el proceso de compra y tu cesta de productos podría ser abandonada.</p><p style='padding-top: 20px;'>Sería para nosotros un placer tenerl".$sex." como cliente y le invitamos a procesar su compra con éxito.</p><p style='padding-top: 20px;'>Con el deseo de enviarle sus productos a la brevedad posible, nos despedimos.</p><p style='padding-top: 20px;'>Muy Cordialmente,</p><p style='padding-top: 20px;'> <a href='http://upalopa.com'>www.upalopa.com</a><br>Su tienda en línea.</p><p style='padding-top: 20px;'><a href='mailto:".$correo_compras."'>".$correo_compras."</a></p>";
	$destino = $res_consulta[corre_user];
	$correo_origen = $correo_compras;
	$titulo = 'Recordatorio de compra en carrito ';
	include ('enviar_correo.php');
	$actualizar="UPDATE venta set aviso=1 where id_venta='$res_consulta[id_venta]'";
	$actu=mysql_query($actualizar);
	
}
/// verificar las ventas con N días de antiguedad sin registro de pago;
$ndia = date('N');
if ($ndia >=2 && $ndia<=5){
	$hoy = date('Y-m-d');
	$fechaComparacion = strtotime($hoy);
	$tope = strtotime("-".$ndias_venta." days", $fechaComparacion);
	$tope = date('Y-m-d', $tope);
	$sql_consulta = "SELECT * FROM venta v, usuarios u WHERE v.status_venta = 'procesado' AND v.aviso=2 AND v.fech_stat <= '".$tope."' AND v.id_user = u.id_user and v.id_venta NOT IN (SELECT id_venta FROM pago)";
	//echo $sql_consulta = "SELECT * FROM venta v, usuarios u WHERE v.status_venta = 'procesado' AND v.id_user = u.id_user AND u.id_user=34";
	$bus_consulta = mysql_query($sql_consulta);
	while ($res_consulta = mysql_fetch_array($bus_consulta)){
		$sex = 'o(a)';
		if ($res_consulta[sex_user]=='F'){
			$sex = 'a';
		}elseif ($res_consulta[sex_user]=='M') {
			$sex = 'o';
		}
		$mensaje = "Estimad".$sex." ".$res_consulta[nom_ape_user].",<p style='padding-top: 20px;'>Antes que nada reciba un cordial saludo de la Tienda <b>Upalopa.com</b>.</p> <p style='padding-top: 20px;'>Te notificamos que hemos procedido a eliminar tu orden de compra <font color='#ff0000'><b>#".$res_consulta[codg_trans]."</b></font>, ya que tal como te informamos en los <a href='http://www.upalopa.com/frontend/index.php?page=tac'>Términos y Condiciones</a>, han transcurrido ".($ndias_carrito-1)." días desde que procesaste tu compra y no has efectuado el registro del pago correspondiente.</p><p style='padding-top: 20px;'>Nos despedimos muy cordialmente,<br><a href='http://upalopa.com'>www.upalopa.com</a><br>Su tienda en línea.</p><p style='padding-top: 20px;'><a href='mailto:".$correo_compras."'>".$correo_compras."</a></p>";
		$destino = $res_consulta[corre_user];
		$correo_origen = $correo_compras;
		$titulo = 'Notificación orden de compra ha caducado ';
		include ('enviar_correo.php');
		$sql_delete = "DELETE FROM venta WHERE id_venta = ".$res_consulta[id_venta];
		mysql_query($sql_delete);
	}
}
/// verificar las ventas con N-1 días de antiguedad sin registro de pago para recordar;
	$ndia = date('N');
	$hoy = date('Y-m-d');
	$fechaComparacion = strtotime($hoy);
	$tope = strtotime("-".($ndias_venta-1)." days", $fechaComparacion);
	$tope = date('Y-m-d', $tope);
	$sql_consulta = "SELECT * FROM venta v, usuarios u WHERE v.status_venta = 'procesado' AND v.aviso!=2 AND v.fech_stat <= '".$tope."' AND v.id_user = u.id_user";
	//echo $sql_consulta = "SELECT * FROM venta v, usuarios u WHERE v.status_venta = 'procesado' AND v.aviso!=2 AND v.id_user = u.id_user AND u.id_user=34";
	$bus_consulta = mysql_query($sql_consulta);
	while ($res_consulta = mysql_fetch_array($bus_consulta)){
		$sex = 'o(a)';
		if ($res_consulta[sex_user]=='F'){
			$sex = 'a';
		}elseif ($res_consulta[sex_user]=='M') {
			$sex = 'o';
		}
		$mensaje = "Estimad".$sex." ".$res_consulta[nom_ape_user].",<p style='padding-top: 20px;'>Antes que nada reciba un cordial saludo de la Tienda <b>Upalopa.com</b> agradeciéndole su gentileza de visitar nuestra página web <a href='http://upalopa.com'>www.upalopa.com</a>.</p> <p style='padding-top: 20px;'>Hemos notado en nuestra plataforma que no has registrado el pago de tu compra y tu cesta de productos podría ser abandonada.</p><p style='padding-top: 20px;'>Sería para nosotros un placer tenerl".$sex." como cliente. Te invitamos a registrar el pago correspondiente.</p><p style='padding-top: 20px;'>Con el deseo de enviarle sus productos a la brevedad posible, nos despedimos.</p><p style='padding-top: 20px;'>Muy Cordialmente,</p><p style='padding-top: 20px;'> <a href='http://upalopa.com'>www.upalopa.com</a><br>Su tienda en línea.</p><p style='padding-top: 20px;'><a href='mailto:".$correo_compras."'>".$correo_compras."</a></p>";
		$destino = $res_consulta[corre_user];
		$correo_origen = $correo_compras;
		$titulo = 'Recordatorio orden de compra próxima a caducar ';
		include ('enviar_correo.php');
		$actualizar="UPDATE venta set aviso=2 where id_venta='$res_consulta[id_venta]'";
		$actu=mysql_query($actualizar);		
	}
/// marcar vencidos cupones que no se usaron antes de la fecha de caducidad
	$sql_cupones_v = "UPDATE cupones_descuentos SET stat_cupon = 'vencido' WHERE ffin_cupon < '".date('Y-m-d')."' AND stat_cupon = 'enviado'";
	mysql_query($sql_cupones_v);
?>