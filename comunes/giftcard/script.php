<?php
	//header("Content-type: image/png");
	$font = 'obcecada-sans-ffp-webfont.ttf';
	$font2 = 'roboto-thin-webfont.ttf';
	$para = utf8_decode($_GET['para']);
	$texto = utf8_decode($_GET['texto']);
	$texto = split(' ', $texto);
	$de = 'DE: '.utf8_decode($_GET['de']);
	$url = 'http://www.upalopa.com';
	$max_text = 25;
	$linea=0;
	$cuenta = 0;
	for($i=0;$i<=sizeof($texto); $i++){
		$cuenta = $cuenta + strlen($texto[$i]) + 1;
		if ($max_text<$cuenta){
			$linea=$linea+1;
			$cuenta = 0;
		}
		$texto_mostrar[$linea] .= ' '.$texto[$i];
	}
	$precio = utf8_decode($_GET['precio']);
	$precio = 'Bs. '.number_format($precio,2,',','.');
	$im     = imagecreatefrompng("base.png");
	$color1 = imagecolorallocate($im, 0, 0, 0);
	$color2 = imagecolorallocate($im, 0, 0, 0);
	$color3 = imagecolorallocate($im, 255, 255, 255);
	//array imagettftext ( resource $image , float $size , float $angle , int $x , int $y , int $color , string $fontfile , string $text )
	$px = (imagesx($im) - 7.5 * strlen($para)) - 150;
	imagettftext($im, 35, 0, $px, 100, $color, $font, $para);
	$px = 390;
	$py = 265;
	for ($i=0;$i<=$linea;$i++){
		imagettftext($im, 20, 0, $px, $py, $color2, $font2, $texto_mostrar[$i]);
		$py=$py+30;		
	}
	$px = (imagesx($im) - 7.5 * strlen($precio)) - 210;
	imagettftext($im, 35, 0, $px, 205, $color2, $font2, $precio);
	imagettftext($im, 35, 0, $px, 204, $color3, $font2, $precio);
	imagettftext($im, 35, 0, $px-1, 204, $color3, $font2, $precio);
	$px = 300;
	imagettftext($im, 18, 0, $px, 430, $color2, $font2, $de);
	imagettftext($im, 18, 0, $px+1, 430, $color2, $font2, $de);
	$px = (imagesx($im) - 7.5 * strlen($de))-20;
	imagettftext($im, 15, 0, $px, 30, $color2, $font2, $url);
	imagepng($im);
	imagedestroy($im);
?>