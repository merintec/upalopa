<meta charset="utf-8">
<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once ('lib/mercadopago.php');
$mp = new MP('5909923339367308', '2dqGOZUXMKfrwepZUxLG8HBKvmHIRwpf');
$preference_data = array(
	"payer" => array(
		"name" => "Agapito",
		"surname" => "SantaLucia",
		"email" => "test_user_28216468@testuser.com"
	),
	"payment_methods" => array(
		"excluded_payment_types" => array( 
			array( "id"=>"ticket"), 
			array( "id"=>"bank_transfer"), 
			array( "id"=>"atm"), 
			//array( "id"=>"credit_card"), 
			//array( "id"=>"debit_card"), 
			//array("id"=>"digital_currency"),
			//array("id"=>"digital_prepaid_card") 
		)	  
	),
	"items" => array(
		array(
			"title" => "UPALOPA. Compra #87987123",
			"picture_url" =>"http://upalopa.com/imagenes/sistema/logo.png",
			"quantity" => 1,
			"currency_id" => "VEF",
			"unit_price" => 10.00
		)
	)
);
$preference = $mp->create_preference($preference_data);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Pay</title>
	</head>
	<body>
		<script type="text/javascript">
			function execute_my_onreturn (json) {

			    if (json.collection_status=='approved'){
			        alert ('Pago acreditado' + json.collection_id);
			    } else if(json.collection_status=='pending'){
			        alert ('El usuario no completó el pago');
			    } else if(json.collection_status=='in_process'){    
			        alert ('El pago está siendo revisado');    
			    } else if(json.collection_status=='rejected'){
			        alert ('El pago fué rechazado, el usuario puede intentar nuevamente el pago');
			    } else if(json.collection_status==null){
			        alert ('El usuario no completó el proceso de pago, no se ha generado ningún pago');
			    }
			}
		</script>
		<a href="<?php echo $preference['response']['init_point']; ?>" name="MP-Checkout" class="grey-S-Ov-VeOn" mp-mode="modal" onreturn="execute_my_onreturn">Pagar</a>
		<!-- Código para ejecutar aplicación de estilos y otros. Debe estar antes de cerrar el body-->
		<script type="text/javascript">
		    (function(){function $MPC_load(){window.$MPC_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;
		    s.src = document.location.protocol+"//resources.mlstatic.com/mptools/render.js";
		    var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPC_loaded = true;})();}
		    window.$MPC_loaded !== true ? (window.attachEvent ? window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;})();
		</script>
	</body>
</html>
