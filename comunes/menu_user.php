<div id="menu_user"  onclick="muestra_oculta(this.id)"  class="pop_user hidden-xs" style="display: none;">
    <button class="btn btn_menu fondo_boton" title="Tus datos personales"  data-toggle="modal" data-target="#perfil">Tú Perfil<span class="vineta-user"></span></button>
    <a href="<?php echo $url_add_menu; ?>procesar.php" title="Revisa su cesta de compras actual"><button class="btn btn_menu fondo_boton">Tú Cesta de Compras<span class="vineta-cesta-lg"></span></button></a>
    <a href="<?php echo $url_add_menu; ?>pagos.php" title="Registra o verifica tus pagos"><button class="btn btn_menu fondo_boton">Tus Pagos<span class="vineta-money"></span></button></a>
    <a href="<?php echo $url_add_menu; ?>compras.php" title="Listado de las compras efectuadas"><button class="btn btn_menu fondo_boton">Tus Compras<span class="vineta-compras"></span></button></a>
    <a href="<?php echo $url_add_menu; ?>cupones.php" title="Cupones de Descuentos"><button class="btn btn_menu fondo_boton">Tus Cupones <span class="vineta-cupon"></span></button></a>
    <a href="<?php echo $url_add_menu; ?>giftcard.php" title="Tarjetas de Regalo"><button class="btn btn_menu fondo_boton">Tus GiftCard <span class="vineta-giftc"></span></button></a>
    <a href="<?php echo $url_add_menu; ?>lista_regalo.php" title="Lista de Regalo"><button class="btn btn_menu fondo_boton">Tus Listas de Regalo <span class="vineta-lista-regalo"></span></button></a>

    <button class="btn btn_menu fondo_boton" title="Tus datos personales"  data-toggle="modal" data-target="#perfil">Cambio de Contraseña<span class="vineta-candado"></span></button>
    <div style="float:right" title="Cerrar la sesión actual">
        <a href="<?php echo $url_add_menu; ?>../comunes/cerrar_sesion.php" style="color: #000; font-weight: bold; text-decoration: none;">Salir <span class="vineta-salir"></span> </a>
    </div>
</div>
<div id="menu_user_xs"  onclick="muestra_oculta(this.id)" class="pop_user_xs" style="display: none;">
    <button class="btn btn_menu fondo_boton" title="Tus datos personales"  data-toggle="modal" data-target="#perfil">Tú Perfil<span class="vineta-user"></span></button>
    <a href="<?php echo $url_add_menu; ?>procesar.php" title="Revisa su cesta de compras actual"><button class="btn btn_menu fondo_boton">Tú Cesta de Compras<span class="vineta-cesta-lg"></span></button></a>
    <a href="<?php echo $url_add_menu; ?>pagos.php" title="Registra o verifica tus pagos"><button class="btn btn_menu fondo_boton">Tus Pagos<span class="vineta-money"></span></button></a>
    <a href="<?php echo $url_add_menu; ?>compras.php" title="Listado de las compras efectuadas"><button class="btn btn_menu fondo_boton">Tus Compras<span class="vineta-compras"></span></button></a>
    <a href="<?php echo $url_add_menu; ?>cupones.php" title="Cupones de Descuentos"><button class="btn btn_menu fondo_boton">Tus Cupones <span class="vineta-cupon"></span></button></a>
    <a href="<?php echo $url_add_menu; ?>giftcard.php" title="GiftCards Recibidas"><button class="btn btn_menu fondo_boton">Tus GiftCards <span class="vineta-giftc"></span></button></a>
    <a href="<?php echo $url_add_menu; ?>lista_regalo.php" title="Listas de Regalo Creadas"><button class="btn btn_menu fondo_boton">Tus Listas de Regalo <span class="vineta-lista-regalo"></span></button></a>
    <button class="btn btn_menu fondo_boton" title="Tus datos personales"  data-toggle="modal" data-target="#perfil">Cambio de Contraseña<span class="vineta-candado"></span></button>
    <div style="float:right" title="Cerrar la sesión actual">
        <a href="<?php echo $url_add_menu; ?>../comunes/cerrar_sesion.php" style="color: #000; font-weight: bold; text-decoration: none;">Salir <span class="vineta-salir"></span> </a>
    </div>
</div>