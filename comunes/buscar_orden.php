<?php
	include_once('conexion.php');
	$codg_trans = $_POST[codg_trans];
	$sql_venta = "SELECT * FROM venta v WHERE v.codg_trans = '".$codg_trans."'";
	if($res_venta = mysql_fetch_array(mysql_query($sql_venta))){
    $enviar_regalo=$res_venta[regalo];
    if ($enviar_regalo=='')
    {
      $enviar_regalo='NO';
    }
		$mensaje="<div class='alert alert-info' id='msg_act'>
		<button type='button' class='close' data-dismiss='alert'>&times;</button>
		<span class='glyphicon glyphicon-info-sign pull-left'></span>&nbsp;&nbsp;
		<strong>La orden de compra # ".$codg_trans." se ha encontado exitosamente.</strong></div>";
			///// estado de la venta
	    if ($res_venta[status_venta]=='carrito')
        {
	        $icono= '<button style="width:13em;" type="button" class="btn btn-warning" title="En Carrito de Compra. El cliente aun no ha procesado" > En carrito de compra <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span><br>'.date("d-m-Y", strtotime($res_venta[fech_stat])).'</button> ';
        }
        if ($res_venta[status_venta]=='procesado')
        {
	        $icono= '<button style="width:17em;" type="button" class="btn btn-warning" title="En espera de registro de pago por el cliente" > Espera de Registro de Pago <span class="glyphicon glyphicon-time" aria-hidden="true"></span><br>'.date("d-m-Y", strtotime($res_venta[fech_stat])).'</button> ';
        }
	    if ($res_venta[status_venta]=='pagado')
        {
	        $icono= '<button style="width:13em;" type="button" class="btn btn-warning" title="En espera de confirmación de pago" > Confirmación de Pago <span class="glyphicon glyphicon-time" aria-hidden="true"></span><br>'.date("d-m-Y", strtotime($res_venta[fech_stat])).'</button> ';
        }
        if ($res_venta[status_venta]=='confirmado')
        {
            $icono= '<button style="width:13em;" type="button" class="btn btn-info" title="Pago Confirmador. En espera de envío" > Por Enviar <span class="glyphicon glyphicon-time" aria-hidden="true"></span><br>'.date("d-m-Y", strtotime($res_venta[fech_stat])).'</button> ';
        }
        if ($res_venta[status_venta]=='enviado')
        {
            $icono= '<button style="width:13em;" type="button" class="btn btn-primary" title="Enviado" > Enviado <span class="glyphicon glyphicon-plane" aria-hidden="true"></span><br>'.date("d-m-Y", strtotime($res_venta[fech_stat])).'</button> ';
        }
        if ($res_venta[status_venta]=='entregado')
        {
            $icono= '<button style="width:13em;" type="button" class="btn btn-success" title="Entregado al cliente" > Entregado!!! <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span><br>'.date("d-m-Y", strtotime($res_venta[ffin_venta])).'</button> ';
        }
			///// datos de envio
			$sql_dir = "SELECT * FROM direccion_envio de, parroquias p, municipios m, estados e WHERE de.id_dire = ".$res_venta[id_dire]. " AND de.id_parroquia = p.id_parroquia AND de.id_municipio = m.id_municipio AND de.id_estado = e.id_estado";
			if ($res_dir = mysql_fetch_array(mysql_query($sql_dir))){
				$datos_dir  = '<table class="table table-striped" style="margin-bottom: 0px;">';
				$datos_dir .= '<tr><td>Nombre:</td><td><b>&nbsp;'.$res_dir[nomb_dire].'</b></td></tr>';
				$datos_dir .= '<tr><td>Cédula: </td><td><b>&nbsp;'.$res_dir[cedu_dire].'</b></td></tr>';
				$datos_dir .= '<tr><td>Teléfono:</td><td><b>&nbsp;'.$res_dir[telf_dire].'</b></td></tr>';
				$datos_dir .= '<tr><td>Estado:</td><td><b>&nbsp;'.$res_dir[estado].'</b></td></tr>';
				$datos_dir .= '<tr><td>Municipio:</td><td><b>&nbsp;'.$res_dir[municipio].'</b></td></tr>';
				$datos_dir .= '<tr><td>Parroquia:</td><td><b>&nbsp;'.$res_dir[parroquia].'</b></td></tr>';
				$datos_dir .= '<tr><td>Dirección:</td><td><b>&nbsp;'.$res_dir[direccion].'</b></td></tr>';
				$datos_dir .= '<tr><td>Punto de Referencia:</td><td><b>&nbsp;'.$res_dir[punt_refe_dire].'</b></td></tr>';
				$datos_dir .= '<tr><td>Enviar como Regalo:</td><td><h4><b>&nbsp;'.$enviar_regalo.'</b></h4></td></tr>';

        $datos_dir .= '</table>';
			}
			else{
				$datos_dir = 'No se han registrado datos de Envío';
			}
			$resultado_dir = '<div class="caja_punteada"><span class="text-info"><h4 style="margin: 0px">Dirección de Envío</h4></span>'.$datos_dir.'</div>';
			///// datos de los pagos
	        $sql_pago = "SELECT * FROM pago p, cuentas_bancarias cb, metodo_pago mp WHERE p.id_venta = ".$res_venta[id_venta]." AND p.id_cuen_banc=cb.id_cuen_banc AND p.id_mpago=mp.id_mpago";
			$datos_pago  = '<table  class="table table-striped table-hover"  style="margin-bottom: 0px;">'; 
	        $datos_pago .= '<tr> <th># </th> <th style="width:8.2em;">Fecha</th> <th>Tipo</th> <th>Destino</th> <th>Referencia</th> <th>Monto</th><th>Status</th></tr>';
			$bus_pagos = mysql_query($sql_pago);
			$count_pago = 1;
			while ($res_pagos=mysql_fetch_array($bus_pagos)){
				$fecha = strtotime($res_pagos[fech_pago]);
				$res_pagos[fech_pago] = date("d-m-Y", $fecha);
				$datos_pago .= '<tr title="Realizó el pago: '.$res_pagos[nomb_per_pago].' '."\n".' Doc. Ident: '.$res_pagos[iden_per_pago].'. '."\n".' Desde: '.$res_pagos[banc_origen].'"> <td class="text_right">'.$count_pago.'</td> <td>'.$res_pagos[fech_pago].'</td><td>'.$res_pagos[nomb_mpago].'</td> <td>'.$res_pagos[desc_cuen_banc].'</td> <td class="text-center">'.$res_pagos[num_refe_banc].'</td> <td class="text_right">'.number_format($res_pagos[mont_pago],2,",",".").'</td><td><span id="status'.$res_pagos[id_pago].'">'.ucwords($res_pagos[status_pago]).'</span></td></tr>';
				$count_pago++;
        if ($res_pagos[status_pago]=='Aprobado'){
          $total_pagos += $res_pagos[mont_pago];
        }
			}
      $sql_pagos = "SELECT * FROM pago p, metodo_pago mp WHERE p.id_venta = ".$res_venta[id_venta]." AND p.id_cuen_banc IS NULL AND p.id_mpago=mp.id_mpago";
      $bus_pagos = mysql_query($sql_pagos);
      while ($res_pagos=mysql_fetch_array($bus_pagos)){
        $fecha = strtotime($res_pagos[fech_pago]);
        $res_pagos[fech_pago] = date("d-m-Y", $fecha);
        $datos_pago .= '<tr title="Realizó el pago: '.$res_pagos[nomb_per_pago].' '."\n".' Doc. Ident: '.$res_pagos[iden_per_pago].'. '."\n".' Desde: '.$res_pagos[banc_origen].'"> <td class="text_right">'.$count_pago.'</td> <td>'.$res_pagos[fech_pago].'</td><td>'.$res_pagos[nomb_mpago].'</td> <td>'.$res_pagos[desc_cuen_banc].'</td> <td class="text-center">'.$res_pagos[num_refe_banc].'</td> <td class="text_right">'.number_format($res_pagos[mont_pago],2,",",".").'</td><td><span id="status'.$res_pagos[id_pago].'">'.ucwords($res_pagos[status_pago]).'</span></td></tr>';
        $count_pago++;
        if ($res_pagos[status_pago]=='Aprobado'){
          $total_pagos += $res_pagos[mont_pago];
        }
      }
      if ($count_pago>1){
        $datos_pago .= '<tr><th class="text-right" colspan="5">TOTAL PAGOS &nbsp;</th><th class="text_right">'.number_format($total_pagos,2,",",".").'</th><th>&nbsp;</th></tr>';
      }
      else {
        $datos_pago = '<table><tr><td colspan="7">No se han registrado Pagos</td></tr></table>';
      }



	        $datos_pago .= '</table>';
	   		$resultado_pag = '<div class="table-responsive caja_punteada"><span class="text-info"><h4 style="margin: 0px">Datos de los Pagos</h4></span>'.$datos_pago.'</div>';
			///// datos de envio realizado
			$sql_dir = "SELECT * FROM envios e, empresa_envio ee WHERE e.id_venta = ".$res_venta[id_venta]." AND e.id_empr_envi = ee.id_empr_envi";
			if ($res_dir = mysql_fetch_array(mysql_query($sql_dir))){
				$fecha = strtotime($res_dir[fech_envio]);
				$res_dir[fech_envio] = date("d-m-Y", $fecha);
				$datos_dir  = '<table class="table table-striped"  style="margin-bottom: 0px;">';
				$datos_dir .= '<tr><td>Empresa:</td><td><b>&nbsp;'.$res_dir[nomb_empr_envi].'</b></td></tr>';
				$datos_dir .= '<tr><td>Fecha: </td><td><b>&nbsp;'.$res_dir[fech_envio].'</b></td></tr>';
				$datos_dir .= '<tr><td>Guía:</td><td><b>&nbsp;'.$res_dir[guia_envio].'</b></td></tr>';
				$datos_dir .= '</table>';
			}
			else{
				$datos_dir = 'No se han efectuado el envío';
			}
			$resultado_env = '<div class="caja_punteada"><span class="text-info"><h4 style="margin: 0px">Datos del Envío</h4></span>'.$datos_dir.'</div>';
			///// datos de facturacion
			$sql_fact = "SELECT * FROM datos_fact WHERE id_datos = ".$res_venta[id_datos];
			if ($res_fact = mysql_fetch_array(mysql_query($sql_fact))){
				$datos_fact  = '<table class="table table-striped"  style="margin-bottom: 0px;">';
				$datos_fact .= '<tr><td>RIF:</td><td><b>&nbsp;'.$res_fact[tiden_datos].'-'.$res_fact[iden_datos].'</b></td></tr>';
				$datos_fact .= '<tr><td>Razón Social: </td><td><b>&nbsp;'.$res_fact[razon_datos].'</b></td></tr>';
				$datos_fact .= '<tr><td>Dirección:</td><td><b>&nbsp;'.ucwords($res_fact[dire_datos]).'</b></td></tr>';
				$datos_fact .= '<tr><td>Teléfonos:</td><td><b>&nbsp;'.$res_fact[telfh_datos].' / '.$res_fact[telfm_datos].'</b></td></tr>';
				$datos_fact .= '</table>';
			}
			else{
				$datos_fact = 'No se han registrado datos de Facturación';
			}
			$resultado_fac = '<div class="caja_punteada"><span class="text-info"><h4 style="margin: 0px">Datos de Facturación</h4></span>'.$datos_fact.'</div>';
			/// preparando para mostrar
			$resultado = '<div class="col-md-12 col-xs-12 text-center text-info" style="margin-top: -1em; margin-bottom: 1em;"><h4>Estado actual de la Venta</h4>'.$icono.'</div>';
			$resultado .= '<div class="row" style="font-size: 0.8em">';
			$resultado .= '<div class="col-md-6 col-xs-6">'.$resultado_dir.''.$resultado_env.'</div>';
			$resultado .= '<div class="col-md-6 col-xs-6">'.$resultado_fac.''.$resultado_pag.'</div>';
			$resultado .= '</div>';



			//aqui va


	}
	else{
		 $mensaje="<div class='alert alert-danger' id='msg_act'>
         <button type='button' class='close' data-dismiss='alert'>&times;</button>
         <span class='glyphicon glyphicon-alert pull-left'></span>&nbsp;&nbsp;
         <strong>La orden de compra # ".$codg_trans." no se encuentra registrada. Por favor verifique los datos y vuelva a intentarlo.</strong></div>";
	}
	$pie = '<div class="text-center"><hr style="margin-bottom: 0.5em; "><a href="../index.php">Upalopa.com</a></div>';
	echo $mensaje.''.$resultado;


 		$consulta="SELECT * FROM venta as v  where v.id_venta='$res_venta[id_venta]' ";
        $consulta=mysql_query($consulta);

        $i=0;
        while($fila=mysql_fetch_array($consulta))
        {

          $enviar_regalo=$fila[regalo];
          $totalp=0;
          $total_pagar=0;
          $total_cant=0;
          $i++;
                   $consulta2="SELECT * FROM venta_productos as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
                   vp.id_venta='$res_venta[id_venta]' and vp.status_vent='procesado' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
                  $consulta2=mysql_query($consulta2);
                  while ($fila2=mysql_fetch_array($consulta2)) 
                  {
                     $totalp=$fila2[vuni_venta_prod]*$fila2[cant_venta_prod];
                     $total_pagar += $totalp;
                     $total_cant += $fila2[cant_venta_prod];
                     $peso_cu=$fila2[peso_prod] * $fila2[cant_venta_prod];
                     $total_peso += $peso_cu;

                      $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
                      $consulta_envio=mysql_query($consulta_envio);
                      $con_env=mysql_fetch_assoc($consulta_envio);;

                      if (!$con_env[id_tenvios])
                      {

                            $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                            $consulta_envio=mysql_query($consulta_envio);
                            $con_env=mysql_fetch_assoc($consulta_envio);

                            $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);

                      }


                    }

                  

                  
                  
                        
                          echo '<div class="row caja_punteada">';
                            echo '<div class="col-md-12 col-xs-12">';
                                  echo '<span class="text-info" ><b>Productos de la Compra</b></span>';
                                  echo '<table  class="table table-striped table-hover" style="font-size: 0.7em">'; 
                                  echo '<tr> <th># </th> <th>Producto</th> <th>Cantidad </th> <th> Precio Unitario </th> <th> Total </th> </tr>';

                                  //consulta para mostrar detalle de articulos
                                  $consulta3="SELECT * FROM venta_productos as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
                                   vp.id_venta='$fila[id_venta]' and vp.status_vent='procesado' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
                                  $consulta3=mysql_query($consulta3);
                                  $j=0;
                                    

                                  while ($fila3=mysql_fetch_array($consulta3)) 
                                  {
                                      $totalp=$fila[vuni_venta_prod]*$fila[cant_venta_prod];
                                      $imp_cu = ($totalp / (($val_imp/100)+1));
                                      $imp_cu = $imp_cu * $val_imp / 100;
                                      $imp_cu = number_format($imp_cu,2,'.','');
                                      //// para los totales generales
                                      $peso_cu=$fila[peso_prod] * $fila[cant_venta_prod];
                                      $total_pagar += $totalp;
                                      $total_peso += $peso_cu;
                                      $total_imp += $imp_cu;
                                      $total_cant += $fila[cant_venta_prod];
                                      $subtotal=$fila3[vuni_venta_prod]*$fila3[cant_venta_prod];

                                      $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
                                      $consulta_envio=mysql_query($consulta_envio);
                                      $con_env=mysql_fetch_assoc($consulta_envio);;

                                      if (!$con_env[id_tenvios])
                                      {

                                            $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                                            $consulta_envio=mysql_query($consulta_envio);
                                            $con_env=mysql_fetch_assoc($consulta_envio);

                                            $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);

                                      }
                                      $j++;
                                              echo '<tr> <td>'.$j.'</td><td>'.$fila3[nomb_prod].' </td> <td align="center">'.$fila3[cant_venta_prod].'</td> <td align="right">'.number_format($fila3[vuni_venta_prod],2,",",".").' </td><td align="right">'.number_format($subtotal,2,",",".").' </td> </tr>';

                                     
                                      
                                  }   $final_pagar = number_format(($total_pagar+$con_env[prec_tenvio]+($total_pagar * ($con_env[porc_tenvio]/100))),2,".","");
                                      echo '<tr><td align="right" colspan="3"><b>Subtotal: Bs.</b> </td><td align="right" colspan="2"><b>'.number_format($total_pagar,2,",",".").'</b></td></tr>';
                                      echo '<tr><td align="right" colspan="3"><b>Costo de Envio: Bs. </b></td><td align="right" colspan="2"><b>'.number_format($con_env[prec_tenvio],2,",",".").'</b></td></tr>';
                                      echo '<tr><td align="right" colspan="3"><b>Costo de Seguro: Bs.</b> </td><td align="right" colspan="2"><b>'.number_format(($total_pagar * ($con_env[porc_tenvio]/100)),2,",",".").'</b></td></tr>';
                                      echo '<tr><td align="right" colspan="3"><b>Total a Pagar Bs. </b></td><td align="right" colspan="2"  style=" font-size: 1.3em; color:#c174ad;?>;" ><b>'.$final_pagar.'</b></td></tr>';
                                  echo "</table>";
                            echo '</div>';
                            echo '</div>';

          }





	echo $pie;
?>