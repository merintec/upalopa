<?php 
session_start();
include_once("../comunes/conexion.php");
include_once("../comunes/variables.php");
include("../comunes/verificar_logueo.php");

$id_user=$_SESSION['id_user'];
$tabla='datos_fact';
$titulos_busqueda = 'Tipo de Identificación, N° Identificación, Razón Social, Dirección, Teléfono Local, Teléfono Movil';
$datos_busqueda = 'tiden_datos,iden_datos,razon_datos,dire_datos,telfh_datos,telfm_datos';
$nregistros = 50;
$color_fondo='#D2C8B0';
$logo='../imagenes/sistema/logo.png';
$var_where = 'id_user = '.$_SESSION['id_user']; 
?> 
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $nom_pagina; ?></title>
  </head>
<!-- funcion de guardar formulario -->  
<script type="text/javascript">
$(function()
{
    $("#guardar").click(function()
    {
      if ($("#form1").validationEngine('validate')){
        var url="../comunes/funcion_guardar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: $("#form1").serialize(),
            success: function(data)
            {
              //alert(data);
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                actualizar_dato('venta', 'id_datos', datatemp[2], 'id_venta', $('#id_venta2').val(),'');
                $("#form1")[0].reset();
              }
              $("#resultado").html(mensaje);
              setTimeout(function() {
                $("#msg_act").fadeOut(1500);
              },3000);
              ira_facturacion();
            }
        });
        return false;
      }
    });
});
function buscar()
{
  var url="../comunes/busqueda_cesta.php"; 
  $.ajax
  ({
    type: "POST",
    url: url,
    data: $("#form1").serialize(),
    success: function(data)
    {
      $("#resultado_busqueda").html(data);
    }
  });
  return false; 
}
</script>
  <body class="">
<?php 
$sql_env = "SELECT * FROM datos_fact WHERE id_user = ".$id_user;
$cant_env = mysql_num_rows(mysql_query($sql_env));
if ($cant_env > 0){ $add_clase = " display: none; "; $add_clase2 = ""; } else{ $add_clase = ""; $add_clase2 = " oculto "; }
?>
<div id="formulario" class="jumbotron cajacontacto"  style="margin-top: 0px; <?php echo $add_clase; ?>">
  <div class="ventana-titulo" style="width: 600px; margin-left: -20px; margin-top: -20px;">
    Datos de Facturación
  </div>
  <div class="borde-ventana-punteada">
      <br><br><br><br><br><br>
            <form method="POST" name="form1" id="form1" onsubmit="return jQuery(this).validationEngine('validate');">
                <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
                <input type="hidden" name="var_titulos_busqueda" id="var_titulos_busqueda" value='<?php echo $titulos_busqueda; ?>'>
                <input type="hidden" name="var_datos_busqueda" id="var_datos_busqueda" value='<?php echo $datos_busqueda; ?>'>
                <input type="hidden" name="var_nregistros" id="var_nregistros" value='<?php echo $nregistros; ?>'>
                <input type="hidden" name="var_pag_actual" id="var_pag_actual" value=''>
                <input type="hidden" name="var_where" id="var_where" value='<?php echo $var_where; ?>'>

                <div class="row">
                  <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="tiden_datos" class="etq_form" > Identificaci&oacute;n:</label> </div>
                  <div class="col-md-3">                       
                    <select name="tiden_datos" id="tiden_datos"   class="validate[required], form-control fondo_campo">';
                      <option   value="V" >V</option>
                      <option   value="E" >E</option>
                      <option   value="J" >J</option>
                      <option   value="G" >G</option>
                    </select>
                  </div>
                  <div class="col-md-5">  
                    <div id="grupo_iden_datos" class="">
                      <input type="text" name="iden_datos" id="iden_datos" class="validate[required, custom[integer], minSize[6], maxSize[20]] text-input form-control fondo_campo" placeholder="N&uacute;mero Identificación">
                      <span id="boton_iden_datos" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                        <span id="actualiza_iden_datos" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                      </span>                       
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-4" style="padding-right: 0px;">  <label for="razon_datos" class="etq_form pull-right" >Razón Social: </label> </div>
                  <div class="col-md-8 col-xs-8">
                    <div id="grupo_razon_datos" class="">                     
                      <input type="text" name="razon_datos" id="razon_datos" class="validate[required, minSize[3], maxSize[250]] text-input form-control fondo_campo" placeholder="Indique Razón Social">
                      <span id="boton_razon_datos" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="";>
                        <span id="actualiza_razon_datos" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                      </span>      
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4 col-xs-4" style="padding-right: 0px;">  <label for="direccion" class="etq_form pull-right" >Dirección: </label> </div>
                  <div class="col-md-8 col-xs-8">
                    <div id="grupo_dire_datos" class="">                     
                      <input type="text" name="dire_datos" id="dire_datos" class="validate[required, minSize[3], maxSize[250]] text-input form-control fondo_campo" placeholder="Dirección de Facturación">
                      <span id="boton_dire_datos" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                        <span id="actualiza_dire_datos" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                      </span>      
                    </div>
                  </div>
                </div>


                   <div class="row">
                    <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="tele_user" class="etq_form" >Tel&eacute;fono Fijo:</label> </div>
                      <div class="col-md-8 col-xs-8">
                          <div id="grupo_telfh_datos" class="">
                                <input type="text" name="telfh_datos" id="telfh_datos" class="validate[required, custom[phone], minSize[6], maxSize[11]] text-input form-control fondo_campo" placeholder="N&uacute;mero Telefónico Fijo">
                                  <span id="boton_telfh_datos" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                  <span id="actualiza_telfh_datos" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                  </span>       
                          </div>
                      </div>
                   </div>


                   <div class="row">
                    <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="tele_user" class="etq_form" >Tel&eacute;fono Movil:</label> </div>
                      <div class="col-md-8 col-xs-8">
                        <div id="grupo_telfm_datos" class="">
                              <input type="text" name="telfm_datos" id="telfm_datos" class="validate[required, custom[phone], minSize[6], maxSize[11]] text-input form-control fondo_campo" placeholder="N&uacute;mero Telefónico Movil">
                                <span id="boton_telfm_datos" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                <span id="actualiza_telfm_datos" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                </span>       
                        </div>
                      </div>
                  </div>

      <input type="hidden" name="id_user" id="id_user" value='<?php echo $id_user; ?>'>


                <br><div align="center"><button id="cancelar" type="button" class="btn btn_form fondo_boton oculto" onclick="ira_facturacion()"><div class="vineta-blanco">&nbsp;</div>Cancelar</button>  <button type="button" name="guardar"  id="guardar" class="btn btn-sm fondo_boton" ><div class="vineta-blanco">&nbsp;</div>Guardar </button></div>
           
            </form>
          
           
            </div>
</div>
<div class="row">
  <div class="container <?php echo $add_clase2; ?>">
    <div class="container text-center text-info" style="font-size: 1.2em; " >
      <span><img src="../imagenes/acciones/info.png"></span> Seleccione un dato de facturación de la lista que se muestra a continuación o 
          <button onclick="muestra_oculta('formulario')" title="Agrega nuevos datos de facturación " id="btn_siguiente" type="button" class="btn fondo_boton"> Agrega nuevos datos <span class="vineta-add-datos"></span></button>
    </div>
  </div>
  <br>
  <div id='resultado_busqueda' class="container">
    <?php 
      $seleccion = "id_datos:::venta:::id_datos:::id_venta:::$('#id_venta2'):::ira_facturacion()";
      include("../comunes/busqueda_cesta.php");
    ?>
  </div>
</div>
  </body>
</html>

