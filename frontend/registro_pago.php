<?php
session_start();
include_once("../comunes/variables.php");
include_once("../comunes/conexion.php");
$tabla='pago';
?>
    <script>
    $(document).ready(function() {
        // bootstrap-datepicker
        $('.datepicker').datepicker({
          format: "dd-mm-yyyy",
          language: "es",
          autoclose: true
        }).on(
          'show', function() {      
          // Obtener valores actuales z-index de cada elemento
          var zIndexModal = $('#modal_pago').css('z-index');
          var zIndexFecha = $('.datepicker').css('z-index');
                // alert(zIndexModal + zIndexFEcha);
         
                // Re asignamos el valor z-index para mostrar sobre la ventana modal
                $('.datepicker').css('z-index',zIndexModal+1);
        });
        $('.datepicker').datepicker("setDate", new Date(<?php echo date('Y');?>,<?php echo date('m');?>,<?php echo date('d');?>) );
    });
    </script>
<?php 
  $sql_mpago = "SELECT * FROM metodo_pago WHERE nomb_mpago LIKE '%Cupón%'";
  $mpago = mysql_fetch_array(mysql_query($sql_mpago));
?>
<input type="hidden" name="es_cupon"  id="es_cupon" value="<?php echo $mpago[id_mpago]; ?>" >
<script>
  function verifica_monto(){
    if ($('#mont_pago').val()>0){ 
      $('#ingresar_data').show();
      $('#cerrar_win').hide();
    }
    else{
      $('#ingresar_data').hide();
      $('#cerrar_win').show(); 
    }
  }
  function registrar_pago()
  {
      if ($("#form10").validationEngine('validate')){
              var url="../comunes/funcion_guardar.php"; 
              $.ajax
              ({
                  type: "POST",
                  url: url,
                  data: $("#form10").serialize(),
                  success: function(data)
                  {
                    var codigo, datatemp, mensaje;
                    datatemp=data;
                    datatemp=datatemp.split(":::");
                    codigo=datatemp[0];
                    mensaje=datatemp[1];
                    if (codigo==001)
                    {
                            var parametros2 = {
                              "id_venta": $("#id_venta").val()
                            };
                            var url2="consultar_pagos.php"; 
                            $.ajax
                            ({
                              type: "POST",
                                url: url2,
                                data: parametros2,
                                success: function(data2)
                                {
                                  //alert(data2);
                                  if (data2 <= 0){
                                    actualizar_dato('venta','status_venta','pagado','id_venta',$("#id_venta").val());
                                    fecha_act = '<?php echo date('Y-m-d'); ?>';
                                    actualizar_dato('venta','fech_stat', fecha_act,'id_venta',$("#id_venta").val());
                                  }
                                }
                            });
                      if ($('#id_mpago').val() == $('#es_cupon').val()){
                        actualizar_dato('cupones_descuentos','stat_cupon','utilizado','num_cupon','"' + $("#num_refe_banc").val() + '"');
                        actualizar_dato('pago','status_pago','Aprobado','num_refe_banc','"' + $("#num_refe_banc").val() + '"');
                      }
                      enviar_correo();
                      $("#form10")[0].reset();
                      $("#modal_pago").modal('hide');
                      setTimeout(function() {
                          window.location=('pagos.php');
                      },1500);

                    }
                    $("#resultado").html(mensaje);
                    setTimeout(function() {
                      $("#msg_act").fadeOut(1500);
                    },3000);
                     
                  }
              });
              return false;
            }

  }
function enviar_correo()
{
          var correo=$("#var_correo").val();
          var nom_ape_user=$('#var_nom_ape').val();
          var orden=$('#var_orden').val();
          var monto=$('#mont_pago').val();
          var url="../comunes/enviar_correo.php";
          var sex;
          if ($('#sex_user').val()=='F'){
            sex = 'a';
          }
          else {
            sex = 'o';  
          }  
          var mensaje = "Estimad" + sex + " " + nom_ape_user + ", <p style='padding-top: 20px;'>Hemos registrado un pago para<br>la orden de compra N° <b>" + orden + "</b> por Bs. <b>" + monto + "</b>  en las próximas 48 horas será verificado y se le notificará para el envío del producto. </p><p style='padding-top: 20px;'>Gracias por preferirnos <a href='http://upalopa.com'>Upalopa.com</a></p>";
          //var mensaje = "Estimad" + sex + " " + nom_ape_user + ", <p style='padding-top: 20px;'>!Su pago ha sido registrado exitosamente para la orden de compra N° " + orden + "! <p style='padding-top: 20px;'>Para mayor detalle consulte la sección <b>Tus Pagos</b> en nuestro sitio <a href='http://upalopa.com'>www.upalopa.com</a></p>",
          var parametros = {
              "mensaje" : mensaje,
              "destino" : correo, 
              "correo_origen" : "<?php echo $correo_compras; ?>",
              "titulo" : "Pago Registrado Upalopa"
            };
          $.ajax
          ({
              type: "POST",
              url: url,
              data: parametros,
              success: function(data)
              {

              }
          });
}
  

  function actualizar_dato(tabla,campo,valor,campo_id,valor_id)
{
    //para verificar si es una fecha voltearla al momento de guardarla
      var nueva;
      nueva = valor.split("-");
      if (valor.length == 10 && nueva[0].length == 2 && nueva[1].length == 2 && nueva[2].length == 4){
        valor = nueva[2]+'-'+nueva[1]+'-'+nueva[0];
      }
      var parametros = {
        "var_tabla": tabla,
        "var_campo" : campo,
        "var_valor" : valor,
        "var_id" : campo_id,
        "var_id_val" : valor_id
      };
      var url="../comunes/funcion_actualizarcampo.php"; 
      $.ajax
      ({
        type: "POST",
          url: url,
          data: parametros,
          success: function(data)
          {
              //alert(data);
          }
      });
      return false; 
}
</script>
<!DOCTYPE html>

<div class="ventana-titulo">
  Registro de Pago <br> N° de Orden <span id="codg_trans"> </span>
</div>
<div class="borde-ventana-punteada">
<button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -4.7em; margin-right: -5px;">×</button>
<html lang="es">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">   
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <!-- validacion en vivo -->
<script >
  jQuery(document).ready(function(){
    verifica_monto();
    // binds form submission and fields to the validation engine
    jQuery("#form10").validationEngine('attach', {bindMethod:"live"});
   });
</script>

</head>
<body>
  <div class="" style="padding-top: 0.3em;  margin:0px; margin-left:auto; margin-right: auto;">
     <br><br><br><br><br>
      <form method="POST" name="form10" id="form10" onsubmit="return jQuery(this).validationEngine('validate');">
        <?php if ($_POST[giftcards]=='NO'){ ?>
        <div class="row">
          <div class="col-md-5 col-xs-5 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="id_mpago" >Metodo de Pago</label> 
          </div>
          <div class="col-md-7 col-xs-7">
            <select name="id_mpago" id="id_mpago"  class="validate[required] form-control fondo_campo">
              <option value="" selected disabled style="display:none;">Seleccione...</option>
                <?php 
                  //consulta  
                  $consulta_mpago = mysql_query("SELECT * FROM metodo_pago WHERE nomb_mpago NOT LIKE '%mercadopago%' AND  nomb_mpago NOT LIKE '%GIFTCARDS%' AND  nomb_mpago NOT LIKE '%Cupón%' order by nomb_mpago ");
                  while($fila=mysql_fetch_array($consulta_mpago))
                  {
                    echo "<option  value=".$fila[id_mpago].">".$fila[nomb_mpago]."</option>";
                  }
                ?>
            </select>
          </div>
        </div>
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-5 col-xs-5 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="id_cuen_banc" > Banco Destino</label>
          </div>
          <div class="col-md-7 col-xs-7 text-right">  
           <select name="id_cuen_banc" id="id_cuen_banc"  class="validate[required] form-control fondo_campo">
                          <option value="" selected disabled style="display:none;">Seleccione...</option>
                            <?php 
                            //consulta  
                            $consulta_banco = mysql_query("SELECT * FROM cuentas_bancarias order by nomb_banc ");
                            while($fila=mysql_fetch_array($consulta_banco))
                            {
                               echo "<option  value=".$fila[id_cuen_banc].">".$fila[nomb_banc]."</option>";
                            }
                            ?>

            </select>
          </div>
        </div>
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-5 col-xs-5 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="mont_pago" > Monto</label>
          </div>
          <div class="col-md-7 col-xs-7 text-right">
            <input type="text" name="mont_pago" id="mont_pago" class="validate[required, custom[number], minSize[3], maxSize[60]] text-input form-control fondo_campo" placeholder="P.Ejem 15800.00" onchange="verifica_monto();" onkeyup="verifica_monto();">
          </div> 
        </div>
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-5 col-xs-5 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="iden_per_pago" >Cédula</label>
          </div>
          <div class="col-md-7 col-xs-7 text-right">
            <input type="text" name="iden_per_pago" id="iden_per_pago" class="validate[required, minSize[3], maxSize[30]] text-input form-control fondo_campo" placeholder="P.Ejem 12654789">
          </div>        
        </div>

        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-5 col-xs-5 text-right" style="padding-right: 0px; padding-top: 0.5em;">
            <label for="nomb_per_pago" > Nombre y Apellido </label>
          </div>
          <div class="col-md-7 col-xs-7 text-right">
             <input type="text" name="nomb_per_pago" id="nomb_per_pago"  class="validate[required, minSize[3], maxSize[30]] text-input form-control fondo_campo" value="<?php echo $_SESSION['usuario_logueado'];  ?> "placeholder="Persona quien realiza la Transacción">
          </div>           
        </div>
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-5 col-xs-5 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="banc_origen" > Banco de Origen </label>
          </div>
          <div class="col-md-7 col-xs-7 text-right">         
             <input type="text" name="banc_origen" id="banc_origen" class="validate[minSize[3], maxSize[60]] text-input form-control fondo_campo" placeholder="Banco de Origen">
          </div>        
        </div>
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-5 col-xs-5 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="nomb_per_pago" > N° de Referencia </label>
          </div>
          <div class="col-md-7 col-xs-7 text-right">  
             <input type="text" name="num_refe_banc" id="num_refe_banc" class="validate[required, minSize[3], maxSize[30]] text-input form-control fondo_campo" placeholder="Numero de Deposito o Transferencia">
          </div>
        </div>
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-5 col-xs-5 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="fech_pago"> Fecha del Pago</label>
          </div>
          <div class="col-md-7 col-xs-7 text-right">
                            <div class="input-group">
                                  <input type="text" name="fech_pago" id="fech_pago" class="validate[required, custom[date]] text-input es form-control input-append date datepicker fondo_campo" placeholder="Seleccionar...">
                                  <span id="boton_fechi_desc" class="input-group-addon"  style="visibility:visible; cursor:pointer; cursor: hand;">
                                      <span class="glyphicon glyphicon-calendar" onclick="$('#fech_pago').focus();"></span>
                                  </span>
                            </div>
          </div>
        </div>
      <?php } 
        if ($_POST[giftcards]=='CUPON'){
          ?>
          <script type="text/javascript">
            function seleccionar(id,cupon,valor){
              if(valor && cupon){
                $('.seleccion_off').show();
                $('.seleccion_on').hide();
                $('#sel' + id +'_on').show();
                $('#sel' + id +'_off').hide();
                $('#num_refe_banc').val(cupon);
                $('#mont_pago').val(valor);
              }
              else{
                $('.seleccion_on').hide();
                $('.seleccion_off').show();
                $('#num_refe_banc').val('');
                $('#mont_pago').val('');
              }
              verifica_monto();

            }
          </script>
          <?php
          echo '<div style="margin-bottom: 1em; text-align: center; font-weight: bold;">Selecciona el Cupón que desar utilizar</div>';
          echo '<table class="table table-striped table-hover">';
            echo '<tr style="font-size: 0.8em">
              <th># Cupón</th><th>Motivo</th><th>Monto</th><th>Usar</th>
            </tr>';
          $sql_cupon = "SELECT * FROM cupones_descuentos WHERE id_user =".$_SESSION['id_user']." AND stat_cupon = 'enviado'";
          $bus_cupon = mysql_query($sql_cupon);
          $cont = 0;
          while($res_cupon = mysql_fetch_array($bus_cupon)){
            $cont++;
            echo '<tr style="font-size: 0.7em">
              <td>'.$res_cupon[num_cupon].'</td>
              <td>'.$res_cupon[moti_cupon].'</td>
              <td>'.number_format($res_cupon[valor_cupon],2,',','.').'</td>
              <td>
                <img class="seleccion_off" id="sel'.$cont.'_off" style="height: 20px; cursor: pointer;" src="../imagenes/acciones/default_off.png" onclick="seleccionar('.$cont.',\''.$res_cupon[num_cupon].'\',\''.$res_cupon[valor_cupon].'\');">
                <img class="seleccion_on" id="sel'.$cont.'_on" style="height: 20px; cursor: pointer; display: none;" src="../imagenes/acciones/default_on.png" onclick="seleccionar('.$cont.',\'\',\'\');"></td></tr>';
          }
          if ($cont==0){
            echo '<tr class="text-center"><td colspan="4">No tienes Cupones disponibles</td></tr>';
          }
          echo '</table>';
        ?>
          <input type="hidden" name="num_refe_banc" id="num_refe_banc" class="validate[required, custom[integer] minSize[10], maxSize[10]] text-input form-control fondo_campo datos" placeholder="Número de Cupón">
          <input type="hidden" name="nomb_per_pago" id="nomb_per_pago"  class="validate[required, custom[email]] text-input form-control fondo_campo datos"  placeholder="e-mail donde la recibiste" value="<?php echo $_SESSION[usuario_logueado]; ?>">
          <input type="hidden" name="mont_pago" id="mont_pago" class="validate[required, custom[number], minSize[3], maxSize[60], min[0.1]] text-input form-control fondo_campo" placeholder="P.Ejem 15800.00"  onchange="verifica_monto();">
          <input type="hidden" name="fech_pago"  id="fech_pago" value="<?php echo date('d-m-Y'); ?>" >
          <?php 
            $sql_mpago = "SELECT * FROM metodo_pago WHERE nomb_mpago LIKE '%Cupón%'";
            $mpago = mysql_fetch_array(mysql_query($sql_mpago));
          ?>
          <input type="hidden" name="id_mpago"  id="id_mpago" value="<?php echo $mpago[id_mpago]; ?>" >
        <?php
        }
        if ($_POST[giftcards]=='SI'){ 
        ?>
        <script language="javascript" type="text/javascript">  
            $(document).ready(function() {
              $('.datos').keyup(function(e){
                var num = $('#num_refe_banc').val();
                var mail = $('#nomb_per_pago').val();
                if (num && mail){
                    buscar_giftcard();
                }
              });
              $('#mont_pago').keyup(function(e){
                verifica_monto();
                var mont = Number($('#mont_pago').val());
                var disp = Number($('#var_disp').val());
                if (disp == null ){ disp = 0; }
                if (disp<mont){
                    $('#msg_monto').html('El monto ' + mont +' supera el disponible');
                    $('#mont_pago').val('');
                }
                else{
                  $('#msg_monto').html('');
                }
              });
            });
        </script>
        <script>
          function buscar_giftcard()
          {
            var parametros = {
              "numb_gif" : $('#num_refe_banc').val(),
              "mail_gif" : $('#nomb_per_pago').val()
            }
            var url="buscar_giftcard.php"; 
            $.ajax
            ({
                type: "POST",
                url: url,
                data: parametros,
                beforeSend: function(){
                },
                success: function(data)
                {
                  $('#resultado_giftcard').html(data);
                }
            });
            return false;
          }
        </script>
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-5 col-xs-5 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="num_refe_banc" > N° de GiftCard </label>
          </div>
          <div class="col-md-7 col-xs-7 text-right">  
             <input type="text" name="num_refe_banc" id="num_refe_banc" class="validate[required, custom[integer] minSize[10], maxSize[10]] text-input form-control fondo_campo datos" placeholder="Número de GiftCard">
          </div>
        </div>
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-5 col-xs-5 text-right" style="padding-right: 0px; padding-top: 0.5em;">
            <label for="nomb_per_pago" > e-mail </label>
          </div>
          <div class="col-md-7 col-xs-7 text-right">
             <input type="text" name="nomb_per_pago" id="nomb_per_pago"  class="validate[required, custom[email]] text-input form-control fondo_campo datos"  placeholder="e-mail donde la recibiste">
          </div>           
        </div>
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-5 col-xs-5 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="mont_pago" > Monto a utilizar</label>
          </div>
          <div class="col-md-7 col-xs-7 text-right">
            <input type="text" name="mont_pago" id="mont_pago" class="validate[required, custom[number], minSize[3], maxSize[60], min[0.1]] text-input form-control fondo_campo" placeholder="P.Ejem 15800.00"  onchange="verifica_monto();">
            <div id="msg_monto" style="color: #FF0000; font-size: 10px;"></div>
          </div> 
        </div>
          <input type="hidden" name="fech_pago"  id="fech_pago" value="<?php echo date('d-m-Y'); ?>" >
          <?php 
            $sql_mpago = "SELECT * FROM metodo_pago WHERE nomb_mpago LIKE '%giftcards'";
            $mpago = mysql_fetch_array(mysql_query($sql_mpago));
          ?>
          <input type="hidden" name="id_mpago"  id="id_mpago" value="<?php echo $mpago[id_mpago]; ?>" >
        <?php }?>
          <input type="hidden" name="var_disp" id="var_disp">
          <input type="hidden" name="id_venta"  id="id_venta"  >
          <input type="hidden" name="status_pago"  id="status_pago" value="registrado"  >
          <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
                  <?php 
        if (!$id_user){ $id_user = $_SESSION[id_user]; }
        $consulta_usuario="SELECT * FROM usuarios where id_user='$id_user'";
        $con_usu=mysql_fetch_assoc(mysql_query($consulta_usuario));

        ?>
          <input type="hidden" name="var_correo" id="var_correo" value='<?php echo $con_usu[corre_user]; ?>'>
          <input type="hidden" name="var_nom_ape" id="var_nom_ape" value='<?php echo $con_usu[nom_ape_user]; ?>'>
          <input type="hidden" name="var_orden" id="var_orden" >
          <br>
          <div id="resultado_giftcard" style="padding-bottom: 15px; text-align: center; font-weight: bold; color: #c8bfa9;"></div>
        <center><button id="ingresar_data" type="button" class="btn fondo_boton" onclick="registrar_pago()"><div class="vineta-blanco">&nbsp;</div> Guardar </button>
        <button id="cerrar_win" type="button" class="btn fondo_boton" onclick="$('#modal_pago').modal('hide');"><div class="vineta-blanco">&nbsp;</div>  Cerrar </button></center> 
      
      </form>
  </div>
</body>
</html>
</div>