<?php 
session_start();
include("../comunes/conexion.php");
include("../comunes/verificar_logueo.php");
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
$id_user=$_SESSION['id_user'];
$categoria=$_GET['categoria'];
$categoria = '-7';
$logo='../imagenes/sistema/logo.png';
$con[nomb_cate] = 'Cesta de Compras';
$con[desc_cate] = '<b>Tu Tienda UPALOPA<b>';
$color_fondo='#D2C8B0';
include("../comunes/variables.php");
include("../comunes/verificar_usuario_login.php");
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <script src="../js/inactividad.js"></script>

    <script>
    
    function pasar_modal(id,orden,nomb_prod)
         {
          
            $("#id_venta_prod").val(id);
            $("#codg_trans").html(orden);
            $("#nomb_prod").html(nomb_prod);
            

         } 
   
    </script>
    <title><?php echo $nom_pagina; ?></title>
        
  </head>
  <body class="">    
    <div style="background-color:<?php echo $color_fondo; ?>;" class="cabecera_categorias">
        <a title="ir a página inicial" href="../index.php"><img id="logo_top" class="logo_top" src="<?php echo $logo; ?>" ></a>
    </div>
    <div class="container-fluid">
      <div class="row">
        <?php include("menu_cesta.php"); ?>
      </div>
      <br>
      <div data-offset-top="280" class="container" data-spy="affix">
        <div id="resultado"></div>
      </div>
      <?php
         $consulta1="SELECT * FROM venta as v  where v.id_user='$id_user' and (v.status_venta!='procesado' AND v.status_venta!='carrito' ) ";
         $con1= mysql_fetch_assoc(mysql_query($consulta1));

         if ($con1[id_venta]==NULL)
         {
              ?>

                    <script type="text/javascript">  

                          alert("No tiene Historial de Compras");
                          window.location=("catalogo.php"); 
                         
                    </script>

              <?php 


         }



      ?>
      <br>
      <div class="text-center"> <span class="text-info"><h3> <b> Histórico de Compras </b>  </h3></span> </div>
    <div class="container" >

        <?php    

        $consulta="SELECT * FROM venta as v  where v.id_user='$id_user' and (v.status_venta!='procesado' or v.status_venta!='carrito') order by v.id_venta ";
        $consulta=mysql_query($consulta);

        $i=0;
        while($fila=mysql_fetch_array($consulta))
        {
          $icono = '';
          $totalp=0;
          $total_pagar=0;
          $total_cant=0;
          $i++;

          if ($fila[status_venta]=='pagado')
          {

            $icono= '<button style="width:13em; background-color: #00b6ce; border: 0px;" type="button" class="btn btn-warning" title="En espera de confirmación de pago" > <span class="pull-left"> Por Confirmar Pago</span> <span class="vineta-pago" aria-hidden="true"></span></button> ';
            
          }
           if ($fila[status_venta]=='confirmado')
          {

            $icono= '<button style="width:13em; background-color: #ae4f9e; border: 0px;" type="button" class="btn btn-info" title="Pago Confirmador. En espera de envío" ><span class="pull-left"> Por Enviar</span> <span class="vineta-enviar" aria-hidden="true"></span></button> ';
          }
          if ($fila[status_venta]=='enviado')
          {
            $icono= '<button style="width:13em; background-color: #ef4a7c; border: 1px;" type="button" class="btn btn-primary" title="Enviado" > <span class="pull-left">Enviado</span> <span class="vineta-enviado" aria-hidden="true"></span></button> ';
            $consulta_envio="SELECT * FROM envios, empresa_envio where envios.id_venta=$fila[id_venta] and empresa_envio.id_empr_envi=envios.id_empr_envi";
            $con_envio= mysql_fetch_assoc(mysql_query($consulta_envio));
          }
           if ($fila[status_venta]=='entregado')
          {

            $icono= '<button style="width:13em; background-color: #3e658b; border: 1px;" type="button" class="btn btn-success" title="Entregado al cliente" > <span class="pull-left">Entregado</span> <span class="vineta-compras" aria-hidden="true"></span></button> ';
          }
                   $consulta2="SELECT * FROM venta_productos as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
                   vp.id_venta='$fila[id_venta]' and vp.status_vent='procesado' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
                  $consulta2=mysql_query($consulta2);
                  while ($fila2=mysql_fetch_array($consulta2)) 
                  {
                     $totalp=$fila2[vuni_venta_prod]*$fila2[cant_venta_prod];
                     $total_pagar += $totalp;
                     $total_cant += $fila2[cant_venta_prod];
                     $peso_cu=$fila2[peso_prod] * $fila2[cant_venta_prod];
                     $total_peso += $peso_cu;

                      $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
                      $consulta_envio=mysql_query($consulta_envio);
                      $con_env=mysql_fetch_assoc($consulta_envio);

                      if (!$con_env[id_tenvios])
                      {

                            $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                            $consulta_envio=mysql_query($consulta_envio);
                            $con_env=mysql_fetch_assoc($consulta_envio);

                            $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);

                      }


                    }

                  
                    echo '<div class="table-responsive procesar_linea_punteada">';
                    echo '<table class="table table-striped table-hover">
                    <tr><th class="fondo_predefinido" width="100px"># Orden </th> <th class="fondo_predefinido">Cantidad de Productos </th> <th class="fondo_predefinido"> Monto a Cancelar </th>';    if ($fila[status_venta]=='enviado') { echo '<th class="fondo_predefinido"> Empresa de Envio </th> <th class="fondo_predefinido"> N° de Guia </th> <th class="fondo_predefinido"> Fecha de Envio </th>'; }  echo '<th class="fondo_predefinido text-center" style="width: 240px;"> Status </th> </tr>';
       
                  echo '<tr><td>'.$fila[codg_trans].' </td> <td>'.$total_cant.'</td> <td>'.number_format(($total_pagar+$con_env[prec_tenvio]+($total_pagar * ($con_env[porc_tenvio]/100))),2,",",".").'</td>';  if ($fila[status_venta]=='enviado') {echo '<td>'.$con_envio[nomb_empr_envi].'</td><td>'.$con_envio[guia_envio].'</td> <td>'.$con_envio[fech_envio].'</td>';  } echo '<td class="text-right"> '.$icono.'</button> <img height="30px" id="ver" style="cursor:pointer;"  src="../imagenes/acciones/lupa.png" title="Mostrar detalle de la compra" onclick="muestra_oculta(\'desplegar'.$i.'\')"></td> </tr>';
                  echo '</table>';
                
                  
                        echo '<div class="table-responsive" >';

                        echo '<table   class="table table-striped table-hover"  id="desplegar'.$i.'" style="display: none;">'; 
                        echo '<tr> <th>Nº </th> <th>Producto</th> <th>Cantidad </th> <th> Precio Unitario </th> <th> Total </th> <th> Devolución </th> </tr>';

                        //consulta para mostrar detalle de articulos
                        $consulta3="SELECT * FROM venta_productos as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
                         vp.id_venta='$fila[id_venta]' and vp.status_vent='procesado' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
                        $consulta3=mysql_query($consulta3);
                        $j=0;
                          

                        while ($fila3=mysql_fetch_array($consulta3)) 
                        {
                            $totalp=$fila[vuni_venta_prod]*$fila[cant_venta_prod];
                            $imp_cu = ($totalp / (($val_imp/100)+1));
                            $imp_cu = $imp_cu * $val_imp / 100;
                            $imp_cu = number_format($imp_cu,2,'.','');
                            //// para los totales generales
                            $peso_cu=$fila[peso_prod] * $fila[cant_venta_prod];
                            $total_pagar += $totalp;
                            $total_peso += $peso_cu;
                            $total_imp += $imp_cu;
                            $total_cant += $fila[cant_venta_prod];
                            $subtotal=$fila3[vuni_venta_prod]*$fila3[cant_venta_prod];

                            $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
                            $consulta_envio=mysql_query($consulta_envio);
                            $con_env=mysql_fetch_assoc($consulta_envio);;

                            if (!$con_env[id_tenvios])
                            {

                                  $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                                  $consulta_envio=mysql_query($consulta_envio);
                                  $con_env=mysql_fetch_assoc($consulta_envio);

                                  $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);

                            }
                            $j++;
                                    echo '<tr> <td>'.$j.'</td><td>'.$fila3[nomb_prod].' </td> <td align="center">'.$fila3[cant_venta_prod].'</td> <td align="right">'.number_format($fila3[vuni_venta_prod],2,",",".").' </td><td align="right">'.number_format($subtotal,2,",",".").' </td>
                                    <td class="text-center">';

                                     $consulta_devolucion="SELECT * FROM devoluciones where id_venta_prod='$fila3[id_venta_prod]'";
                                     $con_dev=mysql_fetch_assoc(mysql_query($consulta_devolucion));
                                   
                                    if ($fila[status_venta]=='entregado' and $con_dev[id_devo]==NULL)
                                    {
                                      echo ' <button class="btn btn-success btn-xs" title="Gestionar Devolución" data-toggle="modal" data-target="#devolucion" onclick="pasar_modal('.$fila3[id_venta_prod].',\''.$fila[codg_trans].'\',\''.$fila3[nomb_prod].'\')" id="eliminar"> <span class="glyphicon glyphicon-send"  aria-hidden="true"> </button>';
                                    }
                                    echo '</td> </tr>';

                           
                            
                        }
                            echo '<tr><td align="right" colspan="3"><b>Subtotal: Bs.</b> </td><td align="right" colspan="2"><b>'.number_format($total_pagar,2,",",".").'</b></td></tr>';
                            echo '<tr><td align="right" colspan="3"><b>Costo de Envio: Bs. </b></td><td align="right" colspan="2"><b>'.number_format($con_env[prec_tenvio],2,",",".").'</b></td></tr>';
                            echo '<tr><td align="right" colspan="3"><b>Costo de Seguro: Bs.</b> </td><td align="right" colspan="2"><b>'.number_format(($total_pagar * ($con_env[porc_tenvio]/100)),2,",",".").'</b></td></tr>';
                            echo '<tr><td align="right" colspan="3"><b>Total a Pagar Bs. </b></td><td align="right" colspan="2"  style=" font-size: 1.3em; color:#c174ad;?>;" ><b>'.number_format(($total_pagar+$con_env[prec_tenvio]+($total_pagar * ($con_env[porc_tenvio]/100))),2,",",".").'</b></td></tr>';
                        echo "</table>"; 
                        echo "</div>";
                  echo "</div>";

        }
 

       ?>
       </div>
      <br>


      <div class="row hidden-xs" role="navigation">
        <?php
           include("menu_footer.php");
           include("footer.php");
        ?>
      </div>
      <div class="row visible-xs">
        <?php  include("footer.php"); ?>
      </div>
    </div>
    <!-- Modal para login -->
    <div class="modal fade" id="login" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 350px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal">
              <?php 
                include('login.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div> 
      </div>    
    <!-- Modal para perfil -->
    <div class="modal fade" id="perfil" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal_perfil">
              <?php 
                include('perfil.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div>



        <!-- Modal para devoluacion -->
    <div class="modal fade" id="devolucion" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal">
              <?php 
                include('devolucion.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div> 
    </div>


      <!-- Modal para Registrar Pago -->
    <div class="modal fade" id="modal_pago" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 350px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_pago">
           
              <?php 
                include('registro_pago.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div> 
    <script src="../bootstrap/js/bootstrap.min.js"> </script>  
  </body>
</html>