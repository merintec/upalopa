<div class="row hidden-xs" style="margin-left: 3em; margin-bottom: 2em;">
	<div class="col-md-2 col-xs-1">
		<a title="ir a página inicial" href="../index.php"><img class="logo_top" src="../imagenes/sistema/logo_tn_on.png" width="80em" height="80em" ></a>		
	</div>
	<div class="col-md-8 col-xs-10" style="padding-top: 4em;">
		<div class="titulo_categoria text-info" style="margin-left: 0.5em;">Preguntas Frecuentes</div>
	</div>
</div>
<div class="row visible-xs" style="margin-bottom: 1em; ">
	<div class="col-xs-3">
		<a title="ir a página inicial" href="../index.php"><img class="logo_top" src="../imagenes/sistema/logo_tn_on.png" style="width: 100%"></a>		
	</div>
	<div class="col-xs-8">
		<div class="titulo_categoria text-info" style="margin-left: 0.2em;">Preguntas Frecuentes</div>
	</div>
</div>
<div class="row" style="margin-bottom: 2em; text-align: justify;">
	<div class="col-md-1 hidden-xs"></div>
	<div class="col-md-9 col-xs-12 texto-contenido">
		<div class="opcion-faq" onclick="muestra_oculta('contenido_con'); muestra_oculta('icon_con_b'); muestra_oculta('icon_con_r');">
			<span id="icon_con_b" class="glyphicon glyphicon-triangle-bottom pull-left" style="display: none;">&nbsp;</span>
			<span id="icon_con_r" class="glyphicon glyphicon-triangle-right pull-left" style="display: ''">&nbsp;</span> Contacto		
		</div>
		<div id="contenido_con" style="display: none; width: 80%; margin: auto auto;">
			<div class="pregunta">¿A dónde puedo escribir un correo electrónico para cualquier consulta referente a mi orden?</div>
			<div class="respuesta">Puede escribirnos a la dirección electrónica: <a href="mailto:comprasenlinea@upalopa.com">comprasenlinea@upalopa.com</a></div>
			<div class="pregunta">¿A dónde puedo llamar para cualquier consulta referente a mi orden?</div>
			<div class="respuesta">Puede comunicarse al siguiente teléfono: <b>0414 7588847</b> (Lunes a Viernes en horario de Oficina) 8:00 am a 12:00 m y de 2:00 pm a 5:30 pm.</div>
		</div>
		
		<div class="opcion-faq" onclick="muestra_oculta('contenido_com'); muestra_oculta('icon_com_b'); muestra_oculta('icon_com_r');">
			<span id="icon_com_b" class="glyphicon glyphicon-triangle-bottom pull-left" style="display: none;">&nbsp;</span>
			<span id="icon_com_r" class="glyphicon glyphicon-triangle-right pull-left" style="display: ''">&nbsp;</span> Compras		
		</div>
		<div id="contenido_com" style="display: none; width: 80%; margin: auto auto; text-align: justify; ">
			<div class="pregunta">¿Cómo comprar en Upalopa.com?</div>
			<div class="respuesta" style="text-align: justify;">
				En Upalopa.com es fácil y seguro gestionar tu compra, a continuación los pasos a seguir para efectuar tu compra:
				 <ol>
					 <li><span> Regístrate en nuestra página web haciendo click en el menú superior en la opción registrarse e inicia sesión.</span></li>
					 <li><span> Agrega todo lo que te gusta en la talla de tu preferencia y llévalo a tu cesta de compra.</span></li>
					 <li><span> Verificar  tu cesta de compra dándole click a la palabra <b>Cesta de Compra</b> en el menú Superior.</span></li> 
					 <li><span> Seguir los pasos hasta completar el proceso en relación a: confirmar la compra, datos de envío y dirección, datos de facturación y verificación de tus datos personales.</span></li> 
					 <li><span> Selecciona <b>Procesar Compra</b>.</span></li> 
					 <li><span> Enviar los datos de tu pago Haciendo click en tu nombre en la parte superior y luego seleccionar la opción <b>Tus Pagos</b>.</span></li> 
					 <li><span> Recibir confirmación de tu compra.</span></li> 
					 <li><span> ¡Esperar que llegue tu pedido!</span></li> 
				 </ol>
			 </div> 
			<div class="pregunta">¿Tengo que abrir una cuenta para realizar una comprar Online?</div>
            <div class="respuesta" style="text-align: justify;">Si, para poder comprar en la tienda Online tienes que estar registrado, para registrarte solo necesitas una cuenta de correo electrónico.</div>
			<div class="pregunta">¿Qué hago si se me olvidó mi contraseña?</div>
			<div class="respuesta" style="text-align: justify;">En la ventana de iniciar sesión has clic en la opción  <b>¿Olvido su contraseña?</b> Y allí debes llenar lo datos para su restauración.</div>
		</div>

		<div class="opcion-faq" onclick="muestra_oculta('contenido_pag'); muestra_oculta('icon_pag_b'); muestra_oculta('icon_pag_r');">
			<span id="icon_pag_b" class="glyphicon glyphicon-triangle-bottom pull-left" style="display: none;">&nbsp;</span>
			<span id="icon_pag_r" class="glyphicon glyphicon-triangle-right pull-left" style="display: ''">&nbsp;</span> Pagos		
		</div>
		<div id="contenido_pag" style="display: none; width: 80%; margin: auto auto; ">
			<div class="pregunta">¿Cuáles son los números de cuenta para transferir la compras online?</div>
			<div class="respuesta"><b>Banco Provincial</b><br>Nombre: Upalopa de Vincenliz Mazzola<br>Cuenta Número: 0108 0341 12 0100139655<br>RIF V14.267.763-2<br>Correo: <a href="../index.php">www.upalopa.com</a></div>
			<div class="respuesta"><b>Banco Occidental de Descuento BOD</b><br>Cuenta Número: 0116 0046 81 0023314710<br>RIF V14.267.763-2<br>Correo: comprasenline@upalopa.com</div>
			<div class="pregunta">¿Qué formas de pago están disponibles en Upalopa.com? </div>
			<div class="respuesta">Por los momentos podrás pagar a través de depósitos o transferencias y siguiendo los pasos que se te indican. Recuerda que habrá que esperar a que se haga efectivo el dinero en la cuenta de Upalopa  para gestionar el envío (1 a 2 días hábiles).<br><br>¡Muy pronto también estará disponible pagos con tarjeta de crédito! Fácil, rápido y sencillo!.</div>
			<div class="pregunta">¿Cuál es el procedimiento para registrar el depósito o transferencia?</div>
			<div class="respuesta">Después de realizar el depósito o la transferencia, inicia sesión en Upalopa, ingresa a tu cuenta y en el menú superior derecho haz clic en la sección <b>"Tus Pagos"</b>. Encontrarás un formulario que debes llenar con el número de depósito o transferencia, la fecha, el monto  y el banco de origen y destino.</div>
		</div>
		<div class="opcion-faq" onclick="muestra_oculta('contenido_env'); muestra_oculta('icon_env_b'); muestra_oculta('icon_env_r');">
			<span id="icon_env_b" class="glyphicon glyphicon-triangle-bottom pull-left" style="display: none;">&nbsp;</span>
			<span id="icon_env_r" class="glyphicon glyphicon-triangle-right pull-left" style="display: ''">&nbsp;</span> Envíos		
		</div>
		<div id="contenido_env" style="display: none; width: 80%; margin: auto auto; ">
			<div class="pregunta">¿Puedo recoger mi producto en algún lugar?</div>
			<div class="respuesta">Lamentablemente no majeamos esta modalidad, ya que no contamos con tiendas físicas.</div>
			<div class="pregunta">¿Con qué compañía enviarán mi orden?</div>
			<div class="respuesta">Para entregar los productos justo en la dirección que nos indicas, bajo las mejores condiciones y en el menor tiempo posible, en Upalopa utilizamos los couriers más grandes y confiables de Venezuela como: Grupo Zoom, Domesa y DHL.</div>
			<div class="pregunta">No recibí mi pedido ¿Qué puedo hacer si no recibí mi pedido?</div>
			<div class="respuesta">En caso de que no puedas verificar el estado, o el producto haya sido devuelto, entonces podrás comunicarte con nosotros enviando un e-mail a <a href="mailto:comprasenlinea@upalopa.com" Subject="Problema con mi compra">comprasenlinea@upalopa.com</a></div>
			<div class="pregunta">¿Puedo cambiar la dirección de entrega?</div>
			<div class="respuesta">La dirección de entrega la deberás configurar al momento de realizar tu compra. Una vez realizada, no se podrá cambiar. Podrás guardar hasta tres direcciones de entrega que podrás editar o eliminar cuando sea necesario.</div>
		</div>
		<div class="opcion-faq" onclick="muestra_oculta('contenido_dev'); muestra_oculta('icon_dev_b'); muestra_oculta('icon_dev_r');">
			<span id="icon_dev_b" class="glyphicon glyphicon-triangle-bottom pull-left" style="display: none;">&nbsp;</span>
			<span id="icon_dev_r" class="glyphicon glyphicon-triangle-right pull-left" style="display: ''">&nbsp;</span> Devoluciones y Cambios
		</div>
		<div id="contenido_dev" style="display: none; width: 80%; margin: auto auto; ">
			<div class="pregunta">¿Cuánto tiempo tengo para devolver o cambiar un producto?</div>
			<div class="respuesta">A partir del momento en que el courier hace entrega de tu producto, tienes 48 horas para comenzar con el proceso de devolución.</div>
			<div class="pregunta">¿Cómo puedo cambiar algo de mi orden después que me llegó a mi casa?</div>
			<div class="respuesta">Después de recibir respuesta a la solicitud de devolución y de haber sido aprobada, se debe devolver el producto dentro de los siguientes 3 días continuos después de haber recibido respuesta. <br><br>Vale destacar que productos que se reciban después de esos 3 días continuos no serán aceptados. <br><br>Si has recibido un producto por error, por favor envíanos un correo a <a href="mailto:comprasenlinea@upalopa.com" Subject="Solicitud de Devolución">comprasenlinea@upalopa.com</a></div>
		</div>
	</div>
</div>
<br>
<br>
<br>
<br>
<br>