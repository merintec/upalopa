<?php
include_once("../comunes/variables.php");
include_once("../comunes/conexion.php");
$tabla='usuarios';
?>
<!DOCTYPE html>
<script src="../js/base64.js"> </script>
<script type="text/javascript">
    function guardando_registro()
    {
      $("#resultado").html("");
      if ($("#form_registrar").validationEngine('validate')){
        var sex;
        if ($('#sex_user').val()=='F'){
          sex = 'a';
        }
        else {
          sex = 'o';  
        } 
        var url="../comunes/funcion_guardar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: $("#form_registrar").serialize(),
            success: function(data)
            {
              var codigo, datatemp, mensaje, mensaje2, encrip;
              var form = $("#form_registrar"); 
              var correo = $("[name='corre_user']", form).val();
              var nom_ape_user = $("[name='nom_ape_user']", form).val();
              encrip=Base64.encode(correo);
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                $("#form_registrar")[0].reset();

                //envio de correo                   
                      var url="../comunes/enviar_correo.php"; 
                      var parametros = {
                          "mensaje" : "Estimad" + sex + " " + nom_ape_user + " <br> ¡Bienvenid" + sex + " a Upalopa.com!  <p style='padding-top: 20px;'>Gracias por registrarte en <a href='http://upalopa.com'>www.upalopa.com</a>, <br>Para Iniciar Sesión en nuestra página debes hacer click en el siguiente botón</p> <p style='padding-top: 20px;'><a href='http://upalopa.com/comunes/activar.php?var=" + encrip + "'><button style='background-color: #b3908f; color: #ffffff; font-size: 14px; font-family: arial; font-weight: bold; border: 0px; padding: 10px; cursor: pointer;'>Activar Cuenta</button></a></p> <p style='padding-top: 20px;'>Si tienes alguna duda sobre tu cuenta, puedes contactarnos a <a href='mailto:info@upalopa.com'>info@upalopa.com</a></p> <p style='padding-top: 20px;'>Saludos.</p>",
                          "destino" : correo, 
                          "titulo" : "Activación de cuenta Upalopa"
                        };
                      $.ajax
                      ({
                          type: "POST",
                          url: url,
                          data: parametros,
                          success: function(data)
                          {

                             
                          }
                      });


                //cierre de envio de correo
                mensaje2="<div class='ventana-titulo'>¡Te damos la  <br>Bienvenida a Upalopa!</div><div class='borde-ventana-punteada'><button title='Cerrar Ventana' type='button' class='close' data-dismiss='modal' aria-hidden='true' style='margin-top: -4.7em; margin-right: -5px;'>×</button><div align='center'><br><div class='text-justified' style='margin-top: 5.7em'>Se le ha enviado un correo de activación a su dirección electronica <br> <span class='text-info'> <b>" + correo + "</b> </br> </span>debe verificarlo en su bandeja de entrada o en spam, y presionar el link de activación<br>para poder usar su cuenta.</div> <br> <button id='aceptar' type='button' class='btn btn_form fondo_boton' data-dismiss='modal' onclick=\"abrir_login()\"><div class='vineta-blanco'>&nbsp;</div>Aceptar</button></div></div>";
                $("#contenido_modal").html(mensaje2);
              }
              $("#resultado1").html(mensaje);
              setTimeout(function() {
                $("#msg_act").fadeOut(1500);
              },3000);
               
            }
        });
        return false;
      }
    }
</script>
<div class="ventana-titulo">
  Crear mi cuenta <br>Upalopa
</div>
<div class="borde-ventana-punteada">
<button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -4.7em; margin-right: -5px;">×</button>
<html lang="es">
<body>
  <div class="" style="margin:0px; margin-top: 7.5em; margin-left:auto; margin-right: auto;">
    <div data-offset-top="100" class="" data-spy="affix">
      <div id="resultado1"></div>
    </div>
      <form method="POST" name="form_registrar" id="form_registrar" onsubmit="return jQuery(this).validationEngine('validate');">
      <input type="hidden" name="var_origen" id="var_origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
      <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
                
                  <label for="tipoi_user"> Identificaci&oacute;n:</label> 
                   <div class="row">   
                       <div class="col-md-4 col-xs-4">                         
                         <select name="tipoi_user" id="tipoi_user" class="validate[required] form-control fondo_campo">';
                            <option   value="V" >V</option>
                            <option   value="E" >E</option>
                          </select>
                      </div>
                        <div class="col-md-8 col-xs-7">                          
                             <input type="text" name="numi_user" id="numi_user" class="validate[required, custom[integer], minSize[6], maxSize[20]] text-input fondo_campo form-control" placeholder="P.ejem: 14123456">
                        </div>
                  </div>
               <div class="row">
                   <div class="col-md-12 col-xs-11">  
                        <label for="nom_ape_user" >Nombre y Apellido:</label>
                         <input type="text" name="nom_ape_user" id="nom_ape_user" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[60]] text-input fondo_campo form-control" placeholder="Nombre y Apellido del Usuario">
                   </div>   
               </div>   
               <br>
              <div class="row">
                <div class="col-md-2 col-xs-2"> 
                         <label for="sex_user"> Sexo:</label> 
                  
                </div>
                <div class="col-md-4 col-xs-4">  
                         <select name="sex_user" id="sex_user"   class="validate[required] fondo_campo form-control">';
                            <option   value="F" >F</option>
                            <option   value="M" >M</option>
                          </select>
                </div>
                     
              </div>

                  <div class="row">
                      <div class="col-md-12 col-xs-11">  
                          <label for="corre_user" >Correo Electronico:</label>
                          
                             <input type="text" name="corre_user" id="corre_user" class="validate[required, custom[email], minSize[5], maxSize[100]] text-input fondo_campo form-control" placeholder="Email">
                        </div>  
                    </div>
                  

                  <div class="row">
                    <div class="col-md-12 col-xs-11">  
                        <label for="pass_user" >Contraseña:</label>
                         
                           <input type="password" name="pass_user" id="pass_user" class="validate[required, equals[pass_user2], minSize[5], maxSize[100]] text-input fondo_campo form-control" placeholder="Contraseña de Acceso">
                        
                            <input type="password" name="pass_user2" id="pass_user2" class="validate[required, equals[pass_user]] text-input fondo_campo form-control" placeholder="Confirmar Contraseña Contraseña de Acceso">
                    </div>
                 
                  </div>

                  <div class="checkbox">
                      <label class="etiqueta_check">
                        <input type="checkbox" class="validate[required] fondo_campo " id="var_condiciones" name="var_condiciones" value="condiciones"> He leido y acepto los Términos y Condiciones para todas mis compras en www.upalopa.com.
                      </label>
                  </div>
                  <div class="checkbox">
                      <label class="etiqueta_check" >
                        <input type="checkbox" id="noti_user" class="fondo_campo" name="noti_user" value="1"> Suscribirse a nuestro Newsletter, Recibirás ofertas y descuentos exclusivos, así como actualizaciones.  
                      </label>
                  </div>
              
                <input type="hidden" name="tipo_user" id="tipo_user" value='2'>
                <input type="hidden" name="stat_user" id="stat_user" value='inactivo'>
                <div class="row">
                    <div class="col-md-7 col-xs-7">
                        <button onclick="abrir_login()" type="button" name="login"  id="login" class="btn btn_form fondo_predefinido" style="font-size:0.9em;" ><div class="vineta-blanco">&nbsp;</div>Ya tengo una Cuenta</button> 
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <button type="button"  onclick="guardando_registro()" name="guardar_registro"  id="guardar_registro" class="btn btn_form fondo_boton  pull-right" style="font-size:0.9em;"  ><div class="vineta-blanco">&nbsp;</div>Crear Cuenta</button>
                    </div>
                </div>
      </form>
  </div>
</body>
</html>
</div>