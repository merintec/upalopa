<?php
include_once("../comunes/variables.php");
include_once("../comunes/conexion.php");
?>
<!DOCTYPE html>
<script type="text/javascript">
function enviar_pass()
{
  if ($("#form1").validationEngine('validate')){
    var url="../comunes/enviarpass_aux.php";
    var correo=$("#usuario").val(); 
    $.ajax
    ({
        type: "POST",
        url: url,
        data: $("#form1").serialize(),
        success: function(data)
        {
          datatemp=data;
          datatemp=datatemp.split(":::");
          codigo=datatemp[0];
          mensaje=datatemp[1];
          password=datatemp[2];
          if (codigo==001)
          {
            // Enviar Correo
            var url="../comunes/enviar_correo.php"; 
            var parametros = {
                "mensaje" : "<p>Haz solicitado el reinicio de contraseña.</p><p>Para esto hemos definido la contraseña temporal: " + password + " .</p><p>Recuerda que debes ingresar al sistema y efectuar el cambio de la misma.</p><p>Con nosotros encontraras la mayor variedad y las mejores ofertas en ropa y demas accesorios para tus bebitos y niños</p>",
                "destino" : correo, 
                "titulo" : "Reinicio de contraseña Upalopa"
            };
            $.ajax
            ({
                type: "POST",
                url: url,
                data: parametros,
                success: function(data)
                {
                  $("#msg_login").removeClass("alert-danger");
                  $("#msg_login").addClass("alert-info");
                }
            });
          }
          else{
            $("#msg_login").removeClass("alert-info");
            $("#msg_login").addClass("alert-danger");
          }
          $('#msg_login').show();
          $("#msg_loging_content").html(mensaje);
          setTimeout(function() {
            $("#msg_login").fadeOut(1500);
        },5000);
        }
    });
    return false;
  }
}
</script>

<div class="ventana-titulo">
  Restauración de <br>Contraseña
</div>
<div class="borde-ventana-punteada">
<button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -4.7em; margin-right: -5px;">×</button>
<html lang="es">
<body>
  <div class="" style="margin:0px; margin-top: 7.5em; margin-left:auto; margin-right: auto;">
      <div id="msg_login" class="alert oculto">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong id="msg_loging_content"></strong>
      </div>
      <form method="POST" name="form1" id="form1" onsubmit="return jQuery(this).validationEngine('validate');">
      <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
        <div class="form-group">
          <label for="usuario" > Email</label>
          <input type="email" name="usuario" id="usuario" class="validate[required, custom[email] , minSize[3],maxSize[100]] text-input form-control fondo_campo" placeholder="Ingresar Email">
        </div>
        <div class="form-group">
          <label for="pass" style="font-size: 0.9em;"> Tres primeros números del documento de identidad</label>
          <input type="text" name="pass" id="pass" class="validate[required, custom[integer] , minSize[3],maxSize[3]] text-input form-control fondo_campo" placeholder="3 primeros dígitos">
        </div>
        <center> <button aling="center" id="ingresar" type="button" class="btn fondo_boton" onclick="enviar_pass()"> 
          <div class="vineta-blanco">&nbsp;</div> Enviar
        </button></center>
        <br>
      </form>
  </div>
</body>
</html>
</div>