<?php 
session_start();
include_once("../comunes/conexion.php");
include_once("../comunes/variables.php");
include("../comunes/verificar_logueo.php");
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
$id_user=$_SESSION['id_user'];
$tabla='direccion_envio';
$titulos_busqueda = 'Estado, Municipio, Parroquia, Nombre, Cédula, Teléfono,  Dirección, Correo, Código Postal ,Punto de Referencia';
$datos_busqueda = 'id_estado->estados::id_estado::estado,id_municipio->municipios::id_municipio::municipio,id_parroquia->parroquias::id_parroquia::parroquia,nomb_dire,cedu_dire,telf_dire,direccion,correo,cod_postal,punt_refe_dire';
$nregistros = 50;   
$color_fondo='#D2C8B0';
$logo='../imagenes/sistema/logo.png';
$var_where = 'id_user = '.$_SESSION['id_user']; 
?>
 
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $nom_pagina; ?></title>
  </head>
    <!-- validacion en vivo -->
<!-- funcion de guardar formulario -->  
<script type="text/javascript">
$(function()
{
    $("#guardar").click(function()
    {
      if ($("#form_dire").validationEngine('validate')){
        var url="../comunes/funcion_guardar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: $("#form_dire").serialize(),
            success: function(data)
            {
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                
                actualizar_dato('venta', 'id_dire', datatemp[2], 'id_venta', $('#id_venta2').val(),'');
                $("#form_dire")[0].reset();
              }
              $("#resultado").html(mensaje);
              setTimeout(function() {
                $("#msg_act").fadeOut(1500);
              },3000);
              ira_envio();
            }
        });
        return false;
      }
    });
});
function buscar()
{
  var url="../comunes/busqueda_cesta.php"; 
  $.ajax
  ({
    type: "POST",
    url: url,
    data: $("#form_dire").serialize(),
    success: function(data)
    {
      $("#resultado_busqueda").html(data);
    }
  });
  return false; 
}
</script>


<script type="text/javascript">
//combos
//combo municipios
$(document).ready(function() {
  // Parametros para el combo
  $("#id_estado").change(function () {
    $("#id_estado option:selected").each(function () {
      id_estado=$(this).val();
      $.post("municipios.php", { id_estado: id_estado}, function(data){
        $("#id_municipio").html(data);
      });     
    });
  });    
});
</script>

<script type="text/javascript">
//combo parroquias
$(document).ready(function() {
  // Parametros para el combo
  $("#id_municipio").change(function () {
    $("#id_municipio option:selected").each(function () {
      id_municipio=$(this).val();
      $.post("parroquias.php", { id_municipio: id_municipio}, function(data){
        $("#id_parroquia").html(data);
      });     
    });
  });    
});

function verificar_regalo() {
      id_venta=$('#id_venta2').val();
      $.post("../comunes/verifica_regalo.php", { id_venta: id_venta }, function(data){
        $("#regalo").html(data);
      });
}
</script>
  <body class="">

<div data-offset-top="100" class="container" data-spy="affix">
  <div id="resultado"></div>
</div>
<?php 
$sql_env = "SELECT * FROM direccion_envio WHERE id_user = ".$id_user;
$cant_env = mysql_num_rows(mysql_query($sql_env));
if ($cant_env > 0){ $add_clase = " display: none; "; $add_clase2 = ""; } else{ $add_clase = ""; $add_clase2 = " oculto "; }
?>
<div id="formulario" class="jumbotron cajacontacto"  style="margin-top: 0px; <?php echo $add_clase; ?>">
  <div class="ventana-titulo" style="width: 600px; margin-left: -20px; margin-top: -20px;">
    Datos de Envío
  </div>
  <div class="borde-ventana-punteada">
      <br><br><br><br>
            <form method="POST" name="form_dire" id="form_dire" onsubmit="return jQuery(this).validationEngine('validate');">
                <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
                <input type="hidden" name="var_titulos_busqueda" id="var_titulos_busqueda" value='<?php echo $titulos_busqueda; ?>'>
                <input type="hidden" name="var_datos_busqueda" id="var_datos_busqueda" value='<?php echo $datos_busqueda; ?>'>
                <input type="hidden" name="var_nregistros" id="var_nregistros" value='<?php echo $nregistros; ?>'>
                <input type="hidden" name="var_pag_actual" id="var_pag_actual" value=''>   
                <input type="hidden" name="var_where" id="var_where" value='<?php echo $var_where; ?>'>

                        <br>
                        <div class="row">

                             <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;">  <label for="id_estado" class="etq_form">Estados:</label> </div>
                                 <div class="col-md-8 col-xs-8">
                                     <div id="grupo_id_estado" class="">  
                                        <select name="id_estado" id="id_estado" class="validate[required] form-control fondo_campo">
                                        <option value="" selected disabled style="display:none;">Seleccione el Estado</option>
                                          <?php 
                                          //consulta  
                                          $consulta_estados = mysql_query("SELECT * FROM estados order by estado ");
                                          while($fila=mysql_fetch_array($consulta_estados))
                                          {
                                             echo "<option  value=".$fila[id_estado].">".$fila[estado]."</option>";
                                          }
                                          ?>


                                      </select>      

                                       <span id="boton_id_estado" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                              <span id="actualiza_id_estado" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                       </span> 

                                    </div>
                                  </div>
                          </div>
 

 

                        <div class="row">

                               <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="id_municipio" class="etq_form" > Municipios:</label> </div>
                                <div class="col-md-8 col-xs-8"> 
                                     <div id="grupo_id_municipio" class="">  
                                        <select name="id_municipio" id="id_municipio" class="validate[required] form-control fondo_campo">
                                        <option value="" selected disabled style="display:none;">Debe seleccionar el Municipio</option>
                                         


                                      </select>      

                                       <span id="boton_id_municipio" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                              <span id="actualiza_id_municipio" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                       </span> 

                                </div>
                              </div>
                    </div>
 
 

            

                <div class="row">
                           <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="id_municipio" class="etq_form" > Parroquias:</label> </div>
                              <div class="col-md-8 col-xs-8">  
                               <div id="grupo_id_parroquia" class="">  
                                  <select name="id_parroquia" id="id_parroquia" class="validate[required] form-control fondo_campo">
                                  <option value="" selected disabled style="display:none;">Debe seleccionar la Parroquia</option>
                                   


                                </select>      

                                 <span id="boton_id_parroquia" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                        <span id="actualiza_id_parroquia" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                 </span> 

                              </div>
                            </div>
                    </div>

                  <div class="row">
                    <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="nomb_dire" class="etq_form" > Persona que Recibe:</label> </div>
                       <div class="col-md-8 col-xs-8"> 
                          <div id="grupo_nomb_dire" class="">
                              <input type="text" name="nomb_dire" id="nomb_dire" class="validate[required, minSize[3], maxSize[60]] text-input form-control fondo_campo" placeholder="¿Quién recibe el envío?">
                              <span id="boton_nomb_dire" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="";>
                                <span id="actualiza_nomb_dire" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                              </span>
                          </div>
                        </div>
                  </div>



                  <div class="row">
                    <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="cedu_dire" class="etq_form" > Cédula de quien recibe:</label> </div>
                      <div class="col-md-8 col-xs-8"> 
                        <div id="grupo_cedu_dire" class="">
                          <input type="text" name="cedu_dire" id="cedu_dire" class="validate[required, minSize[3], maxSize[30]] text-input form-control fondo_campo" placeholder="Cédula de quien recibe">
                          <span id="boton_cedu_dire" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="";>
                            <span id="actualiza_cedu_dire" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                          </span>
                        </div>
                      </div>

                  </div>   



                  <div class="row">
                    <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="telf_dire" class="etq_form" > Teléfono:</label> </div>
                     <div class="col-md-8 col-xs-8"> 
                      <div id="grupo_telf_dire" class="">
                        <input type="text" name="telf_dire" id="telf_dire" class="validate[required, minSize[3], maxSize[30]] text-input form-control fondo_campo" placeholder="Teléfono de Contacto">
                        <span id="boton_telf_dire" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                          <span id="actualiza_telf_dire" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                        </span>
                      </div>
                    </div>
                  </div>   
              


                  <div class="row">
                    <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="direccion" class="etq_form" > Dirección:</label> </div>
                    <div class="col-md-8 col-xs-8"> 
                        <div id="grupo_direccion" class="">
                          <input type="text" name="direccion" id="direccion" class="validate[required, minSize[3], maxSize[250]] text-input form-control fondo_campo" placeholder="Dirección de envío">
                          <span id="boton_direccion" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                            <span id="actualiza_direccion" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                          </span>
                        </div>
                    </div>
                  </div>   


                  <div class="row">
                    <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="correo" class="etq_form" > Email:</label> </div>
                    <div class="col-md-8 col-xs-8"> 
                        <div id="grupo_direccion" class="">
                          <input type="text" name="correo" id="correo" class="validate[required, custom[email], minSize[3], maxSize[250]] text-input form-control fondo_campo" placeholder="Correo Electronico">
                          <span id="boton_direccion" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                            <span id="actualiza_direccion" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                          </span>
                        </div>
                    </div>
                  </div>  


                  <div class="row">
                    <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="cod_postal" class="etq_form" > Código Postal:</label> </div>
                    <div class="col-md-8 col-xs-8"> 
                        <div id="grupo_direccion" class="">
                          <input type="text" name="cod_postal" id="cod_postal" class="validate[required, minSize[3], maxSize[30]] text-input form-control fondo_campo" placeholder="Código Postal">
                          <span id="boton_direccion" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                            <span id="actualiza_direccion" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                          </span>
                        </div>
                    </div>
                  </div>  


                  <div class="row">
                    <div class="col-md-4 col-xs-4 text-right" style="padding-right: 0px;"> <label for="punt_refe_dire" class="etq_form" > Punto de Referencia:</label> </div>
                       <div class="col-md-8 col-xs-8">
                        <div id="grupo_punt_refe_dire" class="">
                          <input type="text" name="punt_refe_dire" id="punt_refe_dire" class="validate[required, minSize[3], maxSize[250]] text-input form-control fondo_campo" placeholder="Punto de Referencia">
                          <span id="boton_punt_refe_dire" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                            <span id="actualiza_punt_refe_dire" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                          </span>
                        </div>
                       </div>
                  </div>   


      <input type="hidden" name="id_user" id="id_user" value='<?php echo $id_user; ?>'>


                <br><div align="center"><button id="cancelar" type="button" class="btn btn_form fondo_boton oculto" onclick="ira_envio()"><div class="vineta-blanco">&nbsp;</div> Cancelar</button>  <button type="button" name="guardar"  id="guardar" class="btn btn-sm fondo_boton" ><div class="vineta-blanco">&nbsp;</div>Guardar </button> </div>
           
            </form>
          </div>
</div>
<div>
  <script>verificar_regalo();</script>
  <div class="container">
      <div class="container text-center text-info" style="font-size: 1.5em;" id="regalo">
      </div>
  </div>
</div>
<div class="row">
  <div class="container <?php echo $add_clase2; ?>">
    <div class="container text-center text-info" style="font-size: 1.2em; " >
      <span><img src="../imagenes/acciones/info.png"></span> Seleccione una dirección de la lista que se muestra a continuación o 
      <button onclick="muestra_oculta('formulario')" title="Agrega nuevos datos de facturación " id="btn_siguiente" type="button" class="btn fondo_boton"> Agrega nuevos datos <span class="vineta-add-datos"></span></button>
    </div>
  </div>
  <br>
  <div id='resultado_busqueda' class="container">
    <?php 
      $seleccion = "id_dire:::venta:::id_dire:::id_venta:::$('#id_venta2'):::ira_envio()";
      include_once("../comunes/busqueda_cesta.php");
    ?>
  </div>
</div>
  </body>
</html>

