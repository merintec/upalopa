<?php 
  session_start();
  include_once("../comunes/conexion.php");
  include_once("../comunes/variables.php");
  include("../comunes/verificar_logueo.php");
  $_SESSION['usuario_logueado'];
  $_SESSION['tipo_usuario'];
  $id_user=$_SESSION['id_user'];
  $color_fondo='#D2C8B0';
  $logo='../imagenes/sistema/logo.png';
?>
<script>
  function eliminar_cesta(id){
    var id_venta_prod = id;
    var url="eliminar_cesta.php"; 
    var parametros = {
      "id_venta_prod" : id_venta_prod
    };
    $.ajax
    ({
      type: "POST",
      url: url,
      data: parametros,
      success: function(data)
      {
        recargar_cuenta();
        ira_cesta();     
      }
    });
    $('#confirmar').removeClass('fade');
  }
  function pasar_modal(id,nregistro){
    $("#nregistro").html(nregistro);
    $("#id_eliminar").val(id);
  }
</script>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <title><?php echo $nom_pagina; ?></title>
  </head>
  <body class="">
  <?php 
    $consulta_productos="SELECT * FROM  venta as v, venta_productos  as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
    v.id_user='$id_user' and v.status_venta='carrito' and vp.id_venta=v.id_venta and vp.status_vent='carrito' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
    $consulta_productos=mysql_query($consulta_productos); 
  ?>
    <form > 
    <div class="container" >

        <div class="table-responsive">
            <table class="table table-striped table-hover">
              <tr class="th-tabla" >  <th class="th-tabla" class="text-right">#</th><th  class="text-left th-tabla">Imagen</th><th class="th-tabla">Descripción</th> <th class="text-center th-tabla">Color</th><th class="text-center th-tabla">Talla</th><th class="text-center th-tabla">Cantidad</th><th class="text-right th-tabla">Precio Unitario</th><th class="text-center th-tabla">Sub-Total</th><th class="text-right th-tabla">IVA (<?php echo $val_imp; ?>%)</th><th class="text-right th-tabla">Total</th> <th class="text-center th-tabla">&nbsp;</th class="th-tabla"></tr>
                <?php 
                while ($fila=mysql_fetch_array($consulta_productos))
                {
                  ///verificamos si esta en las giftcards
                  $text_giftcard = '';
                  $sql_gif = "SELECT * FROM giftcard WHERE id_venta_prod = ".$fila[id_venta_prod]."";
                  if ($giftcard = mysql_fetch_array(mysql_query($sql_gif))){
                    $text_giftcard = '<br><b>Para: </b>'.$giftcard[nomb_gif].'.<br><b>e-mail: </b>'.$giftcard[mail_gif].'<br><b>Mensaje: </b>'.$giftcard[msg_gif];
                  }
                  $i++;
                  $totalp=$fila[vuni_venta_prod]*$fila[cant_venta_prod];
                  $imp_cu = ($totalp / (($val_imp/100)+1));
                  $imp_cu = $imp_cu * $val_imp / 100;
                  $imp_cu = number_format($imp_cu,2,'.','');
                  //// para los totales generales
                  $peso_cu=$fila[peso_prod] * $fila[cant_venta_prod];
                  $total_pagar += $totalp;
                  $total_peso += $peso_cu;
                  $total_imp += $imp_cu;
                  $total_cant += $fila[cant_venta_prod];
                  $sub_totalp=$totalp-$imp_cu;
                    echo '<tr><th  class="text-right">'.$i.'</th> <td align="left"><div class="img-circle img_catalogo_div_back" style="margin-left: 10px;">  <img  class="img-circle img_catalogo_div"  src="../imagenes/uploads/productos/'.$fila[imag_prod].'" title="'.$fila[nomb_prod].'"></div></td> 
                    <td align="left"><b>'.$fila[nomb_prod].':</b><br>'.$fila[desc_prod].''.$text_giftcard.'</td> <td align="center">'.$fila[nom_color].'</td><td align="center">'.$fila[nomb_talla].'</td> <td align="center">'.$fila[cant_venta_prod].'</td>  <td align="right">'.number_format($fila[vuni_venta_prod],2,",",".").'</td><td align="right">'.number_format($sub_totalp,2,",",".").'</td><td align="right">'.number_format($imp_cu,2,",",".").'</td><td align="right">'.number_format($totalp,2,",",".").'</td>
                    <td align="center"><img id="eliminar" style="cursor:pointer;"  src="../imagenes/sistema/elimina.png" title="Eliminar de Cesta" data-toggle="modal" data-target="#confirmar" onclick="pasar_modal('.$fila[id_venta_prod].','.$i.')" /></td>
                    </tr>';
                  $id_venta2=$fila[id_venta];
                }
                echo '<script> $("#id_venta2").val("'.$id_venta2.'") </script>';
                echo '<tr><td colspan="10"><hr style="border: 2px dashed #555151;"></td></tr>';

              $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
              $consulta_envio=mysql_query($consulta_envio);
              $con_env=mysql_fetch_assoc($consulta_envio);;

              if (!$con_env[id_tenvios])
              {

                    $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                    $consulta_envio=mysql_query($consulta_envio);
                    $con_env=mysql_fetch_assoc($consulta_envio);

                    $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);

              }

                ?>
            </table>

        </div>
        <div class="row">
         <div class="text-center"> 
             <!--<div style="border: 3px dashed #555151; width: 23em; margin:auto auto; padding:1em;"> -->
             <div style="width: 23em; margin:auto auto; padding:1em;">
                    <table>
                      <tr><td align="left">Total de Productos:  </td><td align="right"><?php if (!$total_cant < 1) { ?> <b><?php echo $total_cant; ?></b></td></tr>

                      <tr><td align="left">Total Peso:</td><td align="right"><b><?php echo number_format(($total_peso/1000),1,",","."); ?> Kg.</b></td></tr>

                      <tr><td align="left">Subtotal:</td><td align="right"> <b>Bs.<?php echo number_format($total_pagar,2,",","."); ?></b></td></tr>
                     
                      <tr><td align="left">Costo de Envio:</td><td align="right"> <b>Bs. <?php echo number_format($con_env[prec_tenvio],2,",","."); ?></b></td></tr>
                     
                      <tr><td align="left">Costo de Seguro:</td><td align="right"> <b>Bs. <?php echo number_format(($total_pagar * ($con_env[porc_tenvio]/100)),2,",","."); ?></b></td></tr>
                      
                      <tr><td align="left"  >Total a Pagar:</td><td align="right"><span style=" font-size: 1.3em; color:#c174ad;?>;" ><b>Bs. <?php echo number_format(($total_pagar+$con_env[prec_tenvio]+($total_pagar * ($con_env[porc_tenvio]/100))),2,",","."); ?></b></td></tr>
                   </table>
                <?php } else {
                  echo '<script>ira_bloqueado();</script>';
                  echo 'No tienes artículos en tu cesta!!!';  
                }?>
              </div>

         </div>

        </div>


      </form>

    <!--modal para confirmacion -->
      <div class="modal fade" id="confirmar" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 400px">
          <div class="modal-content">
            <div class="modal-body">
              <div id="contenido_modal">
                <div class="ventana-titulo">
                  Esta acción requiere su confirmación
                </div>
                <div class="borde-ventana-punteada">
                  <button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -4.7em; margin-right: -5px;">×</button>
                  <div class="text-center" style="margin-top: 8em;">
                    <h4>¿Desea Eliminar el registro #<span  id="nregistro"></span>?</h4><br>
                    <input type="hidden"  id="id_eliminar" />
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <button class="btn fondo_boton" style="border: 0px;" data-dismiss="modal" onclick="eliminar_cesta($('#id_eliminar').val())">Aceptar</button>
                  </div>
                </div>
              </div>
            </div>
          </div>  
        </div>    
      </div>
</body>
</html>