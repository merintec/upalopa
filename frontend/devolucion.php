<?php
include_once("../comunes/variables.php");
include_once("../comunes/conexion.php");
$tabla='devoluciones';

$fecha=date("Y-m-d");
?>

<script>
  function guardar()
  {
      if ($("#formdev").validationEngine('validate')){
              var url="../comunes/funcion_guardar.php"; 
              $.ajax
              ({
                  type: "POST",
                  url: url,
                  data: $("#formdev").serialize(),
                  success: function(data)
                  {
                    //$("#resultado2").html(data);
                    var codigo, datatemp, mensaje;
                    datatemp=data;
                    datatemp=datatemp.split(":::");
                    codigo=datatemp[0];
                    mensaje=datatemp[1];
                    if (codigo==001)
                    {

                      enviar_correo_dev();
                      //$("#formdev")[0].reset();
                      $("#devolucion").modal('hide');
                      setTimeout(function() {
                          window.location=('compras.php');
                      },1500);

                    }
                    $("#resultado").html(mensaje);
                    setTimeout(function() {
                      $("#msg_act").fadeOut(1500);
                    },3000);
                     
                  }
              });
              return false;
            }

  }


function enviar_correo_dev()
{
          var correo=$("#var_correo").val();
          var nom_ape_user=$('#var_nom_ape').val();
          var orden=$('#var_orden').val();
          var url="../comunes/enviar_correo.php";
          var sex;
          if ($('#sex_user').val()=='F'){
            sex = 'a';
          }
          else {
            sex = 'o';  
          }  
          var mensaje = "Estimad" + sex + " " + nom_ape_user + ", <p style='padding-top: 20px;'>Hemos registrado su devolución <br>le estaremos contactando en las próximas 48 Horas vía telefónica para concretar la misma. </p><p>Gracias por preferirnos <a href='http://upalopa.com'>Upalopa.com</a></p>";
          var parametros = {
              "mensaje" : mensaje,
              "destino" : correo, 
              "correo_origen" : "<?php echo $correo_compras; ?>",
              "titulo" : "Registro de devolución de Producto"
            };
          $.ajax
          ({
              type: "POST",
              url: url,
              data: parametros,
              success: function(data)
              {

                 
              }
          });

}
  

  
</script>
<!DOCTYPE html>

<div class="ventana-titulo" style="font-size: 1.8em;">
  Devolución Producto <span id="nomb_prod"> </span> 
</div>
<div class="borde-ventana-punteada">
<button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -4.7em; margin-right: -5px;">×</button>
<html lang="es">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">   
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <!-- validacion en vivo -->
<script >
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form10").validationEngine('attach', {bindMethod:"live"});
   });
</script>

</head>
<body>


  <div class="" style="padding-top: 0.3em;  margin:0px; margin-left:auto; margin-right: auto;">
     <br><br><br><br><br>

     <div class="titulo_form">
           Orden de Compra N° <span id="codg_trans"> </span> 
      </div>
      <form method="POST" name="formdev" id="formdev" onsubmit="return jQuery(this).validationEngine('validate');">

          <input type="hidden" name="id_venta_prod" id="id_venta_prod" >
          <input type="hidden" name="fecha_reporte" id="fecha_reporte" value="<?php echo $fecha ?>" >


            
       
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-3 col-xs-3 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="motivo" > Motivo</label>
          </div>
          <div class="col-md-9 col-xs-9 text-right">
            <input type="text" name="motivo" id="motivo" class="validate[required minSize[3], maxSize[100]] text-input form-control fondo_campo" placeholder="Motivo de la Devolución">
          </div> 
        </div>
        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-3 col-xs-3 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="cantidad" > Cantidad</label>
          </div>
          <div class="col-md-9 col-xs-9 text-right">
            <input type="text" name="cantidad" id="cantidad" class="validate[required, custom[integer], minSize[1], maxSize[4]] text-input form-control fondo_campo" placeholder="Cantidad a devolver">
          </div> 
        </div>

        <div class="row" style="margin-top: 0.4em;">
          <div class="col-md-3 col-xs-3 text-right"  style="padding-right: 0px; padding-top: 0.5em;">
            <label for="motivo"> Teléfono</label>
          </div>
          <div class="col-md-9 col-xs-9 text-right">
            <input type="text" name="contacto" id="contacto" class="validate[required minSize[6], custom[phone], maxSize[20]] text-input form-control fondo_campo" placeholder="N° de Contacto">
          </div> 
        </div>
       
        <?php 

        $consulta_usuario="SELECT * FROM usuarios where id_user='$id_user'";
        $con_usu=mysql_fetch_assoc(mysql_query($consulta_usuario));

        ?>
         
         
          <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
          <input type="hidden" name="var_correo" id="var_correo" value='<?php echo $con_usu[corre_user]; ?>'>
          <input type="hidden" name="var_nom_ape" id="var_nom_ape" value='<?php echo $con_usu[nom_ape_user]; ?>'>
          
          <br>
        <center><button id="ingresar" type="button" class="btn fondo_boton" onclick="guardar()"><div class="vineta-blanco">&nbsp;</div> Enviar </button></center>        
      </form>
  </div>
</body>
</html>
</div>