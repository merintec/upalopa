<?php 
    $usuario_logueado = '';
    include('comunes/verificar_usuario.php');
    include_once("comunes/conexion.php");
    include_once("comunes/verificacion.php"); 
    include ('js/funciones.php'); 
?>
<script type="text/javascript">
$(document).ready(function() {
  recargar_cuenta();
  recargar_lista();
});


function recargar_cuenta()
{
    var id_user = <?php if ($_SESSION) { echo $_SESSION['id_user']; } else { echo "''"; } ?>;
    $.post("frontend/productos_carro.php", { id_user: id_user}, function(data){
        // $("#prueba").html(data);
        //alert(data);
        $("#productos_carro_menu").html(data);
        //alert(data);
        if (data==0){ 
            $("#productos_carro_menu").addClass('oculto');
        }
        else{
            $("#productos_carro_menu").removeClass('oculto');  
        }
    }); 
}

 function recargar_lista()
           {
               var id_cele=$("#id_cele").val();

                $.post("frontend/productos_lista.php", { id_cele: id_cele}, function(data){
               // $("#prueba").html(data);
               //alert(data);
                $("#productos_lista_menu").html(data);
                //alert(data);
                if (data==0){ 
                  $("#productos_lista_menu").addClass('oculto');
                }
                else{
                  $("#productos_lista_menu").removeClass('oculto');  
                }

                 }); 
           }
</script>
<nav id="top_pagina" role="navigation" class="navbar menu_frontend" style="background-color:<?php echo $color_fondo; ?>; ">
    <div class="navbar-header menu_frontend" >
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle" >
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="#" class="navbar-brand menu_frontend visible-xs">&nbsp;Upalopa.com</a>
    </div> 
    <div id="navbarCollapse" class="collapse navbar-collapse menu_frontend" style="background-color:<?php echo $color_fondo; ?>;">
        <ul class="nav navbar-nav navbar-right menu_frontend" style="background-color:<?php echo $color_fondo; ?>;">
            <li class="dropdown">
                <a href="#" data-toggle="dropdown"><span class="pull-left visible-xs glyphicon glyphicon-triangle-right"></span>&nbsp;Cat&aacute;logo</a>
                <ul class="dropdown-menu menu_frontend"  style="background-color:<?php echo $color_fondo; ?>;">
                  <?php 
                      //consulta  
                      $consulta_scategoria = mysql_query("SELECT * FROM categoria order by nomb_cate ");
                      while($fila=mysql_fetch_array($consulta_scategoria))
                      {
                          echo '<li><a href="'.$add_url.'frontend/catalogo.php?categoria='.$fila["id_cate"].'">'.$fila["nomb_cate"].'</a></li>';
                      }
                  ?>
                </ul>
            </li>
            <li><a href="frontend/outlet.php">Outlet</a></li>
            <li><a href="frontend/celebraciones.php">Listas de Regalo</a></li>

            <?php 
                if ($usuario_logueado) {
                   $fechacom=date("Y-m-d");

                    echo '<li><a href="frontend/procesar.php"><span class="vineta-cesta-lg pull-left">&nbsp;</span>&nbsp;Cesta de Compras&nbsp;            <span class="badge oculto" id="productos_carro_menu" style="margin-left:-0.5em; top:-1em; background-color: #e8e4e4; color: #555151;">  </span> </a></li>';
                    $consulta="SELECT * FROM lista_regalo WHERE id_user='$_SESSION[id_user]' and fech_cele>='$fechacom'";
                    $conc=mysql_query($consulta);
                    $conc=mysql_fetch_assoc($conc);

                     if ($conc['id_cele']!='')
                    {

                        echo '<input type="hidden" name="id_cele" id="id_cele" value="'.$conc[id_cele].'">';
                        echo '<li><a href="frontend/lista.php"><span class="vineta-default-white pull-left">&nbsp;</span>&nbsp;Mi Lista de Regalo&nbsp; <span class="badge oculto" id="productos_lista_menu" style="margin-left:-0.5em; top:-1em; background-color: #e8e4e4; color: #555151;">  </span> </a></li>';

                    }
                    echo '<li onclick="muestra_oculta(\'menu_user\')"><a href="#"><span class="glyphicon glyphicon-triangle-right"></span>&nbsp;'.$usuario_logueado.'</a></li>';
                }
                else{
                    echo '<li class="active " data-toggle="modal" data-target="#login" onclick="abrir_registro()"><a href="#">Registrarse</a></li>';
                    echo '<li data-toggle="modal" data-target="#login" onclick="abrir_login()"><a href="#">Iniciar Sesi&oacute;n</a></li>';
                } 
            ?>
            <li class="visible-xs"><a href="mailto:<?php echo $correo_upalopa; ?>">Cont&aacute;ctenos</a></li>
        </ul>
    </div>
</nav>
<div id="menu_user" class="pop_user_home" onclick="muestra_oculta(this.id)"  style="display: none;">
    <button class="btn btn_menu fondo_boton" title="Tus datos personales"  data-toggle="modal" data-target="#perfil">Tú Perfil<span class="vineta-user"></span></button>
    <a href="frontend/procesar.php" title="Revisa su cesta de compras actual"><button class="btn btn_menu fondo_boton">Tú Cesta de Compras<span class="vineta-cesta-lg"></span></button></a>
    <a href="frontend/pagos.php" title="Registra o verifica tus pagos"><button class="btn btn_menu fondo_boton">Tus Pagos<span class="vineta-money"></span></button></a>
    <a href="frontend/compras.php" title="Listado de las compras efectuadas"><button class="btn btn_menu fondo_boton">Tus Compras<span class="vineta-compras"></span></button></a>
    <a href="frontend/cupones.php" title="Cupones de Descuentos"><button class="btn btn_menu fondo_boton">Tus Cupones <span class="vineta-cupon"></span></button></a>
    <a href="frontend/giftcard.php" title="Tarjetas de Regalo"><button class="btn btn_menu fondo_boton">Tus GiftCard <span class="vineta-giftc"></span></button></a>
    <a href="frontend/lista_regalo.php" title="Lista de Regalo"><button class="btn btn_menu fondo_boton">Lista de Regalo <span class="vineta-lista-regalo"></span></button></a>
    <button class="btn btn_menu fondo_boton" title="Tus datos personales"  data-toggle="modal" data-target="#perfil">Cambio de Contraseña<span class="vineta-candado"></span></button>
    <div style="float:right" title="Cerrar la sesión actual">
        <a href="comunes/cerrar_sesion.php" style="color: #000; font-weight: bold; text-decoration: none;">Salir <span class="vineta-salir"></span> </a>
    </div>
</div>