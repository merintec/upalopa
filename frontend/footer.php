<footer class="footer pie_pagina">
  <div class="row hidden-xs" >
    <div class="col-md-9 col-sm-9 col-xs-8">
      <p class="text-center">
        <small>
          <br>Desarrollado por <a target="blank"  style="color: #FFF" href="http://merintec.com.ve"> Merintec </a> para Upalopa todos los derechos reservados.
        </small>
      </p>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-4 redes_sociales">
      <ul>
        <li title="facebook"><a target="new" href="https://www.facebook.com/pages/Upalopa/1591862134426631"><div class="rs_face">&nbsp;</div></a></li>
        <li title="Instagram"><a target="new" href="https://instagram.com/upalopa/"><div class="rs_insta">&nbsp;</div></a></li>
        <li title="Correo Electrónico"><a target="new" href="mailto:inf.upapola@gmail.com"><div class="rs_mail">&nbsp;</div></a></li>
      </ul>
    </div> 
    </div>
    <div class="row visible-xs">
      <div class="container" style="width: 15em;">
        <div class="row redes_sociales">
          <ul>
            <li title="facebook"><a target="new" href="https://www.facebook.com/pages/Upalopa/1591862134426631"><div class="rs_face">&nbsp;</div></a></li>
            <li title="Instagram"><a target="new" href="https://instagram.com/upalopa/"><div class="rs_insta">&nbsp;</div></a></li>
            <li title="Correo Electrónico"><a target="new" href="mailto:inf.upapola@gmail.com"><div class="rs_mail">&nbsp;</div></a></li>
          </ul>
        </div> 
      </div>
    </div> 
    <div class="row">
      <p class="text-center visible-xs" style="font-size:0.4em;">
        
          Desarrollado por <a target="blank"  style="color: #FFF" href="http://merintec.com.ve" target="blank" > Merintec </a>  <br>para Upalopa todos los derechos reservados.
        
      </p>
    </div>
  </div>
</footer>