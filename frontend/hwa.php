<div class="row hidden-xs" style="margin-left: 3em; margin-bottom: 2em;">
	<div class="col-md-2 col-xs-1">
		<a title="ir a página inicial" href="../index.php"><img class="logo_top" src="../imagenes/sistema/logo_tn_on.png"  width="80em" height="80em" ></a>		
	</div>
	<div class="col-md-8 col-xs-10" style="padding-top: 4em;">
		<div class="titulo_categoria text-info" style="margin-left: 0.5em;">UPALOPA.COM</div>
	</div>
</div>
<div class="row visible-xs" style="margin-bottom: 1em; ">
	<div class="col-xs-3">
		<a title="ir a página inicial" href="../index.php"><img class="logo_top" src="../imagenes/sistema/logo_tn_on.png" style="width: 100%"></a>		
	</div>
	<div class="col-xs-8">
		<div class="titulo_categoria text-info" style="margin-left: 0.2em;">UPALOPA.COM</div>
	</div>
</div>
<div class="row" style="margin-bottom: 2em;">
	<div class="col-md-2 col-xs-1">&nbsp;</div>
	<div class="col-md-7 col-xs-10 texto-contenido text-justificado respuesta" style="text-align: justify;">
		Upalopa es la tienda online de moda infantil multimarca,
		 un punto de encuentro para todas las mamás e interesados
		en el universo de los más pequeños, que buscan la mezcla entre 
		un toque casual y tendencias en  las mejores marcas.<br><br>
		En Upalopa nos encanta el buen vestir y disfrutamos compartirlo, 
		por lo que seleccionamos las mejores marcas de ropa para bebés y
		 niños hasta los 8  años y en algunas marcas hasta 12 años.<br><br>
		Creemos en los detalles y en las cosas hechas con amor, 
		disfrutamos todo lo relacionado con el mundo de los niños y deseamos 
		que, entrando en www.upalopa.com, tu experiencia sea  tan especial que 
		merezca ser contada.<br><br>
		Entra a <a herf="upalopa.com">www.upalopa.com</a> y descubre el mundo que tenemos para tu niño.
	</div>
	<div class="col-md-2 col-xs-1">&nbsp;</div>
</div>
<br>
<br>
<br>
<br>
<br>