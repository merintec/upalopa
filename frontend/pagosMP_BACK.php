<?php 
session_start();
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
include("../comunes/conexion.php");
include("../comunes/verificar_logueo.php");
//// requerido para generar boton mercadopago
require_once ('../comunes/mp/lib/mercadopago.php');
/////////////////////////////////////////////
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
$id_user=$_SESSION['id_user'];
$categoria=$_GET['categoria'];
$categoria = '-7';
$logo='../imagenes/sistema/logo.png';
$con[nomb_cate] = 'Tus Pagos';
$con[desc_cate] = '<b>Tu Tienda UPALOPA<b>';
$color_fondo='#D2C8B0';
include("../comunes/variables.php");
include("../comunes/verificar_usuario_login.php");
$buscar_mp = "SELECT * FROM metodo_pago WHERE nomb_mpago = 'MERCADOPAGO'";
$codg_mp = mysql_fetch_array(mysql_query($buscar_mp));
$id_mpago = $codg_mp[id_mpago];
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <script src="../js/inactividad.js"></script>


    <title><?php echo $nom_pagina; ?></title>
    <script>
      function pasar_modal(id,orden,giftcard)
      {
        var parametros = {
          "giftcards" : giftcard
        }
        var url="registro_pago.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: parametros,
            beforeSend: function(){
            },
            success: function(data)
            {
              $('#contenido_pago').html(data);
              $("#id_venta").val(id);
              $("#codg_trans").html(orden);
              $("#var_orden").val(orden);
            }
        });
        return false;
      }
    </script>
    <script>
      function registrar_mp(id_venta, id_mpago, mont_pago, iden_per_pago, nomb_per_pago, fech_pago, status_pago, num_refe_banc, msg, correo, sexo, orden)
      {
        var parametros = {
          "var_tabla" : "pago",
          "id_venta" : id_venta,
          "id_mpago" : id_mpago, 
          "mont_pago" : mont_pago, 
          "iden_per_pago" : iden_per_pago,
          "nomb_per_pago" : nomb_per_pago,
          "fech_pago" : fech_pago, 
          "status_pago" : status_pago, 
          "num_refe_banc" : num_refe_banc
        }
        var url="../comunes/funcion_guardar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: parametros,
            beforeSend: function(){
            },
            success: function(data)
            {
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                actualizar_dato_mp('venta','status_venta','pagado','id_venta',id_venta);
                fecha_act = '<?php echo date('Y-m-d'); ?>';
                actualizar_dato_mp('venta','fech_stat', fecha_act,'id_venta',id_venta);
                enviar_correo_mp(orden,nomb_per_pago,correo,sexo,mont_pago);
                setTimeout(function() {
                    window.location=('pagos.php');
                },4000);
              }
              $("#resultado").html(msg);
              setTimeout(function() {
                $("#msg_act").fadeOut(3000);
              },3000);               
            }
        });
        return false;
      }
    function enviar_correo_mp(orden,nombre,correo,sexo,monto)
    {
              var correo=correo;
              var nom_ape_user=nombre;
              var orden=orden;
              var monto=monto;
              var sexo = sexo;
              var url="../comunes/enviar_correo.php";
              var sex;
              if (sexo=='F'){
                sex = 'a';
              }
              else {
                sex = 'o';  
              }  
              var mensaje = "Estimad" + sex + " " + nom_ape_user + ", <p style='padding-top: 20px;'>Hemos registrado un pago por MERCADOPAGO para<br>la orden de compra N° <b>" + orden + "</b> por Bs. <b>" + monto + "</b>  en las próximas 48 horas será verificado y se le notificará para el envío del producto. </p><p style='padding-top: 20px;'>Gracias por preferirnos <a href='http://upalopa.com'>Upalopa.com</a></p>";
              //var mensaje = "Estimad" + sex + " " + nom_ape_user + ", <p style='padding-top: 20px;'>!Su pago ha sido registrado exitosamente para la orden de compra N° " + orden + "! <p style='padding-top: 20px;'>Para mayor detalle consulte la sección <b>Tus Pagos</b> en nuestro sitio <a href='http://upalopa.com'>www.upalopa.com</a></p>",
              var parametros = {
                  "mensaje" : mensaje,
                  "destino" : correo, 
                  "correo_origen" : "<?php echo $correo_compras; ?>",
                  "titulo" : "Pago Registrado Upalopa"
                };
              $.ajax
              ({
                  type: "POST",
                  url: url,
                  data: parametros,
                  success: function(data)
                  {

                     
                  }
              });
    }
    function actualizar_dato_mp(tabla,campo,valor,campo_id,valor_id)
    {
        //para verificar si es una fecha voltearla al momento de guardarla
          var nueva;
          nueva = valor.split("-");
          if (valor.length == 10 && nueva[0].length == 2 && nueva[1].length == 2 && nueva[2].length == 4){
            valor = nueva[2]+'-'+nueva[1]+'-'+nueva[0];
          }
          var parametros = {
            "var_tabla": tabla,
            "var_campo" : campo,
            "var_valor" : valor,
            "var_id" : campo_id,
            "var_id_val" : valor_id
          };
          var url="../comunes/funcion_actualizarcampo.php"; 
          $.ajax
          ({
            type: "POST",
              url: url,
              data: parametros,
              success: function(data)
              {

                 $("#resultado").html(msg);
                  setTimeout(function() {
                    $("#msg_act").fadeOut(3000);
                  },3000);    
                             

              }
          });
          return false; 
    }



  
    
 
    </script>
  </head>
  <body class="">    
    <div style="background-color:<?php echo $color_fondo; ?>;" class="cabecera_categorias">
        <a title="ir a página inicial" href="../index.php"><img id="logo_top" class="logo_top" src="<?php echo $logo; ?>" ></a>
    </div>
    <div class="container-fluid">
      <div class="row">
        <?php include("menu_cesta.php"); ?>
      </div>
      <br>
      <div data-offset-top="280" class="container" data-spy="affix">
        <div id="resultado"></div>
      </div>
      <?php
         $consulta1="SELECT * FROM venta as v  where v.id_user='$id_user' and v.status_venta='procesado' ";
         $con1= mysql_fetch_assoc(mysql_query($consulta1));

         if ($con1[id_venta]==NULL)
         {
              ?>

                    <script type="text/javascript">  
                          alert("No tiene pagos que registrar");
                          window.location=("catalogo.php"); 
                    </script>

              <?php 


         }



      ?>
      <br>
      <div class="text-center"> <span class="text-info"><h3> <b> Registro de Pago </b>  </h3></span> </div>
    <div class="container" >

        <?php    

        $consulta="SELECT * FROM venta as v, usuarios as u  where v.id_user='$id_user' and v.id_user=u.id_user and v.status_venta='procesado' ";
        $consulta=mysql_query($consulta);

        $i=0;
        while($fila=mysql_fetch_array($consulta))
        {
          $totalp=0;
          $total_pagar=0;
          $total_cant=0;
          $i++;
                   $consulta2="SELECT * FROM venta_productos as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
                   vp.id_venta='$fila[id_venta]' and vp.status_vent='procesado' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
                  $consulta2=mysql_query($consulta2);
                  while ($fila2=mysql_fetch_array($consulta2)) 
                  {
                     $totalp=$fila2[vuni_venta_prod]*$fila2[cant_venta_prod];
                     $total_pagar += $totalp;
                     $total_cant += $fila2[cant_venta_prod];
                     $peso_cu=$fila2[peso_prod] * $fila2[cant_venta_prod];
                     $total_peso += $peso_cu;

                      $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
                      $consulta_envio=mysql_query($consulta_envio);
                      $con_env=mysql_fetch_assoc($consulta_envio);;

                      if (!$con_env[id_tenvios])
                      {

                            $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                            $consulta_envio=mysql_query($consulta_envio);
                            $con_env=mysql_fetch_assoc($consulta_envio);

                            $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);

                      }


                    }

                    /// consultamos los pago de venta
                    $sql_pagado = "SELECT SUM(mont_pago) as aprobado FROM pago p WHERE p.id_venta = ".$fila[id_venta]." AND status_pago='Aprobado' GROUP BY p.id_venta";
                    $res_pagado = mysql_fetch_array(mysql_query($sql_pagado));
                    $pagado = $res_pagado[aprobado];
                    $sql_pagado = "SELECT SUM(mont_pago) as por_confirmar FROM pago p WHERE p.id_venta = ".$fila[id_venta]." AND status_pago='registrado' GROUP BY p.id_venta";
                    $res_pagado = mysql_fetch_array(mysql_query($sql_pagado));
                    $registrado = $res_pagado[por_confirmar];
                    $mont_compra = number_format(($total_pagar+$con_env[prec_tenvio]+($total_pagar * ($con_env[porc_tenvio]/100))),2,".","");
                    $mont_por_pagar = number_format(($mont_compra - number_format(($pagado + $registrado),2,'.','')),2,'.','');

                  
                    echo '<div class="table-responsive procesar_linea_punteada">';
                    echo '<table class="table table-striped table-hover">
                    <tr style="font-size: 0.8em"><th>N° de Orden </th> <th>Nº de Productos </th><th>Monto de la Compra</th><th>Confirmado</th><th>Por Confirmar</th><th>Por Pagar</th><th>Detalles</th><th style="text-align: center;"> Formas de Pago </th> </tr>';
                    /////// codigo para generar el pago con Mercado Pago
                      $mp = new MP('5909923339367308', '2dqGOZUXMKfrwepZUxLG8HBKvmHIRwpf');
                      $mp_usuario = $fila[nom_ape_user];
                      $mp_email = $fila[corre_user];
                      $mp_ncompra = $fila[codg_trans];
                      $mp_price = number_format($mont_por_pagar,2,".","");
                      $mp_price = floatval($mp_price);
                      $preference_data = array(
                        "payer" => array(
                          "name" => $mp_usuario,
                          //"email" => $mp_email
                          "email" => "test_user_28216468@testuser.com"
                        ),
                        "payment_methods" => array(
                          "excluded_payment_types" => array( 
                            array( "id"=>"ticket"), 
                            array( "id"=>"bank_transfer"), 
                            array( "id"=>"atm"), 
                            //array( "id"=>"credit_card"), 
                            //array( "id"=>"debit_card"), 
                            //array("id"=>"digital_currency"),
                            //array("id"=>"digital_prepaid_card") 
                          )   
                        ),
                        "items" => array(
                          array(
                            "title" => "Compra #".$mp_ncompra." UPALOPA",
                            "picture_url" =>"http://upalopa.com/imagenes/sistema/logo.png",
                            "quantity" => 1,
                            "currency_id" => "VEF",
                            "unit_price" => $mp_price
                          )
                        )
                      );
                      $preference = $mp->create_preference($preference_data);
                      ?>
                      <script type="text/javascript">
                        function execute_my_onreturn<?php echo $i; ?> (json) {
                            if (json.collection_status=='approved'){
                              id_venta = '<?php echo $fila[id_venta]; ?>';
                              id_mpago = '<?php echo $id_mpago ?>';
                              mont_pago = <?php echo $mp_price ?>;
                              iden_per_pago = '<?php echo $fila[numi_user]; ?>';
                              nomb_per_pago = '<?php echo $fila[nom_ape_user]; ?>';
                              fech_pago = '<? echo date('Y-m-d'); ?>';
                              status_pago =  'Aprobado';
                              num_refe_banc = json.collection_id;
                              msg = "</br><div class='alert alert-info' id='msg_act'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='glyphicon glyphicon-info-sign pull-left'></span>&nbsp;&nbsp;<strong>El pago se registró exitosamente. bajo el Número " + json.collection_id + ". Proximamente serás informado sobre el estatus de la compra.</strong></div>";
                              correo =  '<?php echo $fila[corre_user]; ?>';
                              sexo = '<?php echo $fila[sex_user]; ?>';
                              orden = '<?php echo $mp_ncompra; ?>';
                              registrar_mp(id_venta, id_mpago, mont_pago, iden_per_pago, nomb_per_pago, fech_pago, status_pago, num_refe_banc, msg, correo, sexo,orden);
                            } else if(json.collection_status=='pending'){
                                alert ('No has completado el proceso de pago, no se ha generado ningún pago');
                            } else if(json.collection_status=='in_process'){
                                id_venta = '<?php echo $fila[id_venta]; ?>';
                                id_mpago = '<?php echo $id_mpago ?>';
                                mont_pago = <?php echo $mp_price ?>;
                                iden_per_pago = '<?php echo $fila[numi_user]; ?>';
                                nomb_per_pago = '<?php echo $fila[nom_ape_user]; ?>';
                                fech_pago = '<? echo date('Y-m-d'); ?>';
                                status_pago =  'registrado';
                                num_refe_banc = json.collection_id;
                                msg = "</br><div class='alert alert-info' id='msg_act'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='glyphicon glyphicon-info-sign pull-left'></span>&nbsp;&nbsp;<strong>El pago se registró exitosamente. bajo el Número " + json.collection_id + ". Pero está siendo revisado por MercadoPago. Te informaremos cuando se confirme el mismo.</strong></div>";
                                correo =  '<?php echo $fila[corre_user]; ?>';
                                sexo = '<?php echo $fila[sex_user]; ?>';
                                orden = '<?php echo $mp_ncompra; ?>';
                                registrar_mp(id_venta, id_mpago, mont_pago, iden_per_pago, nomb_per_pago, fech_pago, status_pago, num_refe_banc, msg, correo, sexo,orden);
                            } else if(json.collection_status=='rejected'){
                                alert ('El pago fué rechazado, intente nuevamente el pago. Si persiste contante a un representante de su Banco');
                            } else if(json.collection_status==null){
                                alert ('No has completado el proceso de pago, no se ha generado ningún pago');
                            }
                        }
                      </script>
                      <?php
                    ////////////////////////////////////////////////////

                  echo '<tr><td>'.$fila[codg_trans].' </td> <td align="center">'.$total_cant.'</td> <td style="text-align: right; padding-left: 5px; padding-right: 5px;">'.number_format($mont_compra,2,',','.').' </td><td style="text-align: right; padding-left: 5px; padding-right: 5px;">'.number_format($pagado,2,',','.').'</td><td style="text-align: right; padding-left: 5px; padding-right: 5px;">'.number_format($registrado,2,',','.').'</td><td style="text-align: right; padding-left: 5px; padding-right: 5px;">'.number_format($mont_por_pagar,2,',','.').'</td><td align="center"><img id="ver" style="cursor:pointer;"  src="../imagenes/acciones/lupa.png" title="Mostrar detalle de la compra" onclick="muestra_oculta(\'desplegar'.$i.'\')"></td><td style="text-align: right; padding-left: 5px; padding-right: 5px;"><img height="40px" id="pagar" style="cursor:pointer;"  src="../imagenes/acciones/money2.png" data-toggle="modal" data-target="#modal_pago" onclick="pasar_modal('.$fila[id_venta].',\''.$fila[codg_trans].'\',\'NO\');" title="Registrar Depósito o Transferencia"><img height="40px" id="pagar" style="cursor:pointer;"  src="../imagenes/acciones/icono_descuento.png" data-toggle="modal" data-target="#modal_pago" onclick="pasar_modal('.$fila[id_venta].',\''.$fila[codg_trans].'\',\'CUPON\');" title="Utilizar cupón de Descuento">';?>   <?php echo '<img src="../imagenes/acciones/giftcard_tn.png" data-toggle="modal" data-target="#modal_pago" onclick="pasar_modal('.$fila[id_venta].',\''.$fila[codg_trans].'\',\'SI\');" title="Utilizar GiftCard" style="cursor:pointer;">'; ?> <a href="<?php echo $preference['response']['init_point']; ?>" name="MP-Checkout" class="ico-mp" mp-mode="modal" onreturn="execute_my_onreturn<?php echo $i; ?>" title="Paga usando tus tarjetas de crédito por medio de MercadoPago">Pagar</a><?php echo'</td></tr>';
                  echo '</table>';
                        echo '<div class="table-responsive" >';

                          echo '<div id="desplegar'.$i.'" style="display: none;">';

                            echo '<div class="col-md-6 col-xs-6">';
                              echo '<span class="text-info" ><b>Datos de la Compra</b></span>';
                            echo '<table   class="table table-striped table-hover" style="font-size: 0.7em">'; 
                            echo '<tr> <th># </th> <th>Producto</th> <th>Cantidad </th> <th> Precio Unitario </th> <th> Total </th> </tr>';

                            //consulta para mostrar detalle de articulos
                            $consulta3="SELECT * FROM venta_productos as vp, productos as p, producto_detalles as pd, tallas as t, colores as c where 
                             vp.id_venta='$fila[id_venta]' and vp.status_vent='procesado' and pd.id_prod_deta=vp.id_prod_deta and p.id_prod=pd.id_prod and t.id_talla=pd.id_talla and c.id_color=pd.id_color order by p.nomb_prod, t.nomb_talla";
                            $consulta3=mysql_query($consulta3);
                            $j=0;
                              

                            while ($fila3=mysql_fetch_array($consulta3)) 
                            {
                                $totalp=$fila[vuni_venta_prod]*$fila[cant_venta_prod];
                                $imp_cu = ($totalp / (($val_imp/100)+1));
                                $imp_cu = $imp_cu * $val_imp / 100;
                                $imp_cu = number_format($imp_cu,2,'.','');
                                //// para los totales generales
                                $peso_cu=$fila[peso_prod] * $fila[cant_venta_prod];
                                $total_pagar += $totalp;
                                $total_peso += $peso_cu;
                                $total_imp += $imp_cu;
                                $total_cant += $fila[cant_venta_prod];
                                $subtotal=$fila3[vuni_venta_prod]*$fila3[cant_venta_prod];

                                $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and pesof_tenvio>='$total_peso'";
                                $consulta_envio=mysql_query($consulta_envio);
                                $con_env=mysql_fetch_assoc($consulta_envio);;

                                if (!$con_env[id_tenvios])
                                {

                                      $consulta_envio="SELECT * FROM tabla_envios where pesoi_tenvio<='$total_peso' and  pesof_tenvio=0 order by pesoi_tenvio desc limit 1";
                                      $consulta_envio=mysql_query($consulta_envio);
                                      $con_env=mysql_fetch_assoc($consulta_envio);

                                      $con_env[prec_tenvio]=$con_env[prec_tenvio]*($total_peso/1000);

                                }
                                $j++;
                                        echo '<tr> <td>'.$j.'</td><td>'.$fila3[nomb_prod].' </td> <td align="center">'.$fila3[cant_venta_prod].'</td> <td align="right">'.number_format($fila3[vuni_venta_prod],2,",",".").' </td><td align="right">'.number_format($subtotal,2,",",".").' </td> </tr>';

                               
                                
                            }
                                echo '<tr><td align="right" colspan="3"><b>Subtotal: Bs.</b> </td><td align="right" colspan="2"><b>'.number_format($total_pagar,2,",",".").'</b></td></tr>';
                                echo '<tr><td align="right" colspan="3"><b>Costo de Envio: Bs. </b></td><td align="right" colspan="2"><b>'.number_format($con_env[prec_tenvio],2,",",".").'</b></td></tr>';
                                echo '<tr><td align="right" colspan="3"><b>Costo de Seguro: Bs.</b> </td><td align="right" colspan="2"><b>'.number_format(($total_pagar * ($con_env[porc_tenvio]/100)),2,",",".").'</b></td></tr>';
                                echo '<tr><td align="right" colspan="3"><b>Total a Pagar Bs. </b></td><td align="right" colspan="2"  style=" font-size: 1.3em; color:#c174ad;?>;" ><b>'.number_format(($total_pagar+$con_env[prec_tenvio]+($total_pagar * ($con_env[porc_tenvio]/100))),2,",",".").'</b></td></tr>';
                            echo "</table>"; 
                            echo '</div>';
                            echo '<div class="col-md-6 col-xs-6">';
                              echo '<span class="text-info" ><b>Datos de los Pagos</b></span>';
                              echo '<table  class="table table-striped table-hover" style="font-size: 0.7em">'; 
                              echo '<tr> <th># </th> <th style="width:7.2em;">Fecha</th> <th>Tipo</th> <th>Destino</th> <th>Referencia</th> <th>Monto</th><th>Status</th></tr>';
                              $sql_pagos = "SELECT * FROM pago p, cuentas_bancarias cb, metodo_pago mp WHERE p.id_venta = ".$fila[id_venta]." AND p.id_cuen_banc=cb.id_cuen_banc AND p.id_mpago=mp.id_mpago";
                              $bus_pagos = mysql_query($sql_pagos);
                              $count_pago = 1;
                              while ($res_pagos=mysql_fetch_array($bus_pagos)){
                                $fecha = strtotime($res_pagos[fech_pago]);
                                $res_pagos[fech_pago] = date("d-m-Y", $fecha);
                                echo '<tr title="'.$res_pagos[obsr_pago].'"> <td class="text_right">'.$count_pago.'</td> <td>'.$res_pagos[fech_pago].'</td><td>'.$res_pagos[nomb_mpago].'</td> <td>'.$res_pagos[desc_cuen_banc].'</td> <td class="text-center">'.$res_pagos[num_refe_banc].'</td> <td class="text_right">'.number_format($res_pagos[mont_pago],2,",",".").'</td><td><span id="status'.$res_pagos[id_pago].'">'.ucwords($res_pagos[status_pago]).'</span></td></tr>';
                                $count_pago++;
                              }
                              $sql_pagos = "SELECT * FROM pago p, metodo_pago mp WHERE p.id_venta = ".$fila[id_venta]." AND p.id_cuen_banc IS NULL AND p.id_mpago=mp.id_mpago";
                              $bus_pagos = mysql_query($sql_pagos);
                              $count_pago = 1;
                              while ($res_pagos=mysql_fetch_array($bus_pagos)){
                                $fecha = strtotime($res_pagos[fech_pago]);
                                $res_pagos[fech_pago] = date("d-m-Y", $fecha);
                                echo '<tr title="'.$res_pagos[obsr_pago].'"> <td class="text_right">'.$count_pago.'</td> <td>'.$res_pagos[fech_pago].'</td><td>'.$res_pagos[nomb_mpago].'</td> <td>'.$res_pagos[desc_cuen_banc].'</td> <td class="text-center">'.$res_pagos[num_refe_banc].'</td> <td class="text_right">'.number_format($res_pagos[mont_pago],2,",",".").'</td><td><span id="status'.$res_pagos[id_pago].'">'.ucwords($res_pagos[status_pago]).'</span></td></tr>';
                                $count_pago++;
                              }
                              echo '</table>';
                            echo '</div>';
                      echo "</div>";
                    echo '</div>';
                  echo '</div>';













        }


       ?>
       </div>
 
      <br>
      <br>
      <br>
      <br>

      <div class="row hidden-xs" role="navigation">
        <?php
           include("menu_footer.php");
           include("footer.php");
        ?>
      </div>
      <div class="row visible-xs">
        <?php  include("footer.php"); ?>
      </div>
    </div>
        <!-- Modal para perfil -->
    <div class="modal fade" id="perfil" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal_perfil">
              <?php 
                include('perfil.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div>

    <!-- Modal para login -->
    <div class="modal fade" id="login" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 350px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal">
              <?php 
                include('login.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div> 
      </div>    

      <!-- Modal para Registrar Pago -->
    <div class="modal fade" id="modal_pago" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_pago">

            </div>
          </div>
        </div>  
      </div>    
    </div> 
    <script src="../bootstrap/js/bootstrap.min.js"> </script>  
    <script type="text/javascript">
        (function(){function $MPC_load(){window.$MPC_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;
        s.src = document.location.protocol+"//resources.mlstatic.com/mptools/render.js";
        var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPC_loaded = true;})();}
        window.$MPC_loaded !== true ? (window.attachEvent ? window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;})();
    </script>
  </body>
</html>