<div class="logo_carousel hidden-xs">&nbsp;</div>
<div class="logo_carousel_xs visible-xs">&nbsp;</div>
<div id="carousel1" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel1" data-slide-to="0" class="active"></li>
    <li data-target="#carousel1" data-slide-to="1"></li>
    <li data-target="#carousel1" data-slide-to="2"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="imagenes/carrusel/carrusel1.jpg" width="100%" title="UPALOPA.COM">
      
      <div class="carousel-caption"></div>
    </div>
    <div class="item">
      <img src="imagenes/carrusel/carrusel2.jpg" width="100%" title="UPALOPA.COM">
      <div class="carousel-caption"></div>
    </div>
    <div class="item">
      <img src="imagenes/carrusel/carrusel3.jpg" width="100%" title="UPALOPA.COM">
      <div class="carousel-caption"></div>
    </div>   
  </div>
  <!-- Controls -->
  <!-- <a class="left carousel-control" href="#carousel1" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="right carousel-control" href="#carousel1" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Siguiente</span>
  </a> -->
</div>