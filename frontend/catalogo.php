<?php 
session_start();
include("../comunes/conexion.php");
$categoria=$_GET['categoria'];

if (!$categoria){
  $categoria = '-7';
}
if ($categoria=='1')
{
  $color_fondo='#ADC8CC';
}
if ($categoria=='2')
{
  $color_fondo='#CEBCBA'; 
}
if ($categoria=='3')
{
  $color_fondo='#97A8C0'; 
}
if ($categoria=='4')
{
  $color_fondo='#DCB0B4'; 
}
if ($categoria=='5')
{
  $color_fondo='#D2C8B0'; 
}
if ($categoria=='6')
{
  $color_fondo='#D2C8B0'; 
}
if ($categoria=='7')
{
  $color_fondo='#D2C8B0';
}
if ($categoria<0)
{
  $color_fondo='#D2C8B0';
}
$logo='../imagenes/sistema/logo.png';
$consulta=mysql_query("SELECT * FROM categoria where id_cate='$categoria' ");
$con=mysql_fetch_assoc($consulta);
if ($con['nomb_cate']){
  $filtro_cate = "c.id_cate='$categoria' and";
}
else{  
  $con['nomb_cate'] = 'Productos';
  $con['desc_cate'] = '<b>Tu Tienda UPALOPA<b>';
}
include("../comunes/variables.php");
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <link href="../imagenes/favicon.ico" rel="shortcut icon">
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <body class="">    
    <div style="background-color:<?php echo $color_fondo; ?>;" class="cabecera_categorias">
        <a title="ir a página inicial" href="../index.php"><img class="logo_top" src="<?php echo $logo; ?>" ></a>
    </div>
    <div class="container-fluid">
      <div class="row">
        <?php include("menu_catalogo.php"); ?>
      </div>

      <div class="row">
        
         <div class="col-md-5 col-xs-4">    <hr class="linea_punteada" align="right" style= " border: 1px dashed<?php echo $color_fondo; ?>;"> </div>
          <div class="col-md-2 col-xs-4 text-center">    <span class="titulo_categoria" style= "color:<?php echo $color_fondo; ?>;"> <?php echo ucwords($con['nomb_cate']); ?> </span> <br><span class="desc_cate"> <?php echo $con['desc_cate']; ?> </span>    </div>
          <div class="col-md-5 col-xs-4">  <hr class="linea_punteada" align="left" style= "border: 1px dashed <?php echo $color_fondo; ?>;"> </div>
          <br>
          <br>
      </div>

      <?php
        /// BUSCAR LOS PARAMETROS EN TODOS LOS CAMPOS TIPO VAR O TEXT
        if ($_POST['buscar_a']){
          $buscar_a = explode(' ', $_POST['buscar_a']);
          $n_palabras = count($buscar_a);
          $add_search = '';
          /// para buscar en productos
          $sql_busqueda = "SHOW COLUMNS FROM productos WHERE Type LIKE '%var%' OR Type LIKE '%text%'";
          $bus_busqueda = mysql_query($sql_busqueda);
          while ($res_busqueda = mysql_fetch_array($bus_busqueda)) { 
            for ($i=0;$i<$n_palabras;$i++){
              $add_search .= "p.".$res_busqueda[Field]." LIKE '%".$buscar_a[$i]."%' OR ";
            }
          }
          /// para buscar en Sub_Categorias
          $sql_busqueda = "SHOW COLUMNS FROM sub_categoria WHERE Type LIKE '%var%' OR Type LIKE '%text%'";
          $bus_busqueda = mysql_query($sql_busqueda);
          while ($res_busqueda = mysql_fetch_array($bus_busqueda)) { 
            for ($i=0;$i<$n_palabras;$i++){
              $add_search .= "sc.".$res_busqueda[Field]." LIKE '%".$buscar_a[$i]."%' OR ";
            }
          }
          /// para buscar en Marca
          $sql_busqueda = "SHOW COLUMNS FROM marca WHERE Type LIKE '%var%' OR Type LIKE '%text%'";
          $bus_busqueda = mysql_query($sql_busqueda);
          while ($res_busqueda = mysql_fetch_array($bus_busqueda)) { 
            for ($i=0;$i<$n_palabras;$i++){
              $add_search .= "m.".$res_busqueda[Field]." LIKE '%".$buscar_a[$i]."%' OR ";
            }
          } 
          /// para buscar en colores
          $sql_busqueda = "SHOW COLUMNS FROM colores WHERE Type LIKE '%var%' OR Type LIKE '%text%'";
          $bus_busqueda = mysql_query($sql_busqueda);
          while ($res_busqueda = mysql_fetch_array($bus_busqueda)) { 
            for ($i=0;$i<$n_palabras;$i++){
              $add_search .= "cl.".$res_busqueda[Field]." LIKE '%".$buscar_a[$i]."%' OR ";
            }
          } 
          $add_search = 'AND ('.$add_search;
          $add_search .= 'OR';
          $add_search = str_replace('OR OR', ')', $add_search);
          //echo $add_search;
        }
        if ($_POST['id_scate']){
          $add_search .= ' AND sc.id_scate = '.$_POST['id_scate'];
        }
        if ($_POST['id_marca']){
          $add_search .= ' AND m.id_marca = '.$_POST['id_marca'];
        }
        if ($_POST['tallas']){
          $add_search .= ' AND pd.id_talla = '.$_POST['tallas'];
        }
        if ($_POST['colores']){
          $add_search .= ' AND pd.id_color = '.$_POST['colores'];
        }
        ////////////////////////////////////////////////////////////
        $consulta_productos="SELECT * FROM categoria c, sub_categoria sc, productos p, producto_detalles pd, marca m, colores cl where ".$filtro_cate." sc.id_cate=c.id_cate and  p.id_scate=sc.id_scate and pd.id_prod=p.id_prod and pd.cant_prod>0 and p.id_marca = m.id_marca and pd.id_color = cl.id_color ".$add_search." group by p.id_prod ORDER BY ".$_POST['tipo_precio']." p.condi_prod DESC, p.nomb_prod ASC";
        $consulta_productos=mysql_query($consulta_productos);
        echo '<div class="container"><div class="row">';    
        $contador = 0;
        while ($fila=mysql_fetch_array($consulta_productos)) 
        {
          if ($contador == 4){
            echo '</div><div class="row">';
            $contador = 1;
          }else{
            $contador++;
          }

          $consulta_fisico="SELECT sum(cant_prod) as suma_fisico FROM producto_detalles as pd  where pd.id_prod='$fila[id_prod]'";
          $con_fis=mysql_fetch_assoc(mysql_query($consulta_fisico));

          $consulta_disponibilidad="SELECT sum(cant_venta_prod) as suma_prod FROM producto_detalles as pd, venta_productos as vp, venta as v where pd.id_prod='$fila[id_prod]' and vp.id_prod_deta=pd.id_prod_deta and v.id_venta=vp.id_venta and  (v.status_venta='carrito' or v.status_venta='procesado' or v.status_venta='pagado' or v.status_venta='confirmado'   )";
          $con_dis=mysql_fetch_assoc(mysql_query($consulta_disponibilidad));
          $producto_apartado=$con_dis['suma_prod'];

          $producto_inventario=$con_fis['suma_fisico'];

          if ($producto_apartado<$producto_inventario)
            {
                echo '<div class="col-md-3  col-xs-6 text-center" style="padding:1em;">
                        <div class="img-circle img_catalogo_back">
                          <a href="catalogo_exp.php?prod='.$fila["id_prod"].'&categoria='.$categoria.'">';
                            if ($fila['condi_prod']==2){
                              echo '<div class="nuevo_prod">&nbsp;</div>';
                            }
                            echo '<img class="img-circle img_catalogo" src="../imagenes/uploads/productos/'.$fila["imag_prod"].'" title="'.$fila["nomb_prod"].' - '.$fila["nomb_marca"].'">'; 
                          echo '</a>
                        </div>'; 
                        echo '<div>';
                          echo $fila["nomb_prod"].'<br>';
                          $fechaa=date("Y-m-d");
                          if($fila["porc_desc"]>0 and $fila["fechi_desc"]<=$fechaa and $fila["fechf_desc"]>=$fechaa) 
                          {
                              $precion=($fila["prec_prod"]*$fila["porc_desc"])/100;
                              $precion=$fila["prec_prod"]-$precion;
                              echo '<del class="text-danger"><div class="precio_p" style="color:'.$color_fondo.'">&nbsp;Bs. '.number_format($fila["prec_prod"],2,",",".").'&nbsp;</div></del>';
                              echo '<div class="precio_p" style="color:'.$color_fondo.'">Bs. '.number_format($precion,2,",",".").'</div>';
                          }
                          else
                          {
                                 echo '<div class="precio_p" style="color:'.$color_fondo.'">Bs. '.number_format($fila["prec_prod"],2,",",".").'</div>';
                          }
                        echo '</div>';
                echo '</div>'; 
              }
        }
        echo  '</div></div>';
      ?>

      
      <div class="row hidden-xs" role="navigation">
        <?php
           include("menu_footer.php");
           include("footer.php");
        ?>
      </div>
      <div class="row visible-xs">
        <?php  include("footer.php"); ?>
      </div>
    </div>
    <?php if ($usuario_logueado!='') { ?>
    <!-- Modal para perfil -->
    <div class="modal fade" id="perfil" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal_perfil">
              <?php 
                include('perfil.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div>
    <?php } ?>
    <!-- Modal para login -->
    <div class="modal fade" id="login" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal">
              <?php 
                include('login.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div> 
    <script src="../bootstrap/js/bootstrap.min.js"> </script>  
  </body>
</html>