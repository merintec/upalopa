<?php 
session_start();
include("../comunes/conexion.php");
include("../comunes/verificar_logueo.php");
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
$id_user=$_SESSION['id_user'];
$categoria=$_GET['categoria'];
$categoria = '-7';
$logo='../imagenes/sistema/logo.png';
$con[nomb_cate] = 'Lista de Regalo';
$con[desc_cate] = '<b>Tu Tienda UPALOPA<b>';
$color_fondo='#D2C8B0';
include("../comunes/variables.php");
include("../comunes/verificar_usuario_login.php");


$tabla='lista_regalo';
$titulos_busqueda = 'Nombre Celebración,Fecha de la Celebración, Foto';
$datos_busqueda = 'nomb_cele,fech_cele,foto';
$nregistros = 100;  
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <script src="../js/inactividad.js"></script>

      <!-- CALENDARIO-->
          <script>
    $(document).ready(function() {
        $('.datepicker')
            .datepicker({
              format: 'dd-mm-yyyy',
              autoclose: true,
              language: 'es'
            });
    });
    </script>

<!-- validacion en vivo -->
<script >
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
   });
</script>
<!-- funcion de guardar formulario -->  
<?php 

$fecha=date("Y-m-d");
$consula="SELECT * FROM lista_regalo where id_user='$id_user' and fech_cele>'$fecha'";
$con1=mysql_fetch_assoc(mysql_query($consula));


if (!$con1[id_cele])
{
  ?>
      <script type="text/javascript">
      $(function()
      {
          $("#guardar").click(function()
          {
             $("#resultado").html("");
            if ($("#form1").validationEngine('validate')){
              var url="../comunes/funcion_guardar.php"; 
              $.ajax
              ({
                  type: "POST",
                  url: url,
                  data: $("#form1").serialize(),
                  success: function(data)
                  {
                    var codigo, datatemp, mensaje;
                    datatemp=data;
                    datatemp=datatemp.split(":::");
                    codigo=datatemp[0];
                    mensaje=datatemp[1];
                    if (codigo==001)
                    {
                      $('#msg_imag').hide();
                      $("#form1")[0].reset();
                      $('#imagen_cargada').attr('src','');

                    }
                    $("#resultado").html(mensaje);
                    setTimeout(function() {
                      $("#msg_act").fadeOut(450);
                    },3000);
                    
                    buscar();
                  }
              });
              return false;
            }
          });
      });


      </script>
<?php 
}
else
{

?>
    <script type="text/javascript">
      $(function()
      {
          $("#guardar").click(function()
          {
              alert("Error, no se puede crear otra lista de regalo hasta no finalizar la actual");
              return false;
          });

      });
    </script>
<?php 

}

?>

<script type="text/javascript">
function buscar()
{
  var url="../comunes/busqueda_cesta.php"; 
  $.ajax
  ({
    type: "POST",
    url: url,
    data: $("#form1").serialize(),
    success: function(data)
    {
      $("#resultado_busqueda").html(data);
    }
  });
  return false; 
}
</script>

<script type="text/javascript">
 function muestra_oculta(id){
          if (document.getElementById){ //se obtiene el id
              var el = document.getElementById(id); //se define la variable "el" igual a nuestro div
              el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
          
          }


</script>


<script type="text/javascript">
//imagen 1
$(document).ready(function(){
    var parametros = {
            "direc" : "msg_imag"
    };
    var button = $('#upload_button');
    new AjaxUpload('#upload_button', {
        type: "POST", 
        data: parametros,
        action: 'subir_imagen.php',
        onSubmit : function(file , ext){
        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
            // extensiones permitidas
            var mensaje;
            mensaje='<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error... Solo se permiten imagenes</strong></div>';
             $("#resultado").html(mensaje);
            // cancela upload
            return false;
        } else {
            $('#status_imagen').html('Cargando...');
            this.disable();
        }
        },
        onComplete: function(file, response){
            // habilito upload button
            $('#status_imagen').html(response);            
            this.enable();          
            // Agrega archivo a la lista
            var nom_arch=file;
            $('#foto').val(nom_arch);
            $('#imagen_cargada').attr('src','../imagenes/uploads/fotos/'+nom_arch);

            //html('.files').text(file);
        }   
    });
});




</script>

    <title><?php echo $nom_pagina; ?></title>
        
  </head>
  <body class="">    
   <div class="cabecera_procesar">
        <a title="ir a página inicial" href="../index.php"><img id="logo_top" class="logo_top" src="<?php echo $logo; ?>" ></a>
    </div>
    <div class="container-fluid">
      <div class="row">
        <?php include("menu_cesta.php"); ?>
      </div>
      <br>
     <div data-offset-top="100" class="container" data-spy="affix">
  <div id="resultado"></div>
</div>

<div>

    <div class="row">

            <div class="col-md-12 col-xs-12 text-center">

                <img src="../imagenes/acciones/lista_regalos.png" >

            </div>
    </div>

    <br>
    <div class="row">

            <div class="col-md-12 col-xs-12 text-center">

                <p> <b> <h4> Desde Ya puedes crear tu Lista de Regalos para esas ocaciones especiales. </h4> </b> </p>

            </div>


    </div>

    <br>

    <div class="row">

            <div class="col-md-12 col-xs-12 text-center">

              <A href="#marca">  <button type="button" name="crear" id="crear" class="btn btn_form fondo_boton" title="Crear Lista de Regalo"  onclick="muestra_oculta('formulario')">Crear Lista <span class="vineta-default-white"> </span></button> </A>

            </div>


    </div>
    <br>
</div>

            <div class="col-md-12 col-xs-12 text-center">

            <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Para Agregar Productos a tu lista debes ir a catalogo  seleccionar y hacer clic en el boton "Agregar a Lista" y los podras ver en el menú superior en Mi Lista de Regalo. </strong></div>

              

            </div>
    

<div id="formulario" style="display: none;">
<A name="marca"></A>
    <div class="jumbotron cajalogin">
          <div class="titulo_form">   Crear Lista de Regalo </div>
                <form method="POST" name="form1" id="form1" enctype="multipart/form-data" onsubmit="return jQuery(this).validationEngine('validate');">
                    <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
                     <input type="hidden" name="var_titulos_busqueda" id="var_titulos_busqueda" value='<?php echo $titulos_busqueda; ?>'>
                     <input type="hidden" name="var_datos_busqueda" id="var_datos_busqueda" value='<?php echo $datos_busqueda; ?>'>
                    <input type="hidden" name="var_nregistros" id="var_nregistros" value='<?php echo $nregistros; ?>'>
                    <input type="hidden" name="var_pag_actual" id="var_pag_actual" value=''>
                     <br>

                     <div class="form-group">
                        <label for="nomb_cele" class="etq_form" > Nombre:</label>
                        <div id="grupo_nomb_cele" class="">
                            <input type="text" name="nomb_cele" id="nomb_cele" class="validate[required, minSize[3], maxSize[100]] text-input, form-control" placeholder="Nombre Celebración">
                            <span id="boton_nomb_cele" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                        <span id="actualiza_nomb_cele" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                            </span>                  

                        </div>
                     </div>

                      <div class="form-group">
                        <label for="fech_cele" class="etq_form" > Fecha:</label>
                        <div id="grupo_fech_cele" class="">
                            <input type="text" name="fech_cele" id="fech_cele" class="validate[custom[date]] text-input es form-control input-append date datepicker" placeholder="Fecha Celebración">
                            <span id="boton_fech_cele" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                                  <span id="actualiza_fech_cele" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                            </span>           
                        </div>
                      </div>


                        <div class="form-group">
                          <label for="foto" class="etq_form" > Imagen:</label>
                          <div id="grupo_foto" class="input-group">
                                <span id="upload_button" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="";>
                                    <span  class="glyphicon glyphicon-picture"> </span> 
                                </span>
                              <input type="text" name="foto" id="foto" class="form-control"  placeholder="Subir Imagen">
                                 <span id="boton_foto" class="input-group-addon"  style=" visibility:hidden; cursor:pointer; cursor: hand;" onclick="";>
                                     <span id="actualiza_foto" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                                </span>
                          </div>
                          <div id="status_imagen"></div>
                            <div class="text-center">
                                <img id="imagen_cargada" style="max-width:30%; border:1px solid #000;">                    
                             </div>
                        </div>

                   


                       <input type="hidden" name="id_user" id="id_user" value="<?php echo $id_user; ?>">

              
                    <div align="center"><a href="<?php echo $_PHP_SELF; ?>"><button id="cancelar" type="button" class="btn btn_form oculto" >Cancelar</button></a>  <button name="guardar"  id="guardar"  class="btn fondo_boton" > Guardar </button> </div>
               
                </form>
              
               

    </div>
     
</div> 
<br>
<br>
<br>

    <div class="row">
      <div id='resultado_busqueda' class="container">
        <?php 
          include("../comunes/busqueda_cesta.php");
        ?>
      </div>
    </div>


  <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>

