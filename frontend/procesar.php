<?php 
session_start();
include("../comunes/conexion.php");
include("../comunes/verificar_logueo.php");
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
$categoria=$_GET['categoria'];
$categoria = '-7';
$logo='../imagenes/sistema/logo.png';
$con[nomb_cate] = 'Cesta de Compras';
$con[desc_cate] = '<b>Tu Tienda UPALOPA<b>';
$color_fondo='#D2C8B0';
include("../comunes/variables.php");
include("../comunes/verificar_usuario_login.php");
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../js/inactividad.js"></script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <link href="../imagenes/favicon.ico" rel="shortcut icon">
    <title><?php echo $nom_pagina; ?></title>
    <script type="text/javascript">
    function ira_bloqueado(){
      $('#btn_fact').attr('onclick','');
      $('#btn_envi').attr('onclick','');
      $('#btn_resu').attr('onclick','');
      var btn_back = '<button type="button" class="btn btn-default btn-block oculto" style= "margin: auto auto; width: 9em; color:#000; background-color:<?php echo $color_fondo; ?>"><span class="glyphicon glyphicon-chevron-left"></span> Regresar</button>';
      var btn_next = '<button type="button" class="btn btn-default btn-block oculto" style= "margin: auto auto; width: 9em; color:#000; background-color:<?php echo $color_fondo; ?>"> Continuar <span class="glyphicon glyphicon-chevron-right"></span></button> ';
      $('#btn_a').html(btn_back);
      $('#btn_s').html(btn_next);
    }
    function ira_resumen(){
      var url="resumen.php"; 
      $.ajax
      ({
          type: "POST",
          url: url,
          success: function(data)
          {
            $('#ico_cesta').attr('src','../imagenes/sistema/logo_tn_off.png');
            $('#ico_factura').attr('src','../imagenes/sistema/logo_tn_off.png');
            $('#ico_envio').attr('src','../imagenes/sistema/logo_tn_off.png');
            $('#ico_resumen').attr('src','../imagenes/sistema/logo_tn_on.png');
            $("#process_content").html(data);
            var btn_back = '<button type="button" onclick="ira_envio()" class="btn fondo_boton pull-right" style= "width: 9em;"><span class="vineta-back pull-left">&nbsp;</span> Regresar</button>';
            var btn_next = '<button type="button" class="btn btn-default btn-block oculto" style= "margin: auto auto; width: 9em; color:#000; background-color:<?php echo $color_fondo; ?>"> Continuar <span class="glyphicon glyphicon-chevron-right"></span></button> ';
            $('#btn_a').html(btn_back);
            $('#btn_s').html(btn_next);
            $('html,body').animate({ scrollTop: $("#logo_top").offset().top }, 500);
          }
      });
      return false;  
    }
    function ira_envio(){
      var url="direccion_envio.php"; 
      $.ajax
      ({
          type: "POST",
          url: url,
          success: function(data)
          {
            $('#ico_cesta').attr('src','../imagenes/sistema/logo_tn_off.png');
            $('#ico_factura').attr('src','../imagenes/sistema/logo_tn_off.png');
            $('#ico_envio').attr('src','../imagenes/sistema/logo_tn_on.png');
            $('#ico_resumen').attr('src','../imagenes/sistema/logo_tn_off.png');
            $("#process_content").html(data);
            var btn_back = '<button type="button" onclick="ira_facturacion()" class="btn fondo_boton pull-right" style= "width: 9em;"><span class="vineta-back pull-left">&nbsp;</span> Regresar</button>';
            var btn_next = '<button type="button" onclick="ira_resumen()" class="btn fondo_boton pull-left" style= "width: 9em;"><span class="vineta-next">&nbsp;</span> Continuar</button>';
            $('#btn_a').html(btn_back);
            $('#btn_s').html(btn_next);
            $('html,body').animate({ scrollTop: $("#logo_top").offset().top }, 500);
          }
      });
      return false;  
    }
    function ira_facturacion(){
      var url="datos_facturacion.php"; 
      $.ajax
      ({
          type: "POST",
          url: url,
          success: function(data)
          {
            $('#ico_cesta').attr('src','../imagenes/sistema/logo_tn_off.png');
            $('#ico_factura').attr('src','../imagenes/sistema/logo_tn_on.png');
            $('#ico_envio').attr('src','../imagenes/sistema/logo_tn_off.png');
            $('#ico_resumen').attr('src','../imagenes/sistema/logo_tn_off.png');
            $("#process_content").html(data);
            var btn_back = '<button type="button" onclick="ira_cesta()" class="btn fondo_boton pull-right" style= "width: 9em;"><span class="vineta-back pull-left">&nbsp;</span> Regresar</button>';
            var btn_next = '<button type="button" onclick="ira_envio()" class="btn fondo_boton pull-left" style= "width: 9em;"><span class="vineta-next">&nbsp;</span> Continuar</button>';
            $('#btn_a').html(btn_back);
            $('#btn_s').html(btn_next);
            $('html,body').animate({ scrollTop: $("#logo_top").offset().top }, 500);
          }
      });
      return false;
    }
    function ira_cesta(){
      var url="cesta.php"; 
      $.ajax
      ({
          type: "POST",
          url: url,
          success: function(data)
          {
            $('#ico_cesta').attr('src','../imagenes/sistema/logo_tn_on.png');
            $('#ico_factura').attr('src','../imagenes/sistema/logo_tn_off.png');
            $('#ico_envio').attr('src','../imagenes/sistema/logo_tn_off.png');
            $('#ico_resumen').attr('src','../imagenes/sistema/logo_tn_off.png');           
            var btn_back = '<button type="button" class="btn btn-default btn-block oculto" style= "margin: auto auto; width: 9em; color:#000; background-color:<?php echo $color_fondo; ?>"><span class="glyphicon glyphicon-chevron-left"></span> Regresar</button>';
            var btn_next = '<button type="button" onclick="ira_facturacion()" class="btn fondo_boton pull-left" style= "width: 9em;"><span class="vineta-next">&nbsp;</span> Continuar</button>';
            $('#btn_a').html(btn_back);
            $('#btn_s').html(btn_next);
            $("#process_content").html(data); 
            $('html,body').animate({ scrollTop: $("#logo_top").offset().top }, 500);
          }
      });
      return false;  
    }
    </script>
  </head>
  <body class="">    
    <div class="cabecera_procesar">
        <a title="ir a página inicial" href="../index.php"><img id="logo_top" class="logo_top" src="<?php echo $logo; ?>" ></a>
    </div>
    <div class="container-fluid">
      <div class="row">
        <?php include("menu_cesta.php"); ?>
      </div>
      <div class="container">
 
        <?php include("pasos.php"); ?>

      </div>
      <div class="container text-center" style=" font-size: 1.5em;  color:#857c7c;" > Complete todos los datos y sigue los pasos para procesar su compra </div>
      <br>


      <div data-offset-top="280" class="container" data-spy="affix">
        <div id="resultado"></div>
      </div>
      <br>
      <div id="process_content">
          <script>ira_cesta();</script>
      </div>
      <br>
      <input type="hidden" name="id_venta2" id="id_venta2">
      <div class="row">
        <div class="col-md-6 col-xs-6" id="btn_a">&nbsp;</div>
        <div class="col-md-6 col-xs-6" id="btn_s">&nbsp;</div>
      </div>
      <br>
      <div class="row hidden-xs" role="navigation">
        <?php
           include("menu_footer.php");
           include("footer.php");
        ?>
      </div>
      <div class="row visible-xs">
        <?php  include("footer.php"); ?>
      </div>
    </div>
    <!-- Modal para perfil -->
    <div class="modal fade" id="perfil" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal_perfil">
              <?php 
                include('perfil.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div>
    <!-- Modal para login -->
    <div class="modal fade" id="login" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal">
              <?php 
                include('login.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div> 
    <script src="../bootstrap/js/bootstrap.min.js"> </script>  
  </body>
</html>