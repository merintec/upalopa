<?php 
session_start();
include("../comunes/conexion.php");
include("../comunes/verificar_logueo.php");
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
$id_user=$_SESSION['id_user'];
$categoria=$_GET['categoria'];
$categoria = '-7';
$logo='../imagenes/sistema/logo.png';
$con[nomb_cate] = 'Cupones';
$con[desc_cate] = '<b>Tu Tienda UPALOPA<b>';
$color_fondo='#D2C8B0';
include("../comunes/variables.php");
include("../comunes/verificar_usuario_login.php");
$consulta_usuario="SELECT * FROM usuarios WHERE id_user='$id_user'";
$con_user=mysql_fetch_assoc(mysql_query($consulta_usuario));
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <script src="../js/inactividad.js"></script>

    <script>
    $(document).ready(function() {
        // bootstrap-datepicker
        $('.datepicker').datepicker({
          format: "dd-mm-yyyy",
          language: "es",
          autoclose: true,
          todayBtn: true
        }).on(
          'show', function() {      
          // Obtener valores actuales z-index de cada elemento
          var zIndexModal = $('#modal_pago').css('z-i
            ndex');
          var zIndexFecha = $('.datepicker').css('z-index');
         
                // alert(zIndexModal + zIndexFEcha);
         
                // Re asignamos el valor z-index para mostrar sobre la ventana modal
                $('.datepicker').css('z-index',zIndexModal+1);
        });
    });
    </script>
    <script>
      function pasar_modal(id_gif)
      {
        var parametros = {
          "id_gif" : id_gif
        }
        var url="regalar_giftcard.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: parametros,
            beforeSend: function(){
            },
            success: function(data)
            {
              $('#contenido_regalo').html(data);
              $("#id_gif").val(id_gif);
            }
        });
        return false;
      }
    </script>
    <title><?php echo $nom_pagina; ?></title>
        
  </head>
  <body class="">    
    <div style="background-color:<?php echo $color_fondo; ?>;" class="cabecera_categorias">
        <a title="ir a página inicial" href="../index.php"><img id="logo_top" class="logo_top" src="<?php echo $logo; ?>" ></a>
    </div>
    <div class="container-fluid">
      <div class="row">
        <?php include("menu_cesta.php"); ?>
      </div>
      <br>
      <div data-offset-top="280" class="container" data-spy="affix">
        <div id="resultado"></div>
      </div>

      <br>
      <div class="text-center"> <span class="text-info"><h3> <b> GiftCard Disponibles </b>  </h3></span> </div>
    <div class="container" >

        <?php    

        $consulta="SELECT * FROM giftcard as g where g.mail_gif='$con_user[corre_user]'";;
        $consulta=mysql_query($consulta);

        $i=0;
        echo '<div class="table-responsive procesar_linea_punteada">';
        echo '<table class="table table-striped table-hover">';
        echo '<tr> <th class="fondo_predefinido"># </th> <th class="fondo_predefinido" >N° de Gift </th> <th class="fondo_predefinido"> Fecha de Asignación </th> <th class="fondo_predefinido"> Monto de la Tarjeta </th> <th class="fondo_predefinido"> Monto Disponible </th> <th class="fondo_predefinido"> Motivo</th> <th class="fondo_predefinido"> Status</th><th class="fondo_predefinido">Regalar</th> </tr>';

        while($fila=mysql_fetch_array($consulta))
        {
          $i++;


            $sql_gift = "SELECT *, (SELECT COALESCE(SUM(mont_pago),0) as pagado FROM pago WHERE nomb_per_pago='".$fila[mail_gif]."' AND num_refe_banc = '".$fila[numb_gif]."') AS pagado FROM giftcard g WHERE g.numb_gif = '".$fila[numb_gif]."' AND g.mail_gif = '".$fila[mail_gif]."'";
            $res_gift = mysql_query($sql_gift);
            $cuantos=mysql_num_rows($res_gift);
            $res_gift= mysql_fetch_array($res_gift);
            $disp = $res_gift[mont_gif] - $res_gift[pagado];

            /*
              if ($fila[fech_asig_gift]=='0000-00-00')
            {


                $icono= '<button style="width:13em; background-color: #00b6ce; border: 0px;" type="button" class="btn btn-warning" title="Enviado" > <span class="pull-center"> Comprado</span> </button> ';
                echo '<tr> <td>'.$i.' </td> <td>'.$fila[numb_gif].'</td> <td>'.$fila[fech_asig_gift].'</td> <td>'.$fila[mont_gif].'</td> <td>'.$disp.'</td> <td>'.$fila[msg_gif].'</td> <td class="text-center">'.$icono.'</button></td> </tr> ';
   
            }
              */
            if (($fila[fech_asig_gift]!='0000-00-00' and $disp==$fila[mont_gif]))
            {
              $icono= '<button style="width:7em; background-color: #ae4f9e; border: 0px;" type="button" class="btn btn-info" title="Utilizado" ><span class="pull-center"> Disponible</span> </button> ';
              echo '<tr> <td>'.$i.' </td> <td>'.$fila[numb_gif].'</td> <td>'.date('d-m-Y',strtotime($fila[fech_asig_gift])).'</td> <td>'.$fila[mont_gif].'</td> <td>'.$disp.'</td> <td>'.$fila[msg_gif].'</td> <td class="text-center">'.$icono.'</button></td><td style="text-align: center;"><img height="30px" src="../imagenes/acciones/regalar.png" data-toggle="modal" data-target="#modal_regalo" onclick="pasar_modal('.$fila[id_gif].');" title="Utilizar GiftCard" style="cursor:pointer;"></td></tr> ';
            }
             if (($fila[fech_asig_gift]!='0000-00-00' and $disp<$fila[mont_gif] and $disp!=0)) 
             {
                  $icono= '<button style="width:7em; background-color: #ae4f9e; border: 0px;" type="button" class="btn btn-primary" title="Utilizado" ><span class="pull-center"> Utilizando</span> </button> ';
                  echo '<tr> <td>'.$i.' </td> <td>'.$fila[numb_gif].'</td> <td>'.date('d-m-Y',strtotime($fila[fech_asig_gift])).'</td> <td>'.$fila[mont_gif].'</td> <td>'.$disp.'</td> <td>'.$fila[msg_gif].'</td> <td class="text-center">'.$icono.'</button></td><td style="text-align: center;"><img height="30px" src="../imagenes/acciones/regalar.png" data-toggle="modal" data-target="#modal_regalo" onclick="pasar_modal('.$fila[id_gif].');" title="Utilizar GiftCard" style="cursor:pointer;"></td></tr> ';
             }
             if (($fila[fech_asig_gift]!='0000-00-00' and $disp==0))
             {
                 $icono= '<button style="width:7em; background-color: #3e658b; border: 1px;" type="button" class="btn btn-success" title="Vencido" > <span class="pull-center">Usado</span></button>';
                 echo '<tr> <td>'.$i.' </td> <td>'.$fila[numb_gif].'</td> <td>'.date('d-m-Y',strtotime($fila[fech_asig_gift])).'</td> <td>'.$fila[mont_gif].'</td> <td>'.$disp.'</td> <td>'.$fila[msg_gif].'</td> <td class="text-center">'.$icono.'</button></td> </tr> ';
             }           
        }
        echo "</table>"; 
        echo "</div>";
       ?>
       </div>
      <br>


      <div class="row hidden-xs" role="navigation">
        <?php
           include("menu_footer.php");
           include("footer.php");
        ?>
      </div>
      <div class="row visible-xs">
        <?php  include("footer.php"); ?>
      </div>
    </div>
    <!-- Modal para login -->
    <div class="modal fade" id="login" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 350px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal">
              <?php 
                include('login.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div> 
      </div>    
    <!-- Modal para perfil -->
    <div class="modal fade" id="perfil" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal_perfil">
              <?php 
                include('perfil.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div>
    <!-- Modal para Reenviar tarjeta -->
    <div class="modal fade" id="modal_regalo" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_regalo">

            </div>
          </div>
        </div>  
      </div>    
    </div> 
    <script src="../bootstrap/js/bootstrap.min.js"> </script>  
  </body>
</html>