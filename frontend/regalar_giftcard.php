<?php
session_start();
include_once("../comunes/variables.php");
include_once("../comunes/conexion.php");
$tabla='pago';
?>
    <script>
    $(document).ready(function() {
        // bootstrap-datepicker
        $('.datepicker').datepicker({
          format: "dd-mm-yyyy",
          language: "es",
          autoclose: true
        }).on(
          'show', function() {      
          // Obtener valores actuales z-index de cada elemento
          var zIndexModal = $('#modal_pago').css('z-index');
          var zIndexFecha = $('.datepicker').css('z-index');
                // alert(zIndexModal + zIndexFEcha);
         
                // Re asignamos el valor z-index para mostrar sobre la ventana modal
                $('.datepicker').css('z-index',zIndexModal+1);
        });
        $('.datepicker').datepicker("setDate", new Date(<?php echo date('Y');?>,<?php echo date('m');?>,<?php echo date('d');?>) );
    });
    </script>
<?php 
  $sql_mpago = "SELECT * FROM metodo_pago WHERE nomb_mpago LIKE '%Cupón%'";
  $mpago = mysql_fetch_array(mysql_query($sql_mpago));
?>
<input type="hidden" name="es_cupon"  id="es_cupon" value="<?php echo $mpago[id_mpago]; ?>" >
<script>
  function transfiere_giftcard()
  {
      if ($("#form20").validationEngine('validate')){
              var url="transfiere_giftcard.php"; 
              $.ajax
              ({
                  type: "POST",
                  url: url,
                  data: $("#form20").serialize(),
                  beforeSend: function () 
                  {
                    $('#etiqueta_btn').html("<img valign='middle' src='../imagenes/acciones/cargando_gray.gif' width='30px'> Procesando...");
                  },
                  success: function(data)
                  {
                    var codigo, datatemp, mensaje;
                    datatemp=data;
                    datatemp=datatemp.split(":::");
                    codigo=datatemp[0];
                    mensaje=datatemp[1];
                    if (codigo==001)
                    {
                      setTimeout(function() {
                        window.location='giftcard.php';
                      },3000);
                    }
                    $("#form20")[0].reset();
                    $("#resultado_transf").html(mensaje);
                    $('#etiqueta_btn').html("Procesado");
                  }
              });
              return false;
            }
  }
function enviar_correo()
{
          var correo=$("#var_correo").val();
          var nom_ape_user=$('#var_nom_ape').val();
          var orden=$('#var_orden').val();
          var monto=$('#mont_pago').val();
          var url="../comunes/enviar_correo.php";
          var sex;
          if ($('#sex_user').val()=='F'){
            sex = 'a';
          }
          else {
            sex = 'o';  
          }  
          var mensaje = "Estimad" + sex + " " + nom_ape_user + ", <p style='padding-top: 20px;'>Hemos registrado un pago para<br>la orden de compra N° <b>" + orden + "</b> por Bs. <b>" + monto + "</b>  en las próximas 48 horas será verificado y se le notificará para el envío del producto. </p><p style='padding-top: 20px;'>Gracias por preferirnos <a href='http://upalopa.com'>Upalopa.com</a></p>";
          //var mensaje = "Estimad" + sex + " " + nom_ape_user + ", <p style='padding-top: 20px;'>!Su pago ha sido registrado exitosamente para la orden de compra N° " + orden + "! <p style='padding-top: 20px;'>Para mayor detalle consulte la sección <b>Tus Pagos</b> en nuestro sitio <a href='http://upalopa.com'>www.upalopa.com</a></p>",
          var parametros = {
              "mensaje" : mensaje,
              "destino" : correo, 
              "correo_origen" : "<?php echo $correo_compras; ?>",
              "titulo" : "Pago Registrado Upalopa"
            };
          $.ajax
          ({
              type: "POST",
              url: url,
              data: parametros,
              success: function(data)
              {

                 
              }
          });
}
</script>
<!DOCTYPE html>
  <?php 
    $sql_gif = "SELECT * FROM giftcard WHERE id_gif = ".$_POST[id_gif];
    $res_gif = mysql_fetch_array(mysql_query($sql_gif));
  ?>
<div class="ventana-titulo">
  <div style="margin-left: -0.4em; margin-top: -0.6em;">
    <img height="80px" src="../imagenes/acciones/regalar_white.png" style="float: left;">
  </div>
  <div style="font-size: 0.9em; padding-left: 0.5em; padding-top: 0.8em; float: left; ">
    Transfiere tu GiftCard <br>N° <span id="num_gif"><?php echo $res_gif[numb_gif]; ?></span>
  </div>
</div>
<div class="borde-ventana-punteada">
<button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -4.7em; margin-right: -5px;">×</button>
<html lang="es">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">   
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <!-- validacion en vivo -->
<script >
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form20").validationEngine('attach', {bindMethod:"live"});
   });
</script>

</head>
<body>
  <div class="" style="padding-top: 0.3em;  margin:0px; margin-left:auto; margin-right: auto;">
     <br><br><br><br><br>
      <form method="POST" name="form20" id="form20" onsubmit="return jQuery(this).validationEngine('validate');">
          <div id="resultado_transf"></div>
          <div class="row">
            <div class="col-md-2 hidden-xs text-right">
            </div>
            <div class="col-md-8 col-xs-12 text-right">
              <input type="hidden" name="numb_gif" id="numb_gif" value="<?php echo $res_gif[numb_gif]; ?>">
              <input type="text" name="nomb_gif" id="nomb_gif" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[50]] text-input form-control fondo_campo" placeholder="¿Quién Recibe?">
              <input type="text" name="mail_gif" id="mail_gif" class="validate[required, custom[email], minSize[1], maxSize[50]] text-input form-control fondo_campo" placeholder="Correo de quien recibe" style="margin-top: 0.5em;">
              <input type="text" name="msg_gif" id="msg_gif" class="validate[required, minSize[3], maxSize[140]] text-input form-control fondo_campo" placeholder="Mensaje que deseas incluir..." style="margin-top: 0.5em;">
            </div>
            <div class="col-md-2 hidden-xs text-right">
            </div>
          </div>

          <div id="resultado_giftcard" style="padding-bottom: 15px; text-align: center; font-weight: bold; color: #c8bfa9;"></div>
        <center><button id="ingresar" type="button" class="btn fondo_boton" onclick="transfiere_giftcard()"><div class="vineta-blanco">&nbsp;</div> <span id="etiqueta_btn" style="color: #FFFFFF;">Enviar</span></button></center>
      </form>
  </div>
</body>
</html>
</div>