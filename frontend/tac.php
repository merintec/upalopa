<div class="row hidden-xs" style="margin-left: 3em; margin-bottom: 2em;">
	<div class="col-md-2 col-xs-1">
		<a title="ir a página inicial" href="../index.php"><img class="logo_top" src="../imagenes/sistema/logo_tn_on.png" width="80em" height="80em"  ></a>		
	</div>
	<div class="col-md-8 col-xs-10" style="padding-top: 4em;">
		<div class="titulo_categoria text-info" style="margin-left: 0.5em;">Términos y Condiciones</div>
	</div>
</div>
<div class="row visible-xs" style="margin-bottom: 1em; ">
	<div class="col-xs-3">
		<a title="ir a página inicial" href="../index.php"><img class="logo_top" src="../imagenes/sistema/logo_tn_on.png" style="width: 100%"></a>		
	</div>
	<div class="col-xs-8">
		<div class="titulo_categoria text-info" style="margin-left: 0.2em;">Términos y Condiciones</div>
	</div>
</div>
<div class="row" style="margin-bottom: 2em;">
	<div class="col-md-1 col-xs-1">&nbsp;</div>
	<div class="col-md-7 col-xs-10 texto-contenido text-justificado" style="text-align: justify;">
		<div class="pregunta">CONDICIONES DE USO</div>
			<div class="respuesta"><a href="../index.php">www.upalopa.com</a> es una tienda virtual dedicada a la moda infantil, poniendo a la disposición de los usuarios información concreta y detallada de todos los productos.  Las siguientes Condiciones de Uso regularán el acceso y la utilización del servicio alojado con el dominio <a href="../index.php">www.upalopa.com</a> así como de todos los contenidos que se muestran en el mismo o se pongan a disposición de los usuarios.<br><br>En el momento que acceden en el sitio web manifiestas que has leído y que aceptas cumplir con estas condiciones de uso. Si el Usuario no está de acuerdo con los Términos y Condiciones, debe abstenerse de utilizar el Sitio y/o los Servicios. <a href="../index.php">www.upalopa.com</a> podrá negar o terminar la prestación de los Servicios a quienes no posean capacidad para contratar y a quienes no hayan convenido en la anterior declaración.</div>
		<div class="pregunta">CONDICIONES GENERALES</div>
			<div class="respuesta">
				<a href="../index.php">www.upalopa.com</a> es un sitio web que ofrece servicios de venta, única y exclusivamente por internet, no existe tienda física donde se puedan retirar los productos o hacer reclamos en relación al servicio del mismo. La misión de <a href="../index.php">www.upalopa.com</a> es hacer de su compra una experiencia placentera y  segura, por lo que los usuarios podrán disfrutar del servicio de compra en el sitio <a href="../index.php">www.upalopa.com</a> las 24 horas del día, los 7 días de la semana.<br><br>
				<a href="../index.php">www.upalopa.com</a>   por ser una tienda en línea, tiene a su disposición un Departamento Especializado de Servicio de Atención al Cliente, mediante la cual se canalizará cualquier duda o inconveniente por el correo <a href="mailto:comprasenlinea@upalopa.com">comprasenlinea@upalopa.com</a> se encuentra activo de lunes a viernes, de 8:00 am a 8:00 pm y los sábados de 8:00 am a 5:00 pm, estando inactivo domingos y feriados. <br><br>
				Los Administradores se reservan el derecho de modificar, eliminar, sustituir o ingresar, contenido en el sitio,  herramientas en la página o los productos ofrecidos, así como modificación en el precio establecido para los productos, antes de que el usuario oferte por el mismo, todo lo anterior sin previo aviso.<br><br>
				Las promociones y ofertas acordadas y anunciadas por <a href="../index.php">www.upalopa.com</a> , serán respetadas en los términos y condiciones en que fueron publicadas y por el tiempo en que las mismas se encuentren vigentes.<br><br>
				<a href="../index.php">www.upalopa.com</a>   se encuentra hospedado en una de las plataformas líderes en rendimiento, seguridad  y confiabilidad para poder garantizar la confidencialidad y seguridad de dicha información. Sin embargo <a href="../index.php">www.upalopa.com</a>  no se hace responsable por cualquier violación o usurpación que pueda sufrir el sistema de seguridad del sitio, por terceras personas, fallas en el sistema, en el servidor, en Internet, o cualquier  virus que pudiese alojarse en el sistema. Asimismo,  tampoco se hace responsable por la obtención por medios ilegales o violaciones del sistema de la información alojada en nuestro servidor, así como del uso que se haga de la misma.<br><br>
				<a href="../index.php">www.upalopa.com</a>   intentará garantizar el uso ininterrumpido y continuado del servicio, sin embargo las consecuencias que se generen con ocasión de fallas en el sistema del mismo o dificultades técnicas u otra razón ajena a la voluntad de <a href="../index.php">www.upalopa.com</a> , no serán imputables a <a href="../index.php">www.upalopa.com</a>.<br><br>
				Los Términos y Condiciones aquí expresados, podrán ser modificados por <a href="../index.php">www.upalopa.com</a>   en cualquier momento, y empezaran a entrar en vigencia desde el momento de su publicación en la página de <a href="../index.php">www.upalopa.com</a> con lo cual se entenderán aceptados por el Usuario.
			</div>
		<div class="pregunta">REGISTRO DEL CLIENTE</div>
		<div class="respuesta">
			En <a href="../index.php">www.upalopa.com</a> el registro del Cliente es una condición indispensable, por lo que éste debe registrar en la página sus datos básicos incluyendo información personal como el número de cédula de identidad y correo electrónico.<br><br>
			El Cliente al ingresar sus datos declara bajo fe de juramento que los mismos corresponden a información veraz, vigente y verificable.<br><br>
			 Asimismo al momento en que el Cliente decida adquirir cualquier artículo mediante el medio de pago de su preferencia, será necesario que provea información financiera específica, tal como nombre del banco de donde fue realizada la transferencia o depósito bancario y número que los distingue, o de ser el caso los datos correspondientes a la tarjeta de crédito con la que desea realizar el pago, dirección en la que recibe las facturas, teléfono y dirección a la que desea que se envíe sus productos.<br><br>
			Bajo ningún concepto <a href="../index.php">www.upalopa.com</a> podrá vender, alquilar ni traspasar a otras empresas la Información personal que cada Usuario ha proporcionado para crear su cuenta en la página, salvo en los supuestos establecidos en los Términos y Condiciones aquí expuestos.<br><br>
			No será responsabilidad <a href="../index.php">www.upalopa.com</a> el uso indebido y no autorizado de un Usuario o un tercero, de la información suministrada en la página.  <a href="../index.php">www.upalopa.com</a> reitera que hará todos los esfuerzos físicos y tecnológicos para mantener la información suministrada de manera segura y confidencial.<br><br>
			Así mismo, <a href="../index.php">www.upalopa.com</a> tampoco será responsable de la información que los Usuarios divulguen a terceros por redes sociales, foros o cualquier otro medio.
		</div>
		<div class="pregunta">COMUNICACIONES DE PROMOCIONES</div>
		<div class="respuesta">
			Los Usuarios pueden autorizar en su cuenta <a href="../index.php">www.upalopa.com</a>, para enviarles promociones al correo electrónico registrado en su Perfil. El Cliente podrá solicitar la suspensión de toda comunicación promocional o publicitaria enviada a su correo electrónico en su Perfil, desactivando la opción destinada para tal fin.
		</div>
		<div class="pregunta">CONDICIONES DE LA COMPRA</div>
		<div class="respuesta">
			Para que el Usuario pueda realizar y confirmar una compra en <a href="../index.php">www.upalopa.com</a> es necesario e indispensable que acepte los Términos y  Condiciones establecidos en este texto, así como se informe de los  medios de pagos disponibles y las entidades bancarias aceptadas para la realización del pago, los cuales se encuentran debidamente especificados en Upalopa.com.<br><br>
			Los procesos de recepción del pago, son ajenos a <a href="../index.php">www.upalopa.com</a>  y son gestionados por las entidades bancarias, por lo que Upalopa.com  no se hace responsable por las fallas de comunicación que ellas presenten, ni por las consecuencias que se desprendan de las actuaciones de las mismas. Así mismo, no se hace responsable por los daños o perjuicios que se originen con la manipulación hecha por terceros, que accedan a la red de manera no autorizada, pese a las herramientas de prevención implementadas en la página web.<br><br>
			La compra será aceptada al momento que el Usuario haya hecho el pedido y oprimido el botón de “PROCESAR COMPRA” y será procesada únicamente al momento en que sea confirmado el pago hecho por el Usuario.<br><br>
			La validez del pedido es de tres días hábiles, entre la confirmación del pedido y la realización efectiva del pago. Después de transcurrido este tiempo, sin haber sido confirmado el pago, la compra será cancelada automáticamente por el sistema y el Usuario deberá realizar de nuevo el pedido. <br><br>
			<a href="../index.php">www.upalopa.com</a>  no se hace responsable por el cambio de inventario y de precios de los productos que pueda existir entre la cancelación del pedido y la realización  de la nueva orden por parte del Usuario.<br><br>
			Todo impuesto o tasa que graven o recaigan sobre los productos sometidos a venta, así como los que recaigan  sobre las operaciones de venta, tales como el Impuesto al Valor Agregado (IVA), serán por única y exclusiva cuenta del cliente o usuario, los cuales serán pagados al momento en que éste realice el pedido en el sitio web, y será reflejado en la factura definitiva que le sea entregada al cliente o usuario en el momento de entrega del producto. <br><br>
			<a href="../index.php">www.upalopa.com</a>   es una tienda en línea y los envíos de los productos se gestionan por medio de empresas de envío (ZOOM, Domesa, MRW entre otros); <br><br>
			<a href="../index.php">www.upalopa.com</a>  se reserva el derecho de cancelar cualquier orden que no apruebe los filtros de seguridad requeridos para la aprobación de las mismas.
			</div>
		<div class="pregunta">CONDICIONES DE LA ENTREGA</div>
		<div class="respuesta">
			El envío de los productos se procesará una vez confirmado el pago. Los productos serán enviados única y exclusivamente mediante empresas de envío, el tiempo de entrega será de un tiempo aproximado de 2 a 7 días hábiles.<br><br>
			El cliente debe proporcionar al momento de la compra, la dirección donde quiere que sea enviado su producto, suministrando toda la información que el sistema solicite, por lo <a href="../index.php">www.upalopa.com</a>  no se hace responsable por la validez o certeza de la información suministrada por el Usuario.<br><br>
			Los sábados, domingos y feriados, no cuentan dentro del tiempo de entrega ni son días de despacho.
		</div>
		<div class="pregunta">GARANTÍA Y DEVOLUCIONES</div>
		<div class="respuesta"> 
			Si el producto adquirido en <a href="../index.php">www.upalopa.com</a>  llegase a presentar defectos o desperfectos técnicos, el Usuario podrá notificarlos al correo <a href="mailto:comprasenlinea@upalopa.com">comprasenlinea@upalopa.com</a> <br><br>
			No se aceptarán devoluciones ni cambios en los casos de trajes de baño, prendas íntimas, artículos de uso personal, y cualquier prenda que no cumpla con las especificaciones aquí expuestas.<br><br>
			El plazo para solicitar una devolución es de 5 días contados a partir de la fecha de recepción del envío. <br><br>
			El producto debe estar sin uso, con etiquetas, en perfectas condiciones y en su empaque original. El usuario debe pagar por la devolución de su compra (costo de envío) a menos que sea no sea este el responsable del deterioro de una de sus prendas o de un mal embalaje que dañara su compra.<br><br>
			En caso de que la devolución sea por que el producto presenta deficiencias de fabricación, elaboración y/o materiales, el plazo para realizar la solicitud  de cambio es de 48 horas desde el momento que se realizó la recepción del producto.<br><br>
			Para solicitar la devolución de un producto se debe enviar un correo a <a href="mailto:comprasenlinea@upalopa.com">comprasenlinea@upalopa.com</a> <br><br>
			Después de recibir respuesta a la solicitud de devolución y de haber sido aprobada, se debe devolver el producto dentro de los siguientes 3 días continuos después de haber recibido respuesta. <br><br>
			Vale destacar que productos que se reciban después de esos 3 días continuos no serán aceptados.<br><br>
			Si has recibido un producto por error, por favor envíanos un correo a <a href="mailto:comprasenlinea@upalopa.com">comprasenlinea@upalopa.com</a> 
		 </div>
	</div>
	<div class="col-md-1 col-xs-1">&nbsp;</div>
</div>
<br>
<br>
<br>
<br>
<br>