<?php 
session_start();
include("../comunes/conexion.php");
include("../comunes/verificar_logueo.php");
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
$id_user=$_SESSION['id_user'];
$categoria=$_GET['categoria'];
$categoria = '-7';
$logo='../imagenes/sistema/logo.png';
$con[nomb_cate] = 'Cupones';
$con[desc_cate] = '<b>Tu Tienda UPALOPA<b>';
$color_fondo='#D2C8B0';
include("../comunes/variables.php");
include("../comunes/verificar_usuario_login.php");
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <script src="../js/inactividad.js"></script>

    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
        <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="../js/calendario/datepicker.min.css" />
    <link rel="stylesheet" href="../js/calendario/datepicker3.min.css" />
    <script src="../js/calendario/bootstrap-datepicker.min.js"></script>
    <script src="../js/calendario/bootstrap-datepicker.es.js" charset="UTF-8"></script>
    <title><?php echo $nom_pagina; ?></title>
        
  </head>
  <body class="">    
    <div style="background-color:<?php echo $color_fondo; ?>;" class="cabecera_categorias">
        <a title="ir a página inicial" href="../index.php"><img id="logo_top" class="logo_top" src="<?php echo $logo; ?>" ></a>
    </div>
    <div class="container-fluid">
      <div class="row">
        <?php include("menu_cesta.php"); ?>
      </div>
      <br>
      <div data-offset-top="280" class="container" data-spy="affix">
        <div id="resultado"></div>
      </div>
      <br>
      <div class="text-center"> <span class="text-info"><h3> <b> Cupones de Descuentos </b>  </h3></span> </div>
    <div class="container" >

        <?php    

        $consulta="SELECT * FROM cupones_descuentos as cd  where cd.id_user='$id_user' and cd.stat_cupon='enviado' ";;
        $consulta=mysql_query($consulta);
        $i=0;
        echo '<div class="table-responsive procesar_linea_punteada">';
        echo '<table class="table table-striped table-hover">';
        echo '<tr> <th class="fondo_predefinido"># </th> <th class="fondo_predefinido" >N° de Cupón </th> <th class="fondo_predefinido"> Fecha de Asignación </th> <th class="fondo_predefinido"> Fecha de Vencimiento </th> <th class="fondo_predefinido"> Monto del Cupón </th></tr>';
        while($fila=mysql_fetch_array($consulta))
        {
          $i++;
            
             $nueva = explode('-', $fila[fstat_cupon]);
              if (strlen($fila[fstat_cupon]) == 10 && strlen($nueva[0]) == 4 && strlen($nueva[1]) == 2 && strlen($nueva[2]) == 2){
                 $fecha1 = $nueva[2].'-'.$nueva[1].'-'.$nueva[0];
                }

             $nueva2 = explode('-', $fila[ffin_cupon]);
              if (strlen($fila[ffin_cupon]) == 10 && strlen($nueva2[0]) == 4 && strlen($nueva2[1]) == 2 && strlen($nueva2[2]) == 2){
                  $fecha2 = $nueva2[2].'-'.$nueva2[1].'-'.$nueva2[0];
                }
          
          echo '<tr><td>'.$i.'</td><td>'.$fila[num_cupon].'</td><td>'.$fecha1.'</td><td>'.$fecha2.'</td><td>'.$fila[valor_cupon].'Bs.</td> </tr>';

        }
        if ($i==0){
          echo '<tr class="text-center"><td colspan="5"><b>Actualmente no tienes Cupones de Descuento Disponibles</b></td></tr>';
        } 

         echo "</table>"; 
         echo "</div>";
       ?>
       </div>
      <br>


      <div class="row hidden-xs" role="navigation">
        <?php
           include("menu_footer.php");
           include("footer.php");
        ?>
      </div>
      <div class="row visible-xs">
        <?php  include("footer.php"); ?>
      </div>
    </div>
    <!-- Modal para login -->
    <div class="modal fade" id="login" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 350px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal">
              <?php 
                include('login.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div> 
      </div>    
    <!-- Modal para perfil -->
    <div class="modal fade" id="perfil" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal_perfil">
              <?php 
                include('perfil.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div>

      <!-- Modal para Registrar Pago -->
    <div class="modal fade" id="modal_pago" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 350px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_pago">
           
              <?php 
                include('registro_pago.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div> 
    <script src="../bootstrap/js/bootstrap.min.js"> </script>  
  </body>
</html>