<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>
<?php session_start(); ?>
<?php
include("comunes/variables.php");
$add_url = '';
$categoria = '';
$page = '';
$color_fondo = '';
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $page_descripcion; ?>" />
    <meta name="keywords" content="<?php echo $page_keywords; ?>" />
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
    <script src="bootstrap/js/jquery.js"> </script>
    <script src="validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="validacion/css/template.css" type="text/css"/>
    <link href="imagenes/favicon.ico" rel="shortcut icon">
    <title><?php echo $nom_pagina; ?></title>
  </head>

<script type="text/javascript">
         $(document).ready(function()
          {
                   $('#imagenes').modal('show');
               
          });
</script>
  <body class="fondo_inicial">
    <div class="container-fluid">
      <div class="row">
        <?php include("frontend/menu_frontend.php"); ?>
      </div>

      <div class="row" style="margin-top: -20px;">
        <?php  include("frontend/carrusel.php"); ?>    
      </div>
      <div class="navbar navbar-fixed-bottom hidden-xs" role="navigation">
        <?php
           include("frontend/menu_footer.php");
           include("frontend/footer.php");
        ?>
      </div>
      <div class="row visible-xs">
        <?php  include("frontend/footer.php"); ?>
      </div>
    </div>
    <!-- Modal para perfil -->
    <?php if ($_SESSION) { if ($_SESSION['id_user']) { ?>
    <div class="modal fade" id="perfil" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" name="origen" id="origen" value="<?php echo $_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']; ?>">
            <div id="contenido_modal_perfil">
              <?php 
                include('perfil.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div>
    <?php } } ?>
    <!-- Modal para login -->
    <div class="modal fade" id="login" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    	<div class="modal-dialog" style="width: 400px">
    		<div class="modal-content">
    			<div class="modal-body">
            <div id="contenido_modal">
      				<?php 
      					include('login.php'); 
      				?>
            </div>
    			</div>
    		</div>  
    	</div>    
    </div>

    <div class="modal fade" id="imagenes" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog promo">
        <div class="modal-content">
          <div class="modal-body">
            <div id="contenido_modal">
              <?php 
                include('frontend/promociones.php'); 
              ?>
            </div>
          </div>
        </div>  
      </div>    
    </div>


    <script src="bootstrap/js/bootstrap.min.js"> </script>  
  </body>
</html>