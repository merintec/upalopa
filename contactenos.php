<?php 
session_start();
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
?>
<?php
include("comunes/variables.php");
?>

<script type="text/javascript">
 function enviar_correo()
{
         $("#resultado1").html("");
          if ($("#contacto").validationEngine('validate')){

                      var url="comunes/enviar_correo.php"; 
                      var correo_origen=$("#email").val();
                      var titulo=$('#asunto').val();
                      var mensaje=$('#contenido').val();
                      var resul;
                      

                      var parametros = {
                          "mensaje" : mensaje,
                          "destino" : "infoupalopa@upalopa.com", 
                          "titulo" : titulo,
                          "correo_origen" : correo_origen
                        };
                      $.ajax
                      ({
                          type: "POST",
                          url: url,
                          data: parametros,
                          success: function(data)
                          {
                            
                            
                             
                          }
                      }); 
                            resul= "<div class='alert alert-info' id='msg_act'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='glyphicon glyphicon-info-sign pull-left'></span>&nbsp;&nbsp;<strong>El mensaje se ha enviado con exito... </strong></div>";
                            $("#resultado1").html(resul);

         
        
          $("#contacto")[0].reset();
                         setTimeout(function() {
                          $("#msg_act").fadeOut(1500);
                        },3000);
            
     }
}

</script>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
    <script src="bootstrap/js/jquery.js"> </script>
    <script src="validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="validacion/css/template.css" type="text/css"/>
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <?php include("frontend/menu_frontend.php"); ?>
      </div>

                    <div class="jumbotron cajacontacto" style="margin-top: 0em;">


                        <div class="row">
                            <img  style="margin-left:-45px; width:600px; margin-top:-20px; margin-right:-45px;"  src="imagenes/sistema/banner_contactenos.jpg" >
                        </div>
                        <br>
                        <div class="row" style="margin-top: -20px;">
                            <div class="titulo_categoria" style="margin-left: 0.5em;">Contáctenos</div>
                        </div>
                      <form method="POST" name="contacto" id="contacto" onsubmit="return jQuery(this).validationEngine('validate');">
                          <br>
                          <div class="row">
                              <div class="col-md-3 col-xs-3 text-right"  style="padding-right: 0px;">
                                   <label for="usuario" > Email</label>
                              </div>
                              <div class="col-md-9 col-xs-9">
                                   <input type="email" name="email" id="email" class="validate[required, custom[email] , minSize[3],maxSize[100]] text-input form-control fondo_campo" >
                              </div>
                          </div>
                          <br>

                 
                          <div class="row">
                              <div class="col-md-3 col-xs-3 text-right" style="padding-right: 0px;">
                                 <label for="usuario" > Asunto</label>
                              </div>
                              <div class="col-md-9 col-xs-9">
                                 <input type="text" name="asunto" id="asunto" class="validate[required, minSize[4],maxSize[50]] text-input form-control fondo_campo" >
                              </div>
                          </div>
                          <br>

                        <div class="row">
                            <div class="col-md-3 col-xs-3 text-right" style="padding-right: 0px;">
                               <label for="usuario" >Contenido</label>
                            </div>
                            <div class="col-md-9 col-xs-9">
                               <textarea name="contenido" id="contenido" class="validate[required], text-input form-control fondo_campo" placeholder="Solicitud"> </textarea>
                            </div>
                        </div>
                        <br>
                        <div align="center"> <button type="button" name="enviar" onclick="enviar_correo()" id="enviar" class="btn btn-sm fondo_boton" > <div class="vineta-blanco">&nbsp;</div> Enviar </button>
                      </form>
                  </div>


      </div>
      <br>
      <br>
      <br>
      <br>
      <div class="navbar navbar-fixed-bottom hidden-xs" role="navigation">
        <?php
           include("frontend/menu_footer.php");
           include("frontend/footer.php");
        ?>
      </div>
      <div class="row visible-xs">
        <?php  include("frontend/footer.php"); ?>
      </div>
    </div>
    <!-- Modal para login -->
    <div class="modal fade" id="login" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    	<div class="modal-dialog" style="width: 400px">
    		<div class="modal-content">
    			<div class="modal-body">
            <div id="contenido_modal">
      				<?php 
      					include('login.php'); 
      				?>
            </div>
    			</div>
    		</div>  
    	</div>    
    </div>
    <script src="bootstrap/js/bootstrap.min.js"> </script>  
  </body>
</html>
