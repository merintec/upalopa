<?php
include_once("comunes/variables.php");
include_once("comunes/conexion.php");
?>
<!DOCTYPE html>
<script language="javascript" type="text/javascript">  
  $(document).ready(function(){
    $('.cajas').keypress(function(e){   
      if(e.which == 13){      
        ingresar_sys();
        return false;        
      }   
    });    
  });  
</script>
<script type="text/javascript">
function ingresar_sys()
{
  if ($("#form_login").validationEngine('validate')){
    var url="comunes/login_aux.php"; 
    var parametros = {
      "usuario":   $('#usuario').val(),
      "pass":   $('#pass').val(),
      "origen":   $('#origen').val()

    }
    $.ajax
    ({
        type: "POST",
        url: url,
        data: parametros,
        success: function(data)
        {
          $('#msg_login').show();
          $("#msg_loging_content").html(data);
          setTimeout(function() {
          $("#msg_login").fadeOut(1500);
        },3000);
        }
    });
    return false;
  }
}
function abrir_registro()
{
  var url="registrarse.php"; 
  $.ajax
  ({
      type: "POST",
      url: url,
      success: function(data)
      {
        $("#contenido_modal").html(data);
      }
  });
  return false;
}
function abrir_login()
{
  var url="login.php"; 
  $.ajax
  ({
      type: "POST",
      url: url,
      success: function(data)
      {
        $("#contenido_modal").html(data);
      }
  });
  return false;
}
function abrir_recuperapass()
{
  var url="recuperapass.php"; 
  $.ajax
  ({
      type: "POST",
      url: url,
      success: function(data)
      {
        $("#contenido_modal").html(data);
      }
  });
  return false;
}

</script>
<div class="ventana-titulo">
  Ingresa a Upalopa
</div>
<div class="borde-ventana-punteada">

<html lang="es">
<body>
  <div class="" style="margin:0px; margin-top: 7.5em; margin-left:auto; margin-right: auto;">      
    <button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -5.7em; margin-right: -5px; z-index: 999;">×</button>
      <div id="msg_login" class="alert alert-danger oculto">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong id="msg_loging_content"></strong>
      </div>
      <form method="POST" name="form_login" id="form_login" onsubmit="return jQuery(this).validationEngine('validate');">
      <input type="hidden" name="origen" id="origen" value="externo">
        <div class="row">
           <div class="col-md-3 col-xs-3 text-right" style="padding-right: 0px;">  
              <label for="usuario" > Email</label>
            </div>
           <div class="col-md-9 col-xs-7">  
              <input type="email" name="usuario" id="usuario" class="validate[required, custom[email] , minSize[3],maxSize[100]] text-input  form-control cajas fondo_campo" placeholder=""> 
          </div>
        </div>
        <br>
        <div class="row">
               <div class="col-md-3 col-xs-3">  
                  <label for="pass" style="padding-right: 0px;"> Contraseña</label>
              </div>
              <div class="col-md-9 col-xs-7">  
                   
                    <input type="password" name="pass" id="pass" class=" validate[required] form-control cajas fondo_campo" placeholder="">
              </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-3 col-xs-3"></div> 
            <div class="col-md-9 col-xs-7">  
              <div class="vineta-marron">&nbsp;</div>
              <div onclick="abrir_recuperapass()" >
                <label  style="cursor: pointer;"> ¿Olvidó su contraseña? </label>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 col-xs-3"></div> 
          <div class="col-md-9 col-xs-7">  
              <div class="vineta-marron">&nbsp;</div>
              <div onclick="abrir_registro()">
                <label style="cursor: pointer;"> ¿Desea Registrarse ? </label>
              </div>
          </div>
        </div>
        <br>
        <div align="center">
        <button id="ingresar" type="button" class="btn  fondo_boton" onclick="ingresar_sys()">
           <div class="vineta-blanco">&nbsp;</div> Ingresar
        </button>
        </div>
        <br>
      </form>
  </div>
</body>
</html>
</div>